SELECT G.ACCOUNTSIGN,
       G.PARENT_ID,
       G.ID,
       G.NAME,
       G.ACCOUNT_ID,
       G.ORGANIZADOR,
       G.ELEMENTLEVEL,
       SUM(G.AMTACCTDR) AMTACCTDR,
       SUM(G.AMTACCTCR) AMTACCTCR,
       SUM(G.SALDO_INICIAL) SALDO_INICIAL,
       SUM(G.TOTALAMTACCTDR) TOTALAMTACCTDR,
       SUM(G.TOTALAMTACCTCR) TOTALAMTACCTCR,
       SUM(G.SALDO_FINAL) SALDO_FINAL,
       G.DATE_FROM,
       G.DATE_TO,
       G.AD_ORG_ID,
       G.BP,
       G.DESCRIPTION,
       G.DATEACCT,
       G.TIPDOC,
       ''::VARCHAR AS AD_CLIENT_ID,
       ''::VARCHAR AS ORGANIZACION,
       ''::VARCHAR AS DIRECCION,
       ''::VARCHAR AS NIT,
       ''::VARCHAR AS PAIS,
       ''::VARCHAR AS REGION,
       ''::VARCHAR AS CIUDAD
FROM
  (SELECT ACCOUNTSIGN,
          MAX(PARENT_ID) AS PARENT_ID,
          ID,
          NAME,
          ACCOUNT_ID,
          ACCOUNT_ID AS ORGANIZADOR,
          ELEMENTLEVEL,
          SUM(AMTACCTDR) AS AMTACCTDR,
          SUM(AMTACCTCR) AS AMTACCTCR,
          COALESCE(B.S_I, 0) AS SALDO_INICIAL,
          0 AS TOTALAMTACCTDR,
          0 AS TOTALAMTACCTCR,
          CASE accountsign
              WHEN 'C' THEN CASE
                                WHEN SUM(amtacctcr - amtacctdr) < 0 THEN COALESCE(B.S_I, 0) + (SUM(amtacctcr - amtacctdr) * (-1))
                                ELSE COALESCE(B.S_I, 0) + (SUM(amtacctcr - amtacctdr) * (-1))
                            END
              WHEN 'D' THEN COALESCE(B.S_I, 0) + SUM(AMTACCTDR - AMTACCTCR)
              ELSE COALESCE(B.S_I, 0) + SUM(AMTACCTDR - AMTACCTCR)
          END SALDO_FINAL,
          TO_DATE('01-03-2016') AS DATE_FROM,
          TO_DATE('23-03-2016') AS DATE_TO,
          '92684CAC37824AB1BC733FB43C83F0B6' AS AD_ORG_ID,
          ''::VARCHAR AS BP,
          ''::VARCHAR AS DESCRIPTION,
          ''::VARCHAR AS DATEACCT,
          DOC AS TIPDOC
   FROM
     (SELECT ACCOUNTSIGN,
             AD_TREENODE.PARENT_ID,
             C_ELEMENTVALUE.C_ELEMENTVALUE_ID AS ID,
             C_ELEMENTVALUE.ELEMENTLEVEL,
             C_ELEMENTVALUE.NAME AS NAME,
             C_ELEMENTVALUE.VALUE AS ACCOUNT_ID,
             0 AS SALDO_INICIAL,
             0 AS AMTACCTDR,
             0 AS AMTACCTCR,
             ''::VARCHAR AS DOC
      FROM AD_TREENODE,
           C_ELEMENTVALUE
      WHERE AD_TREENODE.NODE_ID = C_ELEMENTVALUE.C_ELEMENTVALUE_ID
        AND AD_TREENODE.AD_TREE_ID = ('6F0238BE4302467FBED00FA231C2ADB3')
        AND AD_TREENODE.ISACTIVE = 'Y'
        AND C_ELEMENTVALUE.ISACTIVE = 'Y'
      UNION SELECT EV.ACCOUNTSIGN,
                   AD_TREENODE.PARENT_ID,
                   F.ACCOUNT_ID AS ID,
                   EV.ELEMENTLEVEL,
                   EV.NAME AS NAME,
                   EV.VALUE AS ACCOUNT_ID,
                   0 SALDO_INICIAL,
                   SUM(AMTACCTDR),
                   SUM(AMTACCTCR),
                   CASE
                       WHEN elementlevel = 'S' THEN ''::VARCHAR
                       ELSE OFACC_INFORECORD(f.ad_table_id, f.record_id, 'N')
                   END DOC
      FROM AD_TREENODE,
           FACT_ACCT F,
           C_ELEMENTVALUE EV
      WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
        AND AD_TREENODE.NODE_ID = EV.C_ELEMENTVALUE_ID
        AND f.AD_ORG_ID IN('92684CAC37824AB1BC733FB43C83F0B6')
        AND F.AD_CLIENT_ID IN ('0',
                               'A12F8A72D3C6485088BA450F43912630')
        AND F.AD_ORG_ID IN('0',
                           '437EA6F2C2154B38B9658EE82D7BB141',
                           '92684CAC37824AB1BC733FB43C83F0B6')
        AND 1=1
        AND F.DATEACCT >= TO_DATE('01-03-2016')
        AND F.DATEACCT <= TO_DATE('23-03-2016')
        AND F.FACTACCTTYPE <> 'R'
        AND F.FACTACCTTYPE <> 'C'
        AND F.ISACTIVE = 'Y'
      GROUP BY ACCOUNT_ID,
               EV.NAME,
               EV.VALUE,
               EV.ELEMENTLEVEL,
               OFACC_INFORECORD(f.ad_table_id, f.record_id, 'N'),
               EV.ACCOUNTSIGN,
               AD_TREENODE.PARENT_ID) A
   LEFT OUTER JOIN
     (SELECT ''::VARCHAR DOCBASETYPE,
             F1.ACCOUNT_ID AID,
             0 AS P_ID,
             CASE EV1.ACCOUNTSIGN
                 WHEN 'C' THEN CASE
                                   WHEN SUM(f1.amtacctcr - f1.amtacctdr) < 0 THEN SUM(f1.amtacctcr - f1.amtacctdr) * (-1)
                                   ELSE SUM(f1.amtacctcr - f1.amtacctdr) * (-1)
                               END
                 WHEN 'D' THEN SUM(f1.amtacctdr - f1.amtacctcr)
                 ELSE SUM(f1.amtacctdr - f1.amtacctcr)
             END S_I
      FROM FACT_ACCT F1,
           C_ELEMENTVALUE EV1
      WHERE F1.ACCOUNT_ID = EV1.C_ELEMENTVALUE_ID
        AND F1.DATEACCT < TO_TIMESTAMP('01-03-2016','MM-DD-YYYY')
        AND f1.AD_ORG_ID IN ('92684CAC37824AB1BC733FB43C83F0B6')
        AND F1.AD_CLIENT_ID IN ('0',
                                'A12F8A72D3C6485088BA450F43912630')
        AND F1.AD_ORG_ID IN ('0',
                             '437EA6F2C2154B38B9658EE82D7BB141',
                             '92684CAC37824AB1BC733FB43C83F0B6')
        AND F1.ISACTIVE = 'Y'
      GROUP BY F1.ACCOUNT_ID,
               EV1.ACCOUNTSIGN,
               F1.DOCBASETYPE) B ON B.AID = A.ID
   AND B.DOCBASETYPE = A.DOC
   GROUP BY ID,
            NAME,
            ACCOUNT_ID,
            ELEMENTLEVEL,
            DOC,
            ACCOUNTSIGN,
            B.S_I,
            AD_ORG_ID
   ORDER BY PARENT_ID,
            B.S_I,
            ID,
            NAME,
            ACCOUNT_ID,
            ELEMENTLEVEL) G
GROUP BY G.ACCOUNTSIGN,
         G.PARENT_ID,
         G.ID,
         G.NAME,
         G.ACCOUNT_ID,
         G.ORGANIZADOR,
         G.ELEMENTLEVEL,
         G.DATE_FROM,
         G.DATE_TO,
         G.AD_ORG_ID,
         G.BP,
         G.DESCRIPTION,
         G.DATEACCT,
         G.TIPDOC
ORDER BY G.PARENT_ID,
         G.ID,
         G.NAME,
         G.ACCOUNT_ID,
         G.ELEMENTLEVEL,
         G.TIPDOC