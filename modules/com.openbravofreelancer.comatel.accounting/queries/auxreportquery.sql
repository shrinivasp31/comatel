SELECT c_bpartner_id,
       SI_Cuenta,
       SI_Tercerodec,
       SI_Tercero,
       value,
       NameCuenta,
       datetrx,
       org,
       ORGANIZACION,
       NIT,
       tercero,
       tipoDoc,
       NumeroDoc,
       DesDoc,
       base,
       amtacctdr,
       amtacctcr,
       Parcial,
       c_elementvalue_id,
       Inter
  FROM (

        SELECT T.c_bpartner_id,
                T.SI_Cuenta,
                T. SI_Tercerodec,
                T. SI_Tercero,
                T.value,
                T.NameCuenta,
                T.datetrx,
                T.org,
                T.ORGANIZACION,
                T.NIT,
                T.tercero,
                T.tipoDoc,
                T.NumeroDoc,
                T.DesDoc,
                sum(T.amtacctdr) amtacctdr,
                sum(T.amtacctcr) amtacctcr,
                sum(T.base) base,
                sum(T.Parcial) Parcial,
                T.c_elementvalue_id,
                T.Inter,
                T.ad_client_id
          FROM (

                 SELECT f.c_bpartner_id,
                         B.SALDO_INICIAL SI_Cuenta,
                         B2.SI_TERCERO SI_Tercerodec,
                         B2.SI_TERCERO SI_Tercero,
                         ev.value,
                         ev.name NameCuenta,
                         f.datetrx datetrx,
                         f.ad_client_id,
                         org.name org,
                         (SELECT AO.NAME
                            FROM AD_ORG AO
                            RIGHT OUTER JOIN AD_ORGINFO AOI ON AO.AD_ORG_ID = AOI.AD_ORG_ID
                             AND AO.ISSUMMARY = 'Y'
                             AND AO.ISACTIVE = 'Y'
                             AND AO.AD_CLIENT_ID = f.ad_client_id
                             LIMIT 1) ORGANIZACION,
                         (SELECT AOI.TAXID
                            FROM AD_ORG AO
                            RIGHT OUTER JOIN AD_ORGINFO AOI ON AO.AD_ORG_ID = AOI.AD_ORG_ID
                             AND AO.ISSUMMARY = 'Y'
                             AND AO.ISACTIVE = 'Y'
                             AND AO.AD_CLIENT_ID = f.ad_client_id
                              LIMIT 1) NIT,
                         bp.taxid || ' - ' || BP.name tercero,
                         OFACC_INFORECORD(f.ad_table_id, f.record_id, 'N') tipoDoc,
                         OFACC_INFORECORD(f.ad_table_id, f.record_id, 'ND') NumeroDoc,
                         OFACC_INFORECORD(f.ad_table_id, f.record_id, 'D') DesDoc, 0.00 AS  base,
                         f.amtacctdr,
                         f.amtacctcr,
                         case ev.accountsign
                           when 'C' then
                            case
                           when (f.amtacctcr - f.amtacctdr) < 0 then
                            (f.amtacctcr - f.amtacctdr) * (-1)
                           else
                            (f.amtacctcr - f.amtacctdr) * (-1)
                         end

                         when 'D' then(f.amtacctdr - f.amtacctcr) else(f.amtacctdr - f.amtacctcr) End Parcial,

                         ev.c_elementvalue_id,
                         'Periodo del ' || TO_DATE('01-01-2016', 'MM-DD-YYYY') ||
                         ' al ' || to_date('01-31-2016', 'MM-DD-YYYY') Inter
                   FROM FACT_ACCT      F
                         LEFT OUTER JOIN C_Bpartner     bp ON bp.c_bpartner_id = F.c_bpartner_ID
                         LEFT OUTER JOIN ad_org         org ON org.ad_org_id = F.ad_org_id
                         LEFT OUTER JOIN C_ELEMENTVALUE EV on EV.C_ELEMENTVALUE_ID = F.ACCOUNT_ID
                         LEFT OUTER JOIN
                         (SELECT case ev1.accountsign
                                   when 'C' then
                                    case
                                   when sum(f1.amtacctcr - f1.amtacctdr) < 0 then
                                    sum(f1.amtacctcr - f1.amtacctdr) * (-1)
                                   else
                                    sum(f1.amtacctcr - f1.amtacctdr) * (-1)
                                 end when 'D' then sum(f1.amtacctdr - f1.amtacctcr) else sum(f1.amtacctdr - f1.amtacctcr) End SI_TERCERO,
                                 F1.ACCOUNT_ID,
                                 f1.c_bpartner_id
                            FROM FACT_ACCT      F1
                                 RIGHT OUTER JOIN GL_CATEGORY    GL1 ON F1.GL_CATEGORY_ID = GL1.GL_CATEGORY_ID
                                 LEFT OUTER JOIN C_Bpartner     bp ON bp.c_bpartner_id = F1.c_bpartner_ID,
                                 C_ELEMENTVALUE EV1

                           WHERE F1.ACCOUNT_ID = EV1.C_ELEMENTVALUE_ID
                            AND F1.DATEACCT < to_date('MM-DD-YYYY','01-01-2016')

                             AND F1.ISACTIVE = 'Y'
                             AND bp.c_bpartner_id = COALESCE(null, bp.c_bpartner_id)
                             AND ev1.value >=
                                 (select ev1.value
                                    from c_elementvalue ev1
                                   where ev1.c_elementvalue_id = '1006747')
                             AND ev1.value <=
                                 (select ev1.value
                                    from c_elementvalue ev1
                                   where ev1.c_elementvalue_id = '1007051')
                           GROUP BY F1.ACCOUNT_ID,
                                    f1.c_bpartner_id,
                                    ev1.accountsign) B2 ON  B2.c_bpartner_id = F.c_bpartner_id AND B2.account_id = F.account_id
                                    LEFT OUTER JOIN
                         (SELECT case ev1.accountsign
                                   when 'C' then
                                    case
                                   when sum(f1.amtacctcr - f1.amtacctdr) < 0 then
                                    sum(f1.amtacctcr - f1.amtacctdr) * (-1)
                                   else
                                    sum(f1.amtacctcr - f1.amtacctdr) * (-1)
                                 end when 'D' then sum(f1.amtacctdr - f1.amtacctcr) else sum(f1.amtacctdr - f1.amtacctcr) End SALDO_INICIAL,
                                 F1.ACCOUNT_ID
                            FROM FACT_ACCT F1
                            RIGHT OUTER JOIN GL_CATEGORY GL1 ON F1.GL_CATEGORY_ID = GL1.GL_CATEGORY_ID, C_ELEMENTVALUE EV1
                           WHERE F1.ACCOUNT_ID = EV1.C_ELEMENTVALUE_ID
                            AND F1.DATEACCT < to_date('MM-DD-YYYY','01-01-2016')
                             AND F1.ISACTIVE = 'Y'
                             AND ev1.value >=
                                 (select ev1.value
                                    from c_elementvalue ev1
                                   where ev1.c_elementvalue_id = '1006747')
                             AND ev1.value <=
                                 (select ev1.value
                                    from c_elementvalue ev1
                                   where ev1.c_elementvalue_id = '1007051')
                           GROUP BY F1.ACCOUNT_ID, ev1.accountsign) B ON B.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID

                  WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
                    --AND decode($P{Tercero}, null, 1, f.c_bpartner_id) =
                      --  decode($P{Tercero}, null, 1, $P{Tercero})
                    AND ev.value >=
                        (select ev.value
                           from c_elementvalue ev
                                   where ev.c_elementvalue_id = '1006747')
                    AND ev.value <=
                        (select ev.value
                           from c_elementvalue ev
                                   where ev.c_elementvalue_id = '1007051')
                    AND f.dateacct >= '01-01-2016'
                    AND f.dateacct <= '01-31-2016'
                    AND f.ad_org_id in
                        (SELECT NODE_ID AS ID
                           FROM AD_TREENODE
                          where AD_TREE_ID = '633D8CBD50A14A3D8155FEF396BE530A' /*arbol por default*/
                            AND AD_ISMEMBERINCLUDED(NODE_ID,
                                                    COALESCE('92684CAC37824AB1BC733FB43C83F0B6', '0'),
                                                    '633D8CBD50A14A3D8155FEF396BE530A') <> -1)
                  order by bp.taxid,
                            ev.value,
                            f.datetrx,
                            OFACC_INFORECORD(f.ad_table_id, f.record_id, 'N')

                 ) T

         GROUP BY T.value,
                   T.c_bpartner_id,
                   T.SI_Cuenta,
                   T. SI_Tercerodec,
                   T. SI_Tercero,
                   T.NIT,
                   T.NameCuenta,
                   T.datetrx,
                   T.ORGANIZACION,
                   T.org,
                   T.tercero,
                   T.tipoDoc,
                   T.NumeroDoc,
                   T.DesDoc,
                   T.c_elementvalue_id,
                   T.Inter,
                   T.ad_client_id
         ORDER BY T.value, T.tercero, T.tipoDoc, T.datetrx) AA

UNION

SELECT f.c_bpartner_id,
       COALESCE(B.SALDO_INICIAL, 0) SI_Cuenta,
       SUM(COALESCE(B2.SI_TERCERO,0)) SI_Tercerodec,
       SUM(B2.SI_TERCERO) SI_Tercero,
       ev.value,
       ev.name NameCuenta,
       f.datetrx datetrx,
       org.name org,
       (SELECT AO.NAME
          FROM AD_ORG AO
          right outer join AD_ORGINFO AOI on AO.AD_ORG_ID = AOI.AD_ORG_ID
           WHERE AO.ISSUMMARY = 'Y'
           AND AO.ISACTIVE = 'Y'
           AND AO.AD_CLIENT_ID = f.ad_client_id
           LIMIT 1) ORGANIZACION,
       (SELECT AOI.TAXID
          FROM AD_ORG AO
          right outer join AD_ORGINFO AOI on AO.AD_ORG_ID = AOI.AD_ORG_ID
           where AO.ISSUMMARY = 'Y'
           AND AO.ISACTIVE = 'Y'
           AND AO.AD_CLIENT_ID = f.ad_client_id
           LIMIT 1) NIT,
       bp.taxid || ' - ' || BP.name tercero,
       '' tipoDoc,
       '' NumeroDoc,
       '' DesDoc,
       0 base,
       0 amtacctdr,
       0 amtacctcr,
       0 Parcial,

       ev.c_elementvalue_id,
       'Periodo del ' || TO_DATE('01-01-2016', 'MM-DD-YYYY') || ' al ' ||
       TO_DATE('31-01-2016', 'MM-DD-YYYY') Inter
  FROM FACT_ACCT      F
       left outer join C_Bpartner     bp on bp.c_bpartner_id = F.c_bpartner_ID
       JOIN C_ELEMENTVALUE EV ON EV.C_ELEMENTVALUE_ID = F.ACCOUNT_ID
       left outer join ad_org         org on org.ad_org_id = F.ad_org_id

       LEFT OUTER JOIN
       (SELECT case ev1.accountsign
                 when 'C' then
                  case
                 when sum(f1.amtacctcr - f1.amtacctdr) < 0 then
                  sum(f1.amtacctcr - f1.amtacctdr) * (-1)
                 else
                  sum(f1.amtacctcr - f1.amtacctdr) * (-1)
               end when 'D' then sum(f1.amtacctdr - f1.amtacctcr) else sum(f1.amtacctdr - f1.amtacctcr) End SALDO_INICIAL,
               F1.ACCOUNT_ID
          FROM FACT_ACCT F1
          RIGHT OUTER JOIN GL_CATEGORY GL1 ON F1.GL_CATEGORY_ID = GL1.GL_CATEGORY_ID,C_ELEMENTVALUE EV1
         WHERE F1.ACCOUNT_ID = EV1.C_ELEMENTVALUE_ID
           AND F1.DATEACCT > TO_DATE('MM-DD-YYYY','01-01-2016')
           AND F1.ISACTIVE = 'Y'
           AND F1.GL_CATEGORY_ID = GL1.GL_CATEGORY_ID
           AND ev1.value >=
               (select ev1.value
                  from c_elementvalue ev1
                 where ev1.c_elementvalue_id = '1006747')
           AND ev1.value <=
               (select ev1.value
                  from c_elementvalue ev1
                  where ev1.c_elementvalue_id = '1007051')
         GROUP BY F1.ACCOUNT_ID, ev1.accountsign) B ON B.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID

      LEFT OUTER JOIN
       (SELECT case ev1.accountsign
                 when 'C' then
                  case
                 when sum(f1.amtacctcr - f1.amtacctdr) < 0 then
                  sum(f1.amtacctcr - f1.amtacctdr) * (-1)
                 else
                  sum(f1.amtacctcr - f1.amtacctdr) * (-1)
               end when 'D' then sum(f1.amtacctdr - f1.amtacctcr) else sum(f1.amtacctdr - f1.amtacctcr) End SI_TERCERO,
               F1.ACCOUNT_ID,
               f1.c_bpartner_id
          FROM FACT_ACCT      F1
               RIGHT OUTER JOIN GL_CATEGORY    GL1 ON F1.GL_CATEGORY_ID = GL1.GL_CATEGORY_ID
               LEFT OUTER JOIN  C_Bpartner     bp ON bp.c_bpartner_id = F1.c_bpartner_ID,
               C_ELEMENTVALUE EV1
         WHERE F1.ACCOUNT_ID = EV1.C_ELEMENTVALUE_ID
           AND F1.DATEACCT < TO_DATE('MM-DD-YYYY','01-01-2016')
           AND F1.ISACTIVE = 'Y'
           AND bp.c_bpartner_id = COALESCE(null, bp.c_bpartner_id)
           AND ev1.value >=
               (select ev1.value
                  from c_elementvalue ev1
                 where ev1.c_elementvalue_id = '1006747')
           AND ev1.value <=
               (select ev1.value
                  from c_elementvalue ev1
                 where ev1.c_elementvalue_id = '1007051')
         GROUP BY F1.ACCOUNT_ID, f1.c_bpartner_id, ev1.accountsign) B2 ON B2.c_bpartner_id = F.c_bpartner_id AND B2.account_id = F.account_id

 WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
  -- AND decode($P{Tercero}, null, 1, f.c_bpartner_id) =
    --   decode($P{Tercero}, null, 1, $P{Tercero})
   AND ev.value >=
       (select ev.value
          from c_elementvalue ev
         where ev.c_elementvalue_id = '1006747')
   AND ev.value <=
       (select ev.value
          from c_elementvalue ev
         where ev.c_elementvalue_id = '1007051')
   AND f.dateacct <= '01-01-2016'

   AND f.ad_org_id in
       (SELECT NODE_ID AS ID
          FROM AD_TREENODE
         where AD_TREE_ID = '633D8CBD50A14A3D8155FEF396BE530A' /*arbol por default*/
           AND AD_ISMEMBERINCLUDED(NODE_ID,
                                   COALESCE('92684CAC37824AB1BC733FB43C83F0B6', '0'),
                                   '633D8CBD50A14A3D8155FEF396BE530A') <> -1)
 group by f.c_bpartner_id,
          B.SALDO_INICIAL,
          ev.value,
          ev.name,
          f.datetrx,
          org.name,
          bp.taxid,
          BP.name,
          f.record_id,
          f.ad_table_id,
          f.fact_acct_id,
          ev.accountsign,
          ev.c_elementvalue_id,
          f.ad_client_id

 order by value, tercero, datetrx, tipoDoc