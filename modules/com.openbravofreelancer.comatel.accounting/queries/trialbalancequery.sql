SELECT ID,
       ACCOUNT_ID,
       NAME,
       SUM(SALDO_INICIAL) AS SALDO_INICIAL,
       SUM(AMTACCTDR) AS AMTACCTDR,
       SUM(AMTACCTCR) AS AMTACCTCR,
       SUM(SALDO_INICIAL+AMTACCTDR-AMTACCTCR) AS SALDO_FINAL,
       groupbyid,
       CASE NULL
           WHEN 'BPartner' THEN CASE ad_column_identifier(TO_CHAR('C_BPARTNER'), TO_CHAR(groupbyid), TO_CHAR('en_US'))
                                    WHEN '**' THEN ''
                                    ELSE ad_column_identifier(TO_CHAR('C_BPARTNER'), TO_CHAR(groupbyid), TO_CHAR('en_US'))
                                END
           WHEN 'Product' THEN CASE ad_column_identifier(TO_CHAR('M_PRODUCT'), TO_CHAR(groupbyid), TO_CHAR('en_US'))
                                   WHEN '**' THEN ''
                                   ELSE ad_column_identifier(TO_CHAR('M_PRODUCT'), TO_CHAR(groupbyid), TO_CHAR('en_US'))
                               END
           WHEN 'Project' THEN CASE ad_column_identifier(TO_CHAR('C_PROJECT'), TO_CHAR(groupbyid), TO_CHAR('en_US'))
                                   WHEN '**' THEN ''
                                   ELSE ad_column_identifier(TO_CHAR('C_PROJECT'), TO_CHAR(groupbyid), TO_CHAR('en_US'))
                               END
           ELSE ''
       END AS groupbyname
FROM (
        (SELECT ID,
                ACCOUNT_ID,
                NAME,
                0 AS AMTACCTDR,
                0 AS AMTACCTCR,
                COALESCE(SUM(AMTACCTDR-AMTACCTCR), 0) AS SALDO_INICIAL,
                groupbyname,
                groupbyid
         FROM (
                 (SELECT F.ACCOUNT_ID AS ID,
                         EV.VALUE AS ACCOUNT_ID,
                         EV.NAME AS NAME,
                         F.AMTACCTDR,
                         F.AMTACCTCR,
                         F.FACTACCTTYPE,
                         F.DATEACCT,
                         CASE NULL
                             WHEN 'BPartner' THEN c_bpartner.c_bpartner_id
                             WHEN 'Product' THEN m_product.m_product_id
                             WHEN 'Project' THEN c_project.c_project_id
                             ELSE ''
                         END AS groupbyid,
                         CASE NULL
                             WHEN 'BPartner' THEN to_char(c_bpartner.name)
                             WHEN 'Product' THEN to_char(m_product.name)
                             WHEN 'Project' THEN to_char(c_project.name)
                             ELSE ''
                         END AS groupbyname
                  FROM C_ELEMENTVALUE EV,
                       FACT_ACCT F
                  LEFT JOIN C_BPARTNER ON f.C_BPARTNER_ID = C_BPARTNER.C_BPARTNER_ID
                  LEFT JOIN M_PRODUCT ON f.M_PRODUCT_ID = M_PRODUCT.M_PRODUCT_ID
                  LEFT JOIN C_PROJECT ON f.C_PROJECT_ID = C_PROJECT.C_PROJECT_ID
                  WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
                    AND EV.ELEMENTLEVEL = 'S'
                    AND f.AD_ORG_ID IN('92684CAC37824AB1BC733FB43C83F0B6')
                    AND F.AD_CLIENT_ID IN ('0',
                                           'A12F8A72D3C6485088BA450F43912630')
                    AND F.AD_ORG_ID IN('0',
                                       '437EA6F2C2154B38B9658EE82D7BB141',
                                       '92684CAC37824AB1BC733FB43C83F0B6')
                    AND 1=1
                    AND EV.VALUE <= '939930'
                    AND F.DATEACCT < TO_DATE('01-01-2016')
                    AND F.C_ACCTSCHEMA_ID = 'FEB7AF79C6314305BBAEBE0ECEA61B0A'
                    AND F.ISACTIVE = 'Y')
               UNION ALL
                 (SELECT F.ACCOUNT_ID AS ID,
                         EV.VALUE AS ACCOUNT_ID,
                         EV.NAME AS NAME,
                         F.AMTACCTDR,
                         F.AMTACCTCR,
                         F.FACTACCTTYPE,
                         F.DATEACCT,
                         CASE NULL
                             WHEN 'BPartner' THEN c_bpartner.c_bpartner_id
                             WHEN 'Product' THEN m_product.m_product_id
                             WHEN 'Project' THEN c_project.c_project_id
                             ELSE ''
                         END AS groupbyid,
                         CASE NULL
                             WHEN 'BPartner' THEN to_char(c_bpartner.name)
                             WHEN 'Product' THEN to_char(m_product.name)
                             WHEN 'Project' THEN to_char(c_project.name)
                             ELSE ''
                         END AS groupbyname
                  FROM C_ELEMENTVALUE EV,
                       FACT_ACCT F
                  LEFT JOIN C_BPARTNER ON f.C_BPARTNER_ID = C_BPARTNER.C_BPARTNER_ID
                  LEFT JOIN M_PRODUCT ON f.M_PRODUCT_ID = M_PRODUCT.M_PRODUCT_ID
                  LEFT JOIN C_PROJECT ON f.C_PROJECT_ID = C_PROJECT.C_PROJECT_ID
                  WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
                    AND EV.ELEMENTLEVEL = 'S'
                    AND f.AD_ORG_ID IN('92684CAC37824AB1BC733FB43C83F0B6')
                    AND F.AD_CLIENT_ID IN ('0',
                                           'A12F8A72D3C6485088BA450F43912630')
                    AND F.AD_ORG_ID IN('0',
                                       '437EA6F2C2154B38B9658EE82D7BB141',
                                       '92684CAC37824AB1BC733FB43C83F0B6')
                    AND 3=3
                    AND EV.VALUE <= '939930'
                    AND F.DATEACCT = TO_DATE('01-01-2016')
                    AND F.C_ACCTSCHEMA_ID = 'FEB7AF79C6314305BBAEBE0ECEA61B0A'
                    AND F.FACTACCTTYPE = 'O'
                    AND F.ISACTIVE = 'Y' )) A
         GROUP BY ACCOUNT_ID,
                  ID,
                  groupbyname,
                  groupbyid,
                  NAME
         HAVING SUM(AMTACCTDR) - SUM(AMTACCTCR) <> 0)
      UNION
        (SELECT ID,
                ACCOUNT_ID,
                NAME,
                SUM(AMTACCTDR) AS AMTACCTDR,
                SUM(AMTACCTCR) AS AMTACCTCR,
                0 AS SALDO_INICIAL,
                groupbyname,
                groupbyid
         FROM
           (SELECT F.ACCOUNT_ID AS ID,
                   EV.VALUE AS ACCOUNT_ID,
                   EV.NAME AS NAME,
                   F.AMTACCTDR,
                   F.AMTACCTCR,
                   F.FACTACCTTYPE,
                   CASE NULL
                       WHEN 'BPartner' THEN c_bpartner.c_bpartner_id
                       WHEN 'Product' THEN m_product.m_product_id
                       WHEN 'Project' THEN c_project.c_project_id
                       ELSE ''
                   END AS groupbyid,
                   CASE NULL
                       WHEN 'BPartner' THEN to_char(c_bpartner.name)
                       WHEN 'Product' THEN to_char(m_product.name)
                       WHEN 'Project' THEN to_char(c_project.name)
                       ELSE ''
                   END AS groupbyname
            FROM C_ELEMENTVALUE EV,
                 FACT_ACCT F
            LEFT JOIN C_BPARTNER ON f.C_BPARTNER_ID = C_BPARTNER.C_BPARTNER_ID
            LEFT JOIN M_PRODUCT ON f.M_PRODUCT_ID = M_PRODUCT.M_PRODUCT_ID
            LEFT JOIN C_PROJECT ON f.C_PROJECT_ID = C_PROJECT.C_PROJECT_ID
            WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
              AND EV.ELEMENTLEVEL = 'S'
              AND f.AD_ORG_ID IN('92684CAC37824AB1BC733FB43C83F0B6')
              AND F.AD_CLIENT_ID IN ('0',
                                     'A12F8A72D3C6485088BA450F43912630')
              AND F.AD_ORG_ID IN('0',
                                 '437EA6F2C2154B38B9658EE82D7BB141',
                                 '92684CAC37824AB1BC733FB43C83F0B6')
              AND 2=2
              AND EV.VALUE <= '939930'
              AND DATEACCT >= TO_DATE('01-01-2016')
              AND DATEACCT < TO_DATE('01-01-2017')
              AND F.C_ACCTSCHEMA_ID = 'FEB7AF79C6314305BBAEBE0ECEA61B0A'
              AND F.FACTACCTTYPE <> 'O'
              AND F.FACTACCTTYPE <> 'R'
              AND F.FACTACCTTYPE <> 'C'
              AND F.ISACTIVE = 'Y') B
         GROUP BY ACCOUNT_ID,
                  ID,
                  groupbyname,
                  groupbyid,
                  NAME
         HAVING SUM(AMTACCTDR) <> 0
         OR SUM(AMTACCTCR) <> 0)) C
GROUP BY ACCOUNT_ID,
         ID,
         groupbyid,
         groupbyname,
         NAME
ORDER BY ACCOUNT_ID,
         ID,
         groupbyname,
         groupbyid,
         NAME