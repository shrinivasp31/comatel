						SELECT MAX(PARENT_ID),
						       ID,
						       NAME,
						       A.ACCOUNT_ID,
						       ELEMENTLEVEL,
						       MAX(AMTACCTDR) AS AMTACCTDR,
						       MAX(AMTACCTCR) AS AMTACCTCR,
						       coalesce(SUM(B.SALDO_INICIAL), 0) AS SALDO_INICIAL,
						       0 AS TOTALAMTACCTDR,
						       0 AS TOTALAMTACCTCR,
						       SUM((coalesce(B.SALDO_INICIAL, 0)) + AMTACCTDR - AMTACCTCR) AS SALDO_FINAL,
						       TO_DATE('01-01-2016') AS DATE_FROM,
						       TO_DATE('31-12-2016') AS DATE_TO,
						       '' AS AD_ORG_ID,
						       '' AS BP,
						       '' AS PADRE,
						       '' AS cuenta,
						       '' AS IDBP,
						       '' AS SI_CUENTA,
						       '' AS AD_CLIENT_ID,
						       '' AS ORGANIZACION,
						       '' AS DIRECCION,
						       '' AS NIT,
						       '' AS PAIS,
						       '' AS CIUDAD,
						       '' AS REGION,
						       '' AS NUMERO,
						       '' AS ACCOUNTID,
						       '' AS ACCOUNTIDMODIFICADO,
						       '' AS NOMBRE_CUENTA,
						       '0' AS POSICION
						  FROM ( select AA.PARENT_ID AS PARENT_ID,
						               AA.ID,
						               AA.ELEMENTLEVEL,
						               AA.NAME,
						               AA.ACCOUNT_ID,
						               coalesce(SUM(AA.SALDO_INICIAL), 0) AS SALDO_INICIAL,
						               SUM(AA.AMTACCTDR) AS AMTACCTDR,
						               SUM(AA.AMTACCTCR) AS AMTACCTCR
						          from (
						                
						                SELECT AD_TREENODE.PARENT_ID,
						                        C_ELEMENTVALUE.C_ELEMENTVALUE_ID AS ID,
						                        C_ELEMENTVALUE.ELEMENTLEVEL,
						                        C_ELEMENTVALUE.NAME AS NAME,
						                        C_ELEMENTVALUE.VALUE AS ACCOUNT_ID,
						                        0 AS SALDO_INICIAL,
						                        0 AS AMTACCTDR,
						                        0 AS AMTACCTCR
						                  FROM AD_TREENODE, C_ELEMENTVALUE
						                 WHERE AD_TREENODE.NODE_ID = C_ELEMENTVALUE.C_ELEMENTVALUE_ID
						                   AND AD_TREENODE.AD_TREE_ID = '6F0238BE4302467FBED00FA231C2ADB3'
						                   AND AD_TREENODE.ISACTIVE = 'Y'
						                   AND C_ELEMENTVALUE.ISACTIVE = 'Y'
						                   AND (select max(c_element_id)
						                          from c_acctschema_element
						                         where c_acctschema_id = 'FEB7AF79C6314305BBAEBE0ECEA61B0A'
						                           and ELEMENTTYPE = 'AC') =
						                       C_ELEMENTVALUE.C_ELEMENT_ID
						                UNION ALL
						                SELECT '' AS PARENT_ID,
						                       F.ACCOUNT_ID AS ID,
						                       EV.ELEMENTLEVEL,
						                       EV.NAME AS NAME,
						                       EV.VALUE AS ACCOUNT_ID,
						                       SUM((CASE f.FACTACCTTYPE
						                             WHEN 'O' THEN
						                              F.AMTACCTDR - F.AMTACCTCR
						                             ELSE
						                              0
						                           END)) AS SALDO_INICIAL,
						                       SUM((CASE f.FACTACCTTYPE
						                             WHEN 'O' THEN
						                              0
						                             ELSE
						                              F.AMTACCTDR
						                           END)) AS AMTACCTDR,
						                       SUM((CASE f.FACTACCTTYPE
						                             WHEN 'O' THEN
						                              0
						                             ELSE
						                              f.AMTACCTCR
						                           END)) AS AMTACCTCR
						                  FROM FACT_ACCT F, C_ELEMENTVALUE EV
						                 WHERE F.ACCOUNT_ID = EV.C_ELEMENTVALUE_ID
						                   AND f.AD_ORG_ID IN ('92684CAC37824AB1BC733FB43C83F0B6')
						                   AND F.AD_CLIENT_ID IN ('A12F8A72D3C6485088BA450F43912630')
						                   AND F.AD_ORG_ID IN ('92684CAC37824AB1BC733FB43C83F0B6')
						                   AND F.DATEACCT >= TO_DATE('01-01-2016')
						                   AND F.DATEACCT < TO_DATE('07-31-2016')
						                   AND F.FACTACCTTYPE <> 'R'
						                   AND F.FACTACCTTYPE <> 'C'
						                   AND F.ISACTIVE = 'Y'
						                 GROUP BY ACCOUNT_ID, EV.NAME, EV.VALUE, EV.ELEMENTLEVEL) AA
						         GROUP BY ID, ELEMENTLEVEL, NAME, ACCOUNT_ID , PARENT_ID) A
						         LEFT OUTER JOIN 
						       (SELECT SUM(F1.AMTACCTDR - F1.AMTACCTCR) AS SALDO_INICIAL,dateacct,acctvalue , 
						               F1.ACCOUNT_ID
						          FROM FACT_ACCT F1
						         WHERE F1.DATEACCT < TO_DATE('12-31-2016')
						           AND f1.AD_ORG_ID IN ('92684CAC37824AB1BC733FB43C83F0B6')
						           AND F1.AD_CLIENT_ID IN ('A12F8A72D3C6485088BA450F43912630')
						           AND F1.AD_ORG_ID IN ('92684CAC37824AB1BC733FB43C83F0B6')
						           AND 2=2
						           AND F1.ISACTIVE = 'Y'
						         GROUP BY F1.ACCOUNT_ID , dateacct,acctvalue) B
							ON A.ID = B.ACCOUNT_ID
						 GROUP BY ID, NAME, A.ACCOUNT_ID, ELEMENTLEVEL , PARENT_ID
						 ORDER BY ACCOUNT_ID, ID, NAME, ELEMENTLEVEL
