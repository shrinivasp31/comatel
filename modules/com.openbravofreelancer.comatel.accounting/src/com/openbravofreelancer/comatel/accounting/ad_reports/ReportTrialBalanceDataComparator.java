package com.openbravofreelancer.comatel.accounting.ad_reports;

import java.util.Comparator;

/**
 * @author shrinivas
 *
 */
public class ReportTrialBalanceDataComparator implements Comparator<Object> {
	public int compare(Object obj1, Object obj2) {
		String code1 = ((ReportTrialBalanceData) obj1).accountId.toUpperCase();
		String code2 = ((ReportTrialBalanceData) obj2).accountId.toUpperCase();
		return code1.compareTo(code2);
	}
}
