package com.openbravofreelancer.comatel.accounting.ad_reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_combos.OrganizationComboData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.info.SelectorUtilityData;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

/**
 * @author shrinivas
 *
 */
public class ReportLibroDiario extends HttpSecureAppServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8575906784820998419L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);

		if (vars.commandIn("DEFAULT")) {
			String strDateFrom = vars.getGlobalVariable("inpDateFrom", "ReportLibroDiario|DateFrom", "");
			String strDateTo = vars.getGlobalVariable("inpDateTo", "ReportLibroDiario|DateTo", "");
			String strOnly = vars.getGlobalVariable("inpOnly", "ReportLibroDiario|Only", "-1");
			String strOrg = vars.getGlobalVariable("inpOrg", "ReportLibroDiario|Org", "");
			String strLevel = vars.getGlobalVariable("inpLevel", "ReportLibroDiario|Level", "");
			String strAccountFrom = vars.getGlobalVariable("inpAccountFrom", "ReportLibroDiario|AccountFrom", "");
			String strAccountTo = vars.getGlobalVariable("inpAccountTo", "ReportLibroDiario|AccountTo",
					ReportLibroDiarioData.selectLastAccount(this,
							Utility.getContext(this, vars, "#User_Org", "Account"),
							Utility.getContext(this, vars, "#User_Client", "Account")));
			String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN", "ReportTrialBalance|cBpartnerId", "",
					IsIDFilter.instance);
			String strAll = vars.getStringParameter("inpAll");

			printPageDataSheet(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strOnly, strAccountFrom,
					strAccountTo, strAll, strcBpartnerId);
		} else if (vars.commandIn("FIND")) {
			String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom", "ReportLibroDiario|DateFrom");
			String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "ReportLibroDiario|DateTo");
			String strOnly = vars.getRequestGlobalVariable("inpOnly", "ReportLibroDiario|Only");
			String strOrg = vars.getRequestGlobalVariable("inpOrg", "ReportLibroDiario|Org");
			String strLevel = vars.getRequestGlobalVariable("inpLevel", "ReportLibroDiario|Level");
			String strAccountFrom = vars.getRequestGlobalVariable("inpAccountFrom", "ReportLibroDiario|AccountFrom");
			String strAccountTo = vars.getRequestGlobalVariable("inpAccountTo", "ReportLibroDiario|AccountTo");

			String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN", "ReportTrialBalance|cBpartnerId", "",
					IsIDFilter.instance);
			String strAll = vars.getStringParameter("inpAll");
			printPageDataSheet(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strOnly, strAccountFrom,
					strAccountTo, strAll, strcBpartnerId);
		} else if (vars.commandIn("PDF")) {
			String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom", "ReportLibroDiario|DateFrom");
			String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "ReportLibroDiario|DateTo");
			String strOnly = vars.getRequestGlobalVariable("inpOnly", "ReportLibroDiario|Only");
			String strOrg = vars.getRequestGlobalVariable("inpOrg", "ReportLibroDiario|Org");
			String strLevel = vars.getRequestGlobalVariable("inpLevel", "ReportLibroDiario|Level");
			String strAccountFrom = vars.getRequestGlobalVariable("inpAccountFrom", "ReportLibroDiario|AccountFrom");
			String strAccountTo = vars.getRequestGlobalVariable("inpAccountTo", "ReportLibroDiario|AccountTo");

			String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN", "ReportTrialBalance|cBpartnerId", "",
					IsIDFilter.instance);
			String strAll = vars.getStringParameter("inpAll");
			printPageDataPDF(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strOnly, strAccountFrom,
					strAccountTo, strAll, strcBpartnerId);
		} else
			pageError(response);
	}

	void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strDateFrom, String strDateTo,
			String strOrg, String strLevel, String strOnly, String strAccountFrom, String strAccountTo, String strAll,
			String strcBpartnerId) throws IOException, ServletException {
		String strMessage = "";
		if (log4j.isDebugEnabled())
			log4j.debug("Output: dataSheet");
		if (log4j.isDebugEnabled())
			log4j.debug("strAll:" + strAll + " - strLevel:" + strLevel + " - strOnly:" + strOnly);
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String discard[] = { "sectionDiscard", "sectionBP" };
		XmlDocument xmlDocument = null;
		String strTreeOrg = ReportLibroDiarioData.treeOrg(this, vars.getClient());
		String strOrgFamily = getFamily(strTreeOrg, strOrg);
		String strTreeAccount = ReportLibroDiarioData.treeAccount(this, vars.getClient());
		String strcBpartnerIdAux = strcBpartnerId;

		BigDecimal v_amtacctdr = new BigDecimal("0.0");
		BigDecimal v_amtacctcr = new BigDecimal("0.0");
		BigDecimal v_saldoinicial = new BigDecimal("0.0");
		BigDecimal v_Saldofinal = new BigDecimal("0.0");

		String v_Samtacctdr = "";
		String v_Samtacctcr = "";
		String v_Ssaldoinicial = "";
		String v_Ssaldofinal = "";

		ReportLibroDiarioData[] data2 = null;
		ReportLibroDiarioData[] data = null;

		if (strDateFrom.equals("") && strDateTo.equals("")) {
			xmlDocument = xmlEngine
					.readXmlTemplate("com/openbravofreelancer/comatel/accounting/ad_reports/ReportLibroDiario", discard)
					.createXmlDocument();
			data = ReportLibroDiarioData.set();
			if (vars.commandIn("FIND")) {
				strMessage = Utility.messageBD(this, "BothDatesCannotBeBlank", vars.getLanguage());
				log4j.warn("Both dates are blank");
			}
		} else {
			if (!strLevel.equals("S"))
				discard[0] = "selEliminarField";
			else
				discard[0] = "discard";

			if (log4j.isDebugEnabled())
				log4j.debug("printPageDataSheet - strOrgFamily = " + strOrgFamily);

			if (strLevel.equals("S") && strOnly.equals("-1")) {

				if (log4j.isDebugEnabled())
					log4j.debug("strcBpartnerId:" + strcBpartnerId + " - strAll:" + strAll);
				if (!(strAll.equals("") && (strcBpartnerId.equals("")))) {
					if (log4j.isDebugEnabled())
						log4j.debug("Select BP, strcBpartnerId:" + strcBpartnerId + " - strAll:" + strAll);
					if (!strAll.equals(""))
						strcBpartnerId = "";
					discard[1] = "sectionNoBP";
					data = ReportLibroDiarioData.selectBP(this, strDateFrom, strDateTo, strOrg, strOrgFamily,
							Utility.getContext(this, vars, "#User_Client", "2"),
							Utility.getContext(this, vars, "#User_Org", "ReportLibroDiario"), strDateFrom, strDateTo,
							strAccountFrom, strAccountTo, strcBpartnerId);

				} else {
					data = ReportLibroDiarioData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount,
							strOrgFamily, Utility.getContext(this, vars, "#User_Client", "ReportLibroDiario"),
							Utility.getContext(this, vars, "#User_Org", "ReportLibroDiario"), strDateFrom, strDateTo);
				}
			} else {
				data = ReportLibroDiarioData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount, strOrgFamily,
						Utility.getContext(this, vars, "#User_Client", "ReportLibroDiario"),
						Utility.getContext(this, vars, "#User_Org", "ReportLibroDiario"), strDateFrom, strDateTo);
			}

			if (log4j.isDebugEnabled())
				log4j.debug("Calculating tree...");

			data = calculateTree(data, null, new Vector<Object>());

			/* AGF */
			data = addFilter(data, strLevel);
			/* AGF */
			/*
			 * Este fitro ya no se utiliza por que creamos el otro que ademas añade las
			 * cuentas que han tenido movimiento data = levelFilter(data, null, false,
			 * strLevel);
			 */
			data = dataFilter(data);

			/* SUMATORIA TOTAL INICIO - AGF */
			data2 = levelFilter(data, null, false, "E");
			NumberFormat nf = NumberFormat.getInstance();

			for (int i = 0; i < data2.length; i++) {
				v_amtacctdr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctdr)).add(v_amtacctdr);
				v_amtacctcr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctcr)).add(v_amtacctcr);
				v_saldoinicial = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoInicial)).add(v_saldoinicial);
				v_Saldofinal = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoFinal)).add(v_Saldofinal);
			}

			v_Samtacctdr = String.valueOf(nf.format(v_amtacctdr));
			v_Samtacctcr = String.valueOf(nf.format(v_amtacctcr));
			v_Ssaldoinicial = String.valueOf(nf.format(v_saldoinicial));
			v_Ssaldofinal = String.valueOf(nf.format(v_Saldofinal));

			/* SUMATORIA TOTAL FINAL - AGF */

			if (log4j.isDebugEnabled())
				log4j.debug("Tree calculated");
		}
		ReportLibroDiarioData[] new_data = null;
		/*
		 * AJG Este filtro bloquea la funcion addFilter, por que quita los campos que
		 * incluimos en esta funcion if (strOnly.equals("-1") && data!=null &&
		 * data.length>0)new_data = filterTree(data, strLevel); else
		 */
		new_data = data;

		/* AJG Funcion para el filtro de las cuentas INICIO */
		new_data = AccountFilter(data, null, false, strAccountFrom, strAccountTo);
		/* AJG Funcion para el filtro de las cuentas INICIO */
		if (log4j.isDebugEnabled())
			log4j.debug("Creating xmlengine");
		xmlDocument = xmlEngine
				.readXmlTemplate("com/openbravofreelancer/comatel/accounting/ad_reports/ReportLibroDiario", discard)
				.createXmlDocument();
		try {
			ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "", "C_ElementValue level", "",
					Utility.getContext(this, vars, "#User_Org", "ReportLibroDiario"),
					Utility.getContext(this, vars, "#User_Client", "ReportLibroDiario"), 0);
			Utility.fillSQLParameters(this, vars, null, comboTableData, "ReportLibroDiario", "");
			xmlDocument.setData("reportLevel", "liststructure", comboTableData.select(false));
			comboTableData = null;
		} catch (Exception ex) {
			throw new ServletException(ex);
		}

		ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ReportLibroDiario", false, "", "",
				"imprimir();return false;", false, "ad_reports", strReplaceWith, false, true);
		toolbar.prepareSimpleToolBarTemplate();

		xmlDocument.setParameter("toolbar", toolbar.toString());

		try {
			WindowTabs tabs = new WindowTabs(this, vars,
					"com.openbravofreelancer.comatel.accounting.ad_reports.ReportLibroDiario");
			xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
			xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
			xmlDocument.setParameter("childTabContainer", tabs.childTabs());
			xmlDocument.setParameter("theme", vars.getTheme());
			NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ReportLibroDiario.html", classInfo.id,
					classInfo.type, strReplaceWith, tabs.breadcrumb());
			xmlDocument.setParameter("navigationBar", nav.toString());
			LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "ReportLibroDiario.html", strReplaceWith);
			xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		{
			OBError myMessage = vars.getMessage("ReportLibroDiario");
			vars.removeMessage("ReportLibroDiario");

			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		xmlDocument.setData("reportAccountFrom_ID", "liststructure",
				ReportLibroDiarioData.selectAccount(this, Utility.getContext(this, vars, "#User_Org", "Account"),
						Utility.getContext(this, vars, "#User_Client", "Account"), ""));
		xmlDocument.setData("reportAccountTo_ID", "liststructure",
				ReportLibroDiarioData.selectAccount(this, Utility.getContext(this, vars, "#User_Org", "Account"),
						Utility.getContext(this, vars, "#User_Client", "Account"), ""));
		xmlDocument.setData("reportAD_ORGID", "liststructure", OrganizationComboData.selectCombo(this, vars.getRole()));
		xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
		xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
		xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("dateFrom", strDateFrom);
		xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTo", strDateTo);
		xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("Only", strOnly);
		xmlDocument.setParameter("adOrgId", strOrg);
		xmlDocument.setParameter("Level", strLevel);
		xmlDocument.setParameter("accountFrom", strAccountFrom);
		xmlDocument.setParameter("accountTo", strAccountTo);

		/* Envia los Totales - AGF */
		xmlDocument.setParameter("TotalAmtacctdr", v_Samtacctdr);
		xmlDocument.setParameter("TotalAmtacctcr", v_Samtacctcr);
		xmlDocument.setParameter("TotalSaldoInicial", v_Ssaldoinicial);
		xmlDocument.setParameter("TotalSaldoFinal", v_Ssaldofinal);
		/* Envia los totales - AGF */

		xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage + "');"));
		xmlDocument.setParameter("paramAll0", strAll.equals("") ? "0" : "1");
		xmlDocument.setData("reportCBPartnerId_IN", "liststructure",
				SelectorUtilityData.selectBpartner(this, Utility.getContext(this, vars, "#AccessibleOrgTree", ""),
						Utility.getContext(this, vars, "#User_Client", ""), strcBpartnerIdAux));

		if (log4j.isDebugEnabled())
			log4j.debug("filling structure, data.length:" + new_data.length);
		if (discard[1].equals("sectionNoBP")) {
			if (log4j.isDebugEnabled())
				log4j.debug("without BPs");
			xmlDocument.setData("structure2", new_data);
		} else {
			if (log4j.isDebugEnabled())
				log4j.debug("with BPs");
			if (strOnly.equals("-1"))
				Arrays.sort(new_data, new ReportLibroDiarioComparitor());
			for (int i = 0; i < new_data.length; i++) {
				new_data[i].rownum = "" + i;
			}
			/* ORDENAR COMIENZO */
			Arrays.sort(new_data, new ReportLibroDiarioComparitor());
			/* ORDENAR FIN */

			if (log4j.isDebugEnabled())
				log4j.debug("Rownum calculated");
			xmlDocument.setData("structure1", new_data);

		}
		out.println(xmlDocument.print());
		out.close();
	}

	void printPageDataPDF(HttpServletResponse response, VariablesSecureApp vars, String strDateFrom, String strDateTo,
			String strOrg, String strLevel, String strOnly, String strAccountFrom, String strAccountTo, String strAll,
			String strcBpartnerId) throws IOException, ServletException {
		BigDecimal v_amtacctdr = new BigDecimal("0.0");
		BigDecimal v_amtacctcr = new BigDecimal("0.0");
		BigDecimal v_saldoinicial = new BigDecimal("0.0");
		BigDecimal v_Saldofinal = new BigDecimal("0.0");

		String v_Samtacctdr = "";
		String v_Samtacctcr = "";
		String v_Ssaldoinicial = "";
		String v_Ssaldofinal = "";

		ReportLibroDiarioData[] data = null;
		ReportLibroDiarioData[] data2 = null;

		String strTreeOrg = ReportLibroDiarioData.treeOrg(this, vars.getClient());
		String strOrgFamily = getFamily(strTreeOrg, strOrg);
		String strTreeAccount = ReportLibroDiarioData.treeAccount(this, vars.getClient());
		String strcBpartnerIdAux = strcBpartnerId;

		data = ReportLibroDiarioData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount, strOrgFamily,
				Utility.getContext(this, vars, "#User_Client", "ReportLibroDiario"),
				Utility.getContext(this, vars, "#User_Org", "ReportLibroDiario"), strDateFrom, strDateTo);

		data = calculateTree(data, null, new Vector<Object>());
		data = addFilter(data, strLevel);
		/*
		 * Este fitro ya no se utiliza por que creamos el otro que ademas añade las
		 * cuentas que han tenido movimiento data = levelFilter(data, null, false,
		 * strLevel);
		 */
		data = dataFilter(data);
		/* SUMATORIA TOTAL INICIO - AGF */
		data2 = levelFilter(data, null, false, "E");
		NumberFormat nf = NumberFormat.getInstance();

		for (int i = 0; i < data2.length; i++) {
			v_amtacctdr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctdr)).add(v_amtacctdr);
			v_amtacctcr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctcr)).add(v_amtacctcr);
			v_saldoinicial = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoInicial)).add(v_saldoinicial);
			v_Saldofinal = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoFinal)).add(v_Saldofinal);
		}

		v_Samtacctdr = String.valueOf(nf.format(v_amtacctdr));
		v_Samtacctcr = String.valueOf(nf.format(v_amtacctcr));
		v_Ssaldoinicial = String.valueOf(nf.format(v_saldoinicial));
		v_Ssaldofinal = String.valueOf(nf.format(v_Saldofinal));

		/* SUMATORIA TOTAL FINAL - AGF */

		/* AJG Funcion para el filtro de las cuentas INICIO */
		data = AccountFilter(data, null, false, strAccountFrom, strAccountTo);
		/* AJG Funcion para el filtro de las cuentas INICIO */

		/* Envia los Totales - AGF */

		/*
		 * xmlDocument.setParameter("TotalAmtacctdr", v_Samtacctdr);
		 * xmlDocument.setParameter("TotalAmtacctcr", v_Samtacctcr);
		 * xmlDocument.setParameter("TotalSaldoInicial", v_Ssaldoinicial);
		 * xmlDocument.setParameter("TotalSaldoFinal", v_Ssaldofinal);
		 */
		/* Envia los totales - AGF */

		/* ORDENAR COMIENZO */
		Arrays.sort(data, new ReportLibroDiarioComparitor());
		/* ORDENAR FIN */
		String strSubtitle = Utility.messageBD(this, "Period", vars.getLanguage()) + ": " + strDateFrom + " - "
				+ strDateTo;

		String strOutput = vars.commandIn("PDF") ? "pdf" : "xls";
		String strReportName = "";

		strReportName = "@basedesign@/com/openbravofreelancer/comatel/accounting/ad_reports/ReportLibroDiario.jrxml";

		/************************
		 * 19-04-2010 2.4.0.0.2
		 *****************************************/
		// Tomamos el Nombre de cliente y el NIT.
		ReportLibroDiarioData[] org_data = null;
		org_data = ReportLibroDiarioData.selectCompany(this, vars.getClient());
		String strcompany = org_data[0].organizacion;
		String strnit = org_data[0].nit;
		/***********************************************************************************/

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("company", strcompany);
		parameters.put("nit", strnit);
		parameters.put("Title", classInfo.name);
		parameters.put("Subtitle", strSubtitle);
		/* PASA LOS TOTALES AL JRXML INICIO AGF */
		parameters.put("TOTALDEBITO", v_Samtacctdr);
		parameters.put("TOTALCREDITO", v_Samtacctcr);
		parameters.put("TOTALSALDOINICIAL", v_Ssaldoinicial);
		parameters.put("TOTALSALDOFINAL", v_Ssaldofinal);
		/* PASA LOS TOTALES AL JRXML FINAL AGF */
		renderJR(vars, response, strReportName, strOutput, parameters, data, null);

		/* AGF */
		/*
		 * LO QUE ESTA ACA ABJAO ES PARA EVNIAR EL ARCHIVO A UN DOCUMENTO .FO, PERO YA
		 * NO SE HACE POR QUE SE ENVIA POR JRXML
		 */

		/* if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet"); */
		/*
		 * response.setContentType("text/html; charset=UTF-8"); PrintWriter out =
		 * response.getWriter();
		 */
		/*
		 * String discard[]={"selEliminar","sectionBP"};
		 * 
		 * XmlDocument xmlDocument=null; String strTreeOrg =
		 * ReportLibroDiarioData2.treeOrg(this, vars.getClient()); String strOrgFamily =
		 * getFamily(strTreeOrg, strOrg); String strTreeAccount =
		 * ReportLibroDiarioData2.treeAccount(this, vars.getClient());
		 * ReportLibroDiarioData2 [] data = null; if (strDateFrom.equals("") &&
		 * strDateTo.equals("")) { xmlDocument = xmlEngine.readXmlTemplate(
		 * "org/openbravo/erpCommon/ad_reports/ReportTrialRazon2PDF",
		 * discard).createXmlDocument(); data = ReportLibroDiarioData2.set(); } else {
		 * if (!strLevel.equals("S")) discard[0] = "selEliminarField"; else discard[0] =
		 * "discard";
		 * 
		 * 
		 * if(strLevel.equals("S") && strOnly.equals("-1") ){
		 * 
		 * if (!(strAll.equals("")&&(strcBpartnerId.equals("")))) { if
		 * (log4j.isDebugEnabled())
		 * log4j.debug("Select BP, strcBpartnerId:"+strcBpartnerId+" - strAll:"+strAll);
		 * if (!strAll.equals("")) strcBpartnerId=""; discard[1] = "sectionNoBP"; data =
		 * ReportLibroDiarioData2.selectBP(this, strDateFrom, strDateTo, strOrg,
		 * strOrgFamily, Utility.getContext(this, vars, "#User_Client",
		 * "ReportLibroDiario"), Utility.getContext(this, vars, "#User_Org",
		 * "ReportLibroDiario"), strDateFrom, DateTimeData.nDaysAfter(this,
		 * strDateTo,"1"), strAccountFrom, strAccountTo,strcBpartnerId); } else { data =
		 * ReportLibroDiarioData2.select(this, strDateFrom, strDateTo, strOrg,
		 * strTreeAccount, strOrgFamily, Utility.getContext(this, vars, "#User_Client",
		 * "ReportLibroDiario"), Utility.getContext(this, vars, "#User_Org",
		 * "ReportLibroDiario"), strDateFrom, DateTimeData.nDaysAfter(this,
		 * strDateTo,"1"), strAccountFrom, strAccountTo); } }else{ data =
		 * ReportLibroDiarioData2.select(this, strDateFrom, strDateTo, strOrg,
		 * strTreeAccount, strOrgFamily, Utility.getContext(this, vars, "#User_Client",
		 * "ReportLibroDiario"), Utility.getContext(this, vars, "#User_Org",
		 * "ReportLibroDiario"), strDateFrom, DateTimeData.nDaysAfter(this,
		 * strDateTo,"1"),"",""); }
		 * 
		 * 
		 * data = calculateTree(data, null, new Vector<Object>()); data =
		 * levelFilter(data, null, false, strLevel); data = dataFilter(data); }
		 * ReportLibroDiarioData2 [] new_data = null; if (strOnly.equals("-1") &&
		 * data!=null && data.length>0)new_data = filterTree(data, strLevel); else
		 * new_data = data;
		 * 
		 * 
		 * 
		 * xmlDocument = xmlEngine.readXmlTemplate(
		 * "org/openbravo/erpCommon/ad_reports/ReportTrialRazon2PDF",
		 * discard).createXmlDocument(); xmlDocument.setParameter("companyName",
		 * ReportLibroDiarioData2.selectCompany(this, vars.getClient()));
		 * xmlDocument.setParameter("date", DateTimeData.today(this));
		 * xmlDocument.setParameter("period", strDateFrom + " - " + strDateTo); if
		 * (strLevel.equals("S")) xmlDocument.setParameter("accounting",
		 * "Cuenta inicio: "+ReportLibroDiarioData2.selectAccountingName(this,
		 * strAccountFrom)+" - Cuenta fin: "+ReportLibroDiarioData2.selectAccountingName
		 * (this, strAccountTo)); else xmlDocument.setParameter("accounting", "");
		 * 
		 * if (log4j.isDebugEnabled())
		 * log4j.debug("filling structure, data.length:"+new_data.length); if
		 * (log4j.isDebugEnabled()) log4j.debug("discard:"+discard[0]+","+discard[1]);
		 * 
		 * if (discard[1].equals("sectionNoBP")) xmlDocument.setData("structure2",
		 * new_data); else { if(strOnly.equals("-1")) Arrays.sort(new_data, new
		 * ReportLibroDiarioData2Comparator()); xmlDocument.setData("structure1",
		 * new_data); }
		 */
		/* ORDENAR COMIENZO */
		/* Arrays.sort(new_data, new ReportTrialRazonDataComparator2()); */
		/* ORDENAR FIN */

		/*
		 * String strResult = xmlDocument.print(); renderFO(strResult, response);
		 */
		/*
		 * out.println(xmlDocument.print()); out.close();
		 */
		/* AGF */
	}

	private ReportLibroDiarioData[] filterTree(ReportLibroDiarioData[] data, String strLevel) {
		ArrayList<Object> arrayList = new ArrayList<Object>();
		for (int i = 0; data != null && i < data.length; i++) {
			if (data[i].elementlevel.equals(strLevel))
				arrayList.add(data[i]);
		}
		ReportLibroDiarioData[] new_data = new ReportLibroDiarioData[arrayList.size()];
		arrayList.toArray(new_data);
		return new_data;
	}

	private ReportLibroDiarioData[] calculateTree(ReportLibroDiarioData[] data, String indice,
			Vector<Object> vecTotal) {
		if (indice == null)
			indice = "0";
		ReportLibroDiarioData[] result = null;
		Vector<Object> vec = new Vector<Object>();
		// if (log4j.isDebugEnabled())
		// log4j.debug("ReportTrialMajorBalanceData.calculateTree() - data: " +
		// data.length);
		if (vecTotal == null)
			vecTotal = new Vector<Object>();
		if (vecTotal.size() == 0) {
			vecTotal.addElement("0");
			vecTotal.addElement("0");
			vecTotal.addElement("0");
			vecTotal.addElement("0");
		}
		double totalDR = Double.valueOf((String) vecTotal.elementAt(0)).doubleValue();
		double totalCR = Double.valueOf((String) vecTotal.elementAt(1)).doubleValue();
		double totalInicial = Double.valueOf((String) vecTotal.elementAt(2)).doubleValue();
		double totalFinal = Double.valueOf((String) vecTotal.elementAt(3)).doubleValue();
		boolean encontrado = false;
		for (int i = 0; i < data.length; i++) {
			if (data[i].parentId.equals(indice)) {
				encontrado = true;
				Vector<Object> vecParcial = new Vector<Object>();
				vecParcial.addElement("0");
				vecParcial.addElement("0");
				vecParcial.addElement("0");
				vecParcial.addElement("0");
				ReportLibroDiarioData[] dataChilds = calculateTree(data, data[i].id, vecParcial);
				double parcialDR = Double.valueOf((String) vecParcial.elementAt(0)).doubleValue();
				double parcialCR = Double.valueOf((String) vecParcial.elementAt(1)).doubleValue();
				double parcialInicial = Double.valueOf((String) vecParcial.elementAt(2)).doubleValue();
				double parcialFinal = Double.valueOf((String) vecParcial.elementAt(3)).doubleValue();
				data[i].amtacctdr = Double.toString(Double.valueOf(data[i].amtacctdr).doubleValue() + parcialDR);
				data[i].amtacctcr = Double.toString(Double.valueOf(data[i].amtacctcr).doubleValue() + parcialCR);
				data[i].saldoInicial = Double
						.toString(Double.valueOf(data[i].saldoInicial).doubleValue() + parcialInicial);
				data[i].saldoFinal = Double.toString(Double.valueOf(data[i].saldoFinal).doubleValue() + parcialFinal);

				totalDR += Double.valueOf(data[i].amtacctdr).doubleValue();
				totalCR += Double.valueOf(data[i].amtacctcr).doubleValue();
				totalInicial += Double.valueOf(data[i].saldoInicial).doubleValue();
				totalFinal += Double.valueOf(data[i].saldoFinal).doubleValue();

				vec.addElement(data[i]);
				if (dataChilds != null && dataChilds.length > 0) {
					for (int j = 0; j < dataChilds.length; j++)
						vec.addElement(dataChilds[j]);
				}
			} else if (encontrado)
				break;
		}
		vecTotal.set(0, Double.toString(totalDR));
		vecTotal.set(1, Double.toString(totalCR));
		vecTotal.set(2, Double.toString(totalInicial));
		vecTotal.set(3, Double.toString(totalFinal));
		result = new ReportLibroDiarioData[vec.size()];
		vec.copyInto(result);
		return result;
	}

	private ReportLibroDiarioData[] dataFilter(ReportLibroDiarioData[] data) {
		if (data == null || data.length == 0)
			return data;
		Vector<Object> dataFiltered = new Vector<Object>();
		for (int i = 0; i < data.length; i++) {
			try {
				if (Double.valueOf(data[i].amtacctdr).doubleValue() != 0.0
						|| Double.valueOf(data[i].amtacctcr).doubleValue() != 0.0
						|| Double.valueOf(data[i].saldoInicial).doubleValue() != 0.0
						|| Double.valueOf(data[i].saldoFinal).doubleValue() != 0.0) {
					dataFiltered.addElement(data[i]);
				}
			} catch (Exception e) {
				System.out.println("Exception while processing " + data[i].amtacctdr + "Error is " + e);
			}
		}
		ReportLibroDiarioData[] result = new ReportLibroDiarioData[dataFiltered.size()];
		dataFiltered.copyInto(result);
		return result;
	}

	/* AJG-- INICIO FUNCION DE LOS FILTROS DE LAS CUENTAS */

	private ReportLibroDiarioData[] AccountFilter(ReportLibroDiarioData[] data, String indice, boolean found,
			String strAccountFrom, String strAccountTo) {
		if (data == null || data.length == 0 || strAccountFrom == null || strAccountFrom.equals("")
				|| strAccountTo == null || strAccountTo.equals(""))
			return data;
		Vector<Object> dataFiltered = new Vector<Object>();
		/* INICIO Convierte a Long el Codigo de la cuenta que viene String */
		long DESDE = Long.parseLong(strAccountFrom);
		long HASTA = Long.parseLong(strAccountTo);
		/* FIN Convierte a Long el Codigo de la cuenta que viene String */
		for (int i = 0; i < data.length; i++) {
			if ((Long.parseLong(data[i].organizador) >= DESDE) && (Long.parseLong(data[i].organizador) <= HASTA)) {
				dataFiltered.addElement(data[i]);
			}
		}
		ReportLibroDiarioData[] result = new ReportLibroDiarioData[dataFiltered.size()];
		dataFiltered.copyInto(result);
		return result;
	}
	/* AJG-- FIN FUNCION DE LOS FILTROS DE LAS CUENTAS */

	private ReportLibroDiarioData[] levelFilter(ReportLibroDiarioData[] data, String indice, boolean found,
			String strLevel) {
		if (data == null || data.length == 0 || strLevel == null || strLevel.equals(""))
			return data;
		ReportLibroDiarioData[] result = null;
		Vector<Object> vec = new Vector<Object>();
		// if (log4j.isDebugEnabled()) log4j.debug("ReportTrialMayorData.levelFilter() -
		// data: " + data.length);

		if (indice == null)
			indice = "0";
		for (int i = 0; i < data.length; i++) {
			if (data[i].parentId.equals(indice) && (!found || data[i].elementlevel.equalsIgnoreCase(strLevel))) {
				ReportLibroDiarioData[] dataChilds = levelFilter(data, data[i].id,
						(found || data[i].elementlevel.equals(strLevel)), strLevel);
				vec.addElement(data[i]);
				if (dataChilds != null && dataChilds.length > 0)
					for (int j = 0; j < dataChilds.length; j++)
						vec.addElement(dataChilds[j]);
			}
		}
		result = new ReportLibroDiarioData[vec.size()];
		vec.copyInto(result);
		vec.clear();
		return result;
	}

	private ReportLibroDiarioData[] addFilter(ReportLibroDiarioData[] data, String strLevel) {
		if (data == null || data.length == 0 || strLevel == null || strLevel.equals(""))
			return data;
		ReportLibroDiarioData[] result = null;
		Vector<Object> addFiltered = new Vector<Object>();

		if ((strLevel).equals("DBM_B")) {
			for (int i = 0; i < data.length; i++) {
				if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
						|| (data[i].elementlevel).equals("DBM_B") || (data[i].elementlevel).equals("S")
						|| (data[i].elementlevel).equals("C") || (data[i].elementlevel).equals("D")
						|| (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
					if (!(data[i].tipdoc).equals(".")
							|| !(data[i].parentId).equals("0") && !(data[i].elementlevel).equals("DBM_B")
									&& !(data[i].elementlevel).equals("S") && !(data[i].elementlevel).equals("C")
									&& !(data[i].elementlevel).equals("D") && !(data[i].elementlevel).equals("E")) {
						(data[i].accountId) = "";
						(data[i].name) = "";
						addFiltered.addElement(data[i]);
					} else {
						addFiltered.addElement(data[i]);
					}
				}
			}
		}

		if ((strLevel).equals("E")) {
			for (int i = 0; i < data.length; i++) {
				if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
						|| (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
					if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")) {
						(data[i].accountId) = "";
						(data[i].name) = "";
						addFiltered.addElement(data[i]);
					} else {
						addFiltered.addElement(data[i]);
					}

				}

			}
		}
		if ((strLevel).equals("C")) {
			for (int i = 0; i < data.length; i++) {
				if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
						|| (data[i].elementlevel).equals("C") || (data[i].elementlevel).equals("D")
						|| (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
					if (!(data[i].tipdoc).equals(".")
							|| !(data[i].parentId).equals("0") && !(data[i].elementlevel).equals("C")
									&& !(data[i].elementlevel).equals("D") && !(data[i].elementlevel).equals("E")) {
						(data[i].accountId) = "";
						(data[i].name) = "";
						addFiltered.addElement(data[i]);
					} else {
						addFiltered.addElement(data[i]);
					}
				}
			}
		}

		if ((strLevel).equals("D")) {
			for (int i = 0; i < data.length; i++) {
				if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
						|| (data[i].elementlevel).equals("D")
						|| (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
					if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")
							&& !(data[i].elementlevel).equals("D") && !(data[i].elementlevel).equals("E")) {
						(data[i].accountId) = "";
						(data[i].name) = "";
						addFiltered.addElement(data[i]);
					} else {
						addFiltered.addElement(data[i]);
					}
				}
			}
		}

		if ((strLevel).equals("S")) {
			for (int i = 0; i < data.length; i++) {
				if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
						|| (data[i].elementlevel).equals("S") || (data[i].elementlevel).equals("C")
						|| (data[i].elementlevel).equals("D")
						|| (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
					if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")
							&& !(data[i].elementlevel).equals("C") && !(data[i].elementlevel).equals("S")
							&& !(data[i].elementlevel).equals("D") && !(data[i].elementlevel).equals("E")) {
						(data[i].accountId) = "";
						(data[i].name) = "";
						addFiltered.addElement(data[i]);
					} else {
						addFiltered.addElement(data[i]);
					}
				}
			}
		}

		result = new ReportLibroDiarioData[addFiltered.size()];
		addFiltered.copyInto(result);
		addFiltered.clear();
		return result;
	}

	public String getFamily(String strTree, String strChild) throws IOException, ServletException {
		return Tree.getMembers(this, strTree, strChild);
		/*
		 * ReportGeneralLedgerData [] data =
		 * ReportGeneralLedgerData.selectChildren(this, strTree, strChild); String
		 * strFamily = ""; if(data!=null && data.length>0) { for (int i =
		 * 0;i<data.length;i++){ if (i>0) strFamily = strFamily + ","; strFamily =
		 * strFamily + data[i].id; } return strFamily; }else return "'1'";
		 */
	}

	public String getServletInfo() {
		return "Servlet ReportLibroDiario. This Servlet was made by Eduardo Argal";
	} // end of getServletInfo() method

}
