package com.openbravofreelancer.comatel.accounting.ad_reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.businessUtility.AccountingSchemaMiscData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.DateTimeData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

/**
 * @author shrinivas
 *
 */
public class JournalEntriesReport extends HttpSecureAppServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);

		if (log4j.isDebugEnabled())
			log4j.debug("Command: " + vars.getStringParameter("Command"));

		if (vars.commandIn("DEFAULT")) {
			String strcAcctSchemaId = vars.getGlobalVariable("inpcAcctSchemaId", "JournalEntriesReport|cAcctSchemaId",
					"");
			String strDateFrom = vars.getGlobalVariable("inpDateFrom", "JournalEntriesReport|DateFrom", "");
			String strDateTo = vars.getGlobalVariable("inpDateTo", "JournalEntriesReport|DateTo", "");
			String strDocument = vars.getGlobalVariable("inpDocument", "JournalEntriesReport|Document", "");
			String strOrg = vars.getGlobalVariable("inpOrg", "JournalEntriesReport|Org", "0");
			String strTable = vars.getStringParameter("inpTable");
			String strRecord = vars.getStringParameter("inpRecord");
			printPageDataSheet(response, vars, strDateFrom, strDateTo, strDocument, strOrg, strTable, strRecord, "",
					strcAcctSchemaId);
		} else if (vars.commandIn("DIRECT")) {
			String strTable = vars.getGlobalVariable("inpTable", "JournalEntriesReport|Table");
			String strRecord = vars.getGlobalVariable("inpRecord", "JournalEntriesReport|Record");
			setHistoryCommand(request, "DIRECT");
			vars.setSessionValue("JournalEntriesReport.initRecordNumber", "0");
			printPageDataSheet(response, vars, "", "", "", "", strTable, strRecord, "", "");
		} else if (vars.commandIn("DIRECT2")) {
			String strFactAcctGroupId = vars.getGlobalVariable("inpFactAcctGroupId",
					"JournalEntriesReport|FactAcctGroupId");
			setHistoryCommand(request, "DIRECT2");
			vars.setSessionValue("JournalEntriesReport.initRecordNumber", "0");
			printPageDataSheet(response, vars, "", "", "", "", "", "", strFactAcctGroupId, "");
		} else if (vars.commandIn("FIND")) {
			String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
					"JournalEntriesReport|cAcctSchemaId");
			String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom", "JournalEntriesReport|DateFrom");
			String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "JournalEntriesReport|DateTo");
			String strDocument = vars.getRequestGlobalVariable("inpDocument", "JournalEntriesReport|Document");
			String strOrg = vars.getGlobalVariable("inpOrg", "JournalEntriesReport|Org", "0");
			vars.setSessionValue("JournalEntriesReport.initRecordNumber", "0");
			setHistoryCommand(request, "DEFAULT");
			printPageDataSheet(response, vars, strDateFrom, strDateTo, strDocument, strOrg, "", "", "",
					strcAcctSchemaId);
		} else if (vars.commandIn("PDF", "XLS")) {
			if (log4j.isDebugEnabled())
				log4j.debug("PDF");
			String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
					"JournalEntriesReport|cAcctSchemaId");
			String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom", "JournalEntriesReport|DateFrom");
			String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "JournalEntriesReport|DateTo");
			String strDocument = vars.getRequestGlobalVariable("inpDocument", "JournalEntriesReport|Document");
			String strOrg = vars.getGlobalVariable("inpOrg", "JournalEntriesReport|Org", "0");
			String strTable = vars.getStringParameter("inpTable");
			String strRecord = vars.getStringParameter("inpRecord");
			vars.setSessionValue("JournalEntriesReport.initRecordNumber", "0");
			setHistoryCommand(request, "DEFAULT");
			printPagePDF(response, vars, strDateFrom, strDateTo, strDocument, strOrg, strTable, strRecord, "",
					strcAcctSchemaId);
		} else if (vars.commandIn("PREVIOUS_RELATION")) {
			String strInitRecord = vars.getSessionValue("JournalEntriesReport.initRecordNumber");
			String strRecordRange = Utility.getContext(this, vars, "#RecordRange", "JournalEntriesReport");
			int intRecordRange = strRecordRange.equals("") ? 0 : Integer.parseInt(strRecordRange);
			if (strInitRecord.equals("") || strInitRecord.equals("0"))
				vars.setSessionValue("JournalEntriesReport.initRecordNumber", "0");
			else {
				int initRecord = (strInitRecord.equals("") ? 0 : Integer.parseInt(strInitRecord));
				initRecord -= intRecordRange;
				strInitRecord = ((initRecord < 0) ? "0" : Integer.toString(initRecord));
				vars.setSessionValue("JournalEntriesReport.initRecordNumber", strInitRecord);
			}
			response.sendRedirect(strDireccion + request.getServletPath());
		} else if (vars.commandIn("NEXT_RELATION")) {
			String strInitRecord = vars.getSessionValue("JournalEntriesReport.initRecordNumber");
			String strRecordRange = Utility.getContext(this, vars, "#RecordRange", "JournalEntriesReport");
			int intRecordRange = strRecordRange.equals("") ? 0 : Integer.parseInt(strRecordRange);
			int initRecord = (strInitRecord.equals("") ? 0 : Integer.parseInt(strInitRecord));
			if (initRecord == 0)
				initRecord = 1;
			initRecord += intRecordRange;
			strInitRecord = ((initRecord < 0) ? "0" : Integer.toString(initRecord));
			vars.setSessionValue("JournalEntriesReport.initRecordNumber", strInitRecord);
			response.sendRedirect(strDireccion + request.getServletPath());
		} else
			pageError(response);
	}

	void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strDateFrom, String strDateTo,
			String strDocument, String strOrg, String strTable, String strRecord, String strFactAcctGroupId,
			String strcAcctSchemaId) throws IOException, ServletException {
		String strRecordRange = "500";
		BigDecimal v_credito = new BigDecimal("0.0");
		BigDecimal v_debito = new BigDecimal("0.0");
		int intRecordRange = (strRecordRange.equals("") ? 0 : Integer.parseInt(strRecordRange));
		String strInitRecord = vars.getSessionValue("JournalEntriesReport.initRecordNumber");
		int initRecordNumber = (strInitRecord.equals("") ? 0 : Integer.parseInt(strInitRecord));
		if (log4j.isDebugEnabled())
			log4j.debug("Output: dataSheet");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		XmlDocument xmlDocument = null;
		JournalEntriesReportData[] data = null;
		String DateAcct = "";
		String strPosition = "0";
		ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "JournalEntriesReport", false, "", "",
				"imprimir();return false;", false, "ad_reports", strReplaceWith, false, true);
		if (vars.commandIn("FIND")) {
			String strTreeOrg = JournalEntriesReportData.treeOrg(this, vars.getClient());
			String strOrgFamily = getFamily(strTreeOrg, strOrg);
			if (strRecord.equals("")) {
				data = JournalEntriesReportData.select(this,
						Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
						Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strDateFrom,
						DateTimeData.nDaysAfter(this, strDateTo, "1"), strDocument, strcAcctSchemaId, strOrgFamily,
						initRecordNumber, intRecordRange);
				if (data != null && data.length > 0) {
					DateAcct = data[0].dateacct.substring(0, 10);
					strPosition = JournalEntriesReportData.selectCount(this,
							Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
							Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strDateFrom,
							DateTimeData.nDaysAfter(this, strDateTo, "1"), strDocument, strcAcctSchemaId, strOrgFamily,
							DateAcct, data[0].identifier);
				}
			} else {
				data = JournalEntriesReportData.selectDirect(this,
						Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
						Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strTable, strRecord,
						initRecordNumber, intRecordRange);
				if (data != null && data.length > 0)
					strPosition = JournalEntriesReportData.selectCountDirect(this,
							Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
							Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strTable, strRecord,
							DateAcct, data[0].identifier);
			}
		} else if (vars.commandIn("DIRECT")) {
			data = JournalEntriesReportData.selectDirect(this,
					Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
					Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strTable, strRecord,
					initRecordNumber, intRecordRange);
			JournalEntriesReportData[] dataTotal = JournalEntriesReportData.get_TotalDoc(this, strRecord,
					strTable);

			if (data != null && data.length > 0) {
				v_credito = BigDecimal.valueOf(Double.valueOf(dataTotal[0].amtacctcr));
				v_debito = BigDecimal.valueOf(Double.valueOf(dataTotal[0].amtacctdr));
			}
			if (data != null && data.length > 0)
				strPosition = JournalEntriesReportData.selectCountDirect(this,
						Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
						Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strTable, strRecord,
						DateAcct, data[0].identifier);
		} else if (vars.commandIn("DIRECT2")) {
			data = JournalEntriesReportData.selectDirect2(this,
					Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
					Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strFactAcctGroupId,
					initRecordNumber, intRecordRange);
			if (data != null && data.length > 0)
				strPosition = JournalEntriesReportData.selectCountDirect2(this,
						Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
						Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strFactAcctGroupId,
						DateAcct, data[0].identifier);
		}
		if (vars.commandIn("DEFAULT")) {
			String discard[] = { "sectionSchema" };
			toolbar.prepareRelationBarTemplate(false, false,
					"submitCommandForm('XLS', false, null, 'JournalEntriesReport.html', 'EXCEL');return false;");
			xmlDocument = xmlEngine
					.readXmlTemplate("com/openbravofreelancer/comatel/accounting/ad_reports/JournalEntriesReport",
							discard)
					.createXmlDocument();
			data = JournalEntriesReportData.set();
			data[0].rownum = "0";
		} else {
			boolean hasPrevious = !(data == null || data.length == 0 || initRecordNumber <= 1);
			boolean hasNext = !(data == null || data.length == 0 || data.length < intRecordRange);
			toolbar.prepareRelationBarTemplate(hasPrevious, hasNext,
					"submitCommandForm('XLS', false, null, 'JournalEntriesReport.html', 'EXCEL');return false;");
			xmlDocument = xmlEngine
					.readXmlTemplate("com/openbravofreelancer/comatel/accounting/ad_reports/JournalEntriesReport")
					.createXmlDocument();
		}
		try {
			ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "", "C_DocType DocBaseType", "",
					Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"),
					Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"), 0);
			Utility.fillSQLParameters(this, vars, null, comboTableData, "JournalEntriesReport", strDocument);
			xmlDocument.setData("reportDocument", "liststructure", comboTableData.select(false));
			comboTableData = null;
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		xmlDocument.setParameter("toolbar", toolbar.toString());
		try {
			WindowTabs tabs = new WindowTabs(this, vars,
					"com.openbravofreelancer.comatel.accounting.ad_reports.JournalEntriesReport.JournalEntriesReport");
			xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
			xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
			xmlDocument.setParameter("childTabContainer", tabs.childTabs());
			xmlDocument.setParameter("theme", vars.getTheme());
			NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "JournalEntriesReport.html",
					classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
			xmlDocument.setParameter("navigationBar", nav.toString());
			LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "JournalEntriesReport.html",
					strReplaceWith);
			xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		{
			OBError myMessage = vars.getMessage("JournalEntriesReport");
			vars.removeMessage("JournalEntriesReport");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		if (!v_debito.equals(v_credito)) {
			BigDecimal v_dif = new BigDecimal(0);
			OBError myMessage = new OBError();
			v_dif = v_debito.subtract(v_credito);
			if ((v_dif.abs().compareTo(new BigDecimal(1)) < 0) && (data.length != 0)) {

				// Verifica cual es el que tiene el mayor valor (el debito o el credito) y le
				// resta la diferencia
				boolean restDebito = v_dif.signum() == 1;

				// Obtiene el valor mas grande de todos los creditos o debitos
				BigDecimal mayor = new BigDecimal(Integer.MIN_VALUE);
				int row = 0;

				for (int i = 0; i < data.length; i++) {
					JournalEntriesReportData actual = data[i];
					if (restDebito && actual.amtacctdr != null && actual.amtacctdr.length() > 0) {
						BigDecimal valorDebito = new BigDecimal(actual.amtacctdr);
						if (valorDebito.compareTo(mayor) > 0) {
							mayor = valorDebito;
							row = i;
						}
					} else if (!restDebito && actual.amtacctcr != null && actual.amtacctcr.length() > 0) {
						BigDecimal valorCredito = new BigDecimal(actual.amtacctcr);
						if (valorCredito.compareTo(mayor) > 0) {
							mayor = valorCredito;
							row = i;
						}
					}

				}

				// Al mayor le quita los decimales para cuadrar la cuenta
				if (restDebito) {
					data[row].amtacctdr = mayor.subtract(v_dif.abs()).toPlainString();
					JournalEntriesReportData.actualizarDebito(this, data[row].amtacctdr, data[row].factAcctId);
				} else {
					data[row].amtacctcr = mayor.subtract(v_dif.abs()).toPlainString();
					JournalEntriesReportData.actualizarCredito(this, data[row].amtacctcr, data[row].factAcctId);
				}

			} else {
				myMessage.setType("Warning");
				myMessage.setTitle("Warning");
				myMessage.setMessage("El total credito y debito es diferente, el documento ha sido descontabilizado ");

				JournalEntriesReportData.Fact_Acct_Reset(this, vars.getClient(), strTable, strRecord);
				vars.setMessage("JournalEntriesReport", myMessage);
			}
		}

		xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
		xmlDocument.setParameter("document", strDocument);
		xmlDocument.setParameter("cAcctschemaId", strcAcctSchemaId);

		ComboTableData comboTableData;
		try {
			comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_ORG_ID", "", "",
					Utility.getContext(this, vars, "#AccessibleOrgTree", "ReportTrialBalance"),
					Utility.getContext(this, vars, "#User_Client", "ReportTrialBalance"), '*');
			comboTableData.fillParameters(null, "ReportTrialBalance", "");
			xmlDocument.setData("reportAD_ORGID", "liststructure", comboTableData.select(false));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		xmlDocument.setData("reportC_ACCTSCHEMA_ID", "liststructure",
				AccountingSchemaMiscData.selectC_ACCTSCHEMA_ID(this,
						Utility.getContext(this, vars, "#AccessibleOrgTree", "ReportTrialBalance"),
						Utility.getContext(this, vars, "#User_Client", "ReportTrialBalance"), strcAcctSchemaId));
		xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
		xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("dateFrom", strDateFrom);
		xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTo", strDateTo);
		xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("adOrgId", strOrg);
		xmlDocument.setParameter("groupId", strPosition);
		xmlDocument.setParameter("paramRecord", strRecord);
		xmlDocument.setParameter("paramTable", strTable);
		xmlDocument.setData("structure1", data);
		out.println(xmlDocument.print());
		out.close();
	}

	void printPagePDF(HttpServletResponse response, VariablesSecureApp vars, String strDateFrom, String strDateTo,
			String strDocument, String strOrg, String strTable, String strRecord, String strFactAcctGroupId,
			String strcAcctSchemaId) throws IOException, ServletException {

		JournalEntriesReportData[] data = null;

		String strTreeOrg = JournalEntriesReportData.treeOrg(this, vars.getClient());
		String strOrgFamily = getFamily(strTreeOrg, strOrg);

		if (strRecord.equals(""))
			data = JournalEntriesReportData.select(this,
					Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
					Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strDateFrom,
					DateTimeData.nDaysAfter(this, strDateTo, "1"), strDocument, strcAcctSchemaId, strOrgFamily);
		else
			data = JournalEntriesReportData.selectDirect(this,
					Utility.getContext(this, vars, "#User_Client", "JournalEntriesReport"),
					Utility.getContext(this, vars, "#User_Org", "JournalEntriesReport"), strTable, strRecord);

		String strSubtitle = Utility.messageBD(this, "CompanyName", vars.getLanguage()) + ": "
				+ JournalEntriesReportData.selectCompany(this, vars.getClient());

		if (strDateFrom.equals("") && strDateTo.equals(""))
			strSubtitle += " - " + Utility.messageBD(this, "Period", vars.getLanguage()) + ": " + strDateFrom + " - "
					+ strDateTo;

		String strOutput = vars.commandIn("PDF") ? "pdf" : "xls";
		String strReportName = "@basedesign@/com/openbravofreelancer/comatel/accounting/ad_reports/JournalEntriesReport.jrxml";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("Title", classInfo.name);
		parameters.put("Subtitle", strSubtitle);
		renderJR(vars, response, strReportName, strOutput, parameters, data, null);
	}

	public String getFamily(String strTree, String strChild) throws IOException, ServletException {
		return Tree.getMembers(this, strTree, (strChild == null || strChild.equals("")) ? "0" : strChild);
		/*
		 * JournalEntriesReportData [] data =
		 * JournalEntriesReportData.selectChildren(this, strTree, strChild); String
		 * strFamily = ""; if(data!=null && data.length>0) { for (int i =
		 * 0;i<data.length;i++){ if (i>0) strFamily = strFamily + ","; strFamily =
		 * strFamily + data[i].id; } return strFamily += ""; }else return "'1'";
		 */
	}

	public String getServletInfo() {
		return "Servlet JournalEntriesReport. This Servlet was made by Pablo Sarobe modified by everybody";
	} // end of getServletInfo() method
}
