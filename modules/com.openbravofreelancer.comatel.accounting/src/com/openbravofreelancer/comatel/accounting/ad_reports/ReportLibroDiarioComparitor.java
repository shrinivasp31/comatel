package com.openbravofreelancer.comatel.accounting.ad_reports;

import java.util.Comparator;

public class ReportLibroDiarioComparitor implements Comparator<Object> {
	public int compare(Object obj1, Object obj2) {
		String code1 = ((ReportLibroDiarioData) obj1).organizador.toUpperCase();
		String code2 = ((ReportLibroDiarioData) obj2).organizador.toUpperCase();
		return code1.compareTo(code2);
	}
}
