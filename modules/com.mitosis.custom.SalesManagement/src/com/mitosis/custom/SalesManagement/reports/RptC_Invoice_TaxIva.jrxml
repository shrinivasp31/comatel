<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RptC_Invoice_TaxIva" pageWidth="193" pageHeight="24" orientation="Landscape" columnWidth="193" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="default" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="8"/>
	<style name="Report_Title" fontName="Bitstream Vera Sans" fontSize="18">
		<box leftPadding="5"/>
	</style>
	<style name="Report_Subtitle" forecolor="#555555" fontName="Bitstream Vera Sans" fontSize="14">
		<box leftPadding="5"/>
	</style>
	<style name="Report_Data_Label" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="Report_Data_Field" fontName="Bitstream Vera Sans" fontSize="11" isBold="false"/>
	<style name="Total_Field" mode="Opaque" forecolor="#000000" backcolor="#CCCCCC" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="11" isBold="true">
		<box leftPadding="5"/>
	</style>
	<style name="GroupHeader_DarkGray" mode="Opaque" forecolor="#FFFFFF" backcolor="#555555" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="12" isBold="true">
		<box leftPadding="5"/>
	</style>
	<style name="Group_Data_Label" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="Group_Data_Field" fontName="Bitstream Vera Sans" fontSize="11"/>
	<style name="Detail_Header" mode="Opaque" forecolor="#FFFFFF" backcolor="#5D5D5D" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="10" isBold="true">
		<box leftPadding="5">
			<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#FFFFFF"/>
			<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#FFFFFF"/>
		</box>
	</style>
	<style name="Detail_Line" fontName="Bitstream Vera Sans" fontSize="8">
		<conditionalStyle>
			<conditionExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()%2==1)]]></conditionExpression>
			<style mode="Opaque" backcolor="#CCCCCC"/>
		</conditionalStyle>
	</style>
	<style name="Total_Gray" mode="Opaque" forecolor="#000000" backcolor="#CCCCCC"/>
	<style name="Detail_Data_Label" mode="Opaque" backcolor="#CCCCCC" fontName="Bitstream Vera Sans" fontSize="10" isBold="true"/>
	<style name="Detail_Data_Field" mode="Opaque" backcolor="#CCCCCC" fontName="Bitstream Vera Sans" fontSize="10"/>
	<style name="Group_Footer" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="Report_Footer" isDefault="true" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="11"/>
	<parameter name="C_INVOICE_ID" class="java.lang.String"/>
	<parameter name="NUMBERFORMAT" class="java.text.DecimalFormat" isForPrompting="false"/>
	<parameter name="LOCALE" class="java.util.Locale" isForPrompting="false"/>
	<parameter name="TAXAMT" class="java.math.BigDecimal"/>
	<queryString>
		<![CDATA[SELECT COALESCE(SUM(C_INVOICETAX.TAXAMT), 0) AMT,
       C_INVOICETAX.C_INVOICE_ID,
       --C_TAX.NAME,
       C_TAX.RATE
--       (CASE WHEN C_TAX.IVA = 'Y' THEN C_TAX.RATE ELSE 0 END) AS RATE
  FROM C_INVOICETAX, C_TAX
 WHERE C_INVOICETAX.C_TAX_ID = C_TAX.C_TAX_ID
   AND C_INVOICETAX.C_INVOICE_ID =  $P{C_INVOICE_ID}
   AND C_TAX.EM_SM_ISIVA = 'Y'
 GROUP BY C_INVOICETAX.C_INVOICE_ID,
          --C_INVOICETAX.LINE,
          --C_INVOICETAX.TAXAMT,
          --C_TAX.NAME,
          C_TAX.RATE
 ORDER BY C_TAX.RATE]]>
	</queryString>
	<field name="AMT" class="java.math.BigDecimal"/>
	<field name="C_INVOICE_ID" class="java.math.BigDecimal"/>
	<field name="RATE" class="java.math.BigDecimal"/>
	<variable name="I_TAXAMT" class="java.math.BigDecimal" resetType="Group" resetGroup="C_INVOICE_ID">
		<variableExpression><![CDATA[$F{AMT}]]></variableExpression>
	</variable>
	<group name="C_INVOICE_ID">
		<groupExpression><![CDATA[$F{C_INVOICE_ID}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="2" splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="12" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField" style="default" x="0" y="0" width="112" height="12"/>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["I.V.A." + ($F{RATE}==null? new String("                   $"): $F{RATE} + "%                   $")]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Group" evaluationGroup="C_INVOICE_ID" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" style="default" x="119" y="0" width="73" height="12"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{AMT}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="12" splitType="Stretch">
			<textField evaluationTime="Group" evaluationGroup="C_INVOICE_ID" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-1" style="default" x="119" y="-1" width="73" height="12"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$P{TAXAMT}.add( $V{I_TAXAMT} )]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" style="default" x="0" y="-1" width="112" height="12"/>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["TOTAL NETO                    $"]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
