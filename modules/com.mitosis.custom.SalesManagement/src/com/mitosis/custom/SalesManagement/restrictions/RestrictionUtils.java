package com.mitosis.custom.SalesManagement.restrictions;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;

import com.mitosis.custom.SalesManagement.SM_Restriction;
import com.mitosis.custom.SalesManagement.SM_UserRestriction;

/**
 * 
 * @author Anbukkani G
 *
 */
public class RestrictionUtils {

	/**
	 * Checking for the given user has release permission for the given restriction
	 * 
	 * @param user
	 * @param restrictionType
	 * @return boolean
	 */
	public static boolean checkRestrictionForUser(User user, RestrictionType restrictionType) {
		OBContext.setAdminMode();
		boolean isRestrictionNeeded = true;
		OBCriteria<SM_Restriction> restrictionCriteria = OBDal.getInstance().createCriteria(SM_Restriction.class);
		restrictionCriteria.add(
				Restrictions.or(Restrictions.eq(SM_Restriction.PROPERTY_SEARCHKEY, restrictionType.getAbbreviation()),
						Restrictions.eq(SM_Restriction.PROPERTY_COMMERCIALNAME, restrictionType.getAbbreviation())));
		List<SM_Restriction> sm_Restrictions = restrictionCriteria.list();
		for (SM_Restriction sm_Restriction : sm_Restrictions) {
			OBCriteria<SM_UserRestriction> obCriteria = OBDal.getInstance().createCriteria(SM_UserRestriction.class);
			obCriteria.add(Restrictions.eq(SM_UserRestriction.PROPERTY_RESTRICTION, sm_Restriction));
			obCriteria.add(Restrictions.eq(SM_UserRestriction.PROPERTY_USERCONTACT, user));
			List<SM_UserRestriction> userRestrictions = obCriteria.list();
			if (userRestrictions.size() > 0) {
				if (userRestrictions.get(0).getStartingDate().compareTo(new Date()) == -1
						&& userRestrictions.get(0).getEndingDate().compareTo(new Date()) == 1) {
					isRestrictionNeeded = false;
				}
			}
		}
		OBContext.restorePreviousMode();
		return isRestrictionNeeded;
	}
}
