package com.mitosis.custom.SalesManagement.restrictions;

/**
 * Define enum for Restriction type these are used for user based restriction in
 * all transaction.
 * 
 * @author Anbukkani G
 * 
 */
public enum RestrictionType {

	RESTRICTION_RC_SIN_FACT("RC sin Fact"), RESTRICTION_CUPO("CUPO"), MU_TOTAL_RESTRICTION_BASED_ON_BP_CATEGORY(
			"M.U restriction based on bp category"), MSLV_BASED_RESTRICTION(
					"MSLV based purchase order restrictions"), PO_MINI_SALES_PRICE_RESTRICTION(
							"Purchase Order Minimum sales profit Restriction"), PENDING_SHIPMENT_PO_RESTRICTION(
									"Pending shipment based purchase order restriction"), PO_SALESORDER_RESTRICTION(
											"Restriction in purchase order based on sales order"), CARTERA(
													"Cartera");
	;
	String abbreviation;

	private RestrictionType(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

}
