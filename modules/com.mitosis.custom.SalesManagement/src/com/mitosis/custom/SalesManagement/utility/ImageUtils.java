package com.mitosis.custom.SalesManagement.utility;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBConfigFileProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.system.ClientInformation;
import org.openbravo.model.ad.utility.Image;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.utils.FileUtility;

public class ImageUtils {
  private static Logger log = Logger.getLogger(ImageUtils.class);

  public static BufferedImage showImageLogo(String logo, String org) throws IOException {
    return ImageIO.read(new ByteArrayInputStream(getImageLogo(logo, org)));
  }

  /**
   * Provides the image logo as a byte array for the indicated parameters.
   * 
   * @param logo
   *          The name of the logo to display This can be one of the following: yourcompanylogin,
   *          youritservicelogin, yourcompanymenu, yourcompanybig or yourcompanydoc
   * @param org
   *          The organization id used to get the logo In the case of requesting the yourcompanydoc
   *          logo you can indicate the organization used to request the logo.
   * @return The image requested
   */
  public static byte[] getImageLogo(String logo, String org) {
    OBContext.getOBContext().setAdminMode();
    byte[] imageByte;

    try {
      Image img = getImageLogoObject(logo, org);
      if (img == null) {
        String path = getDefaultImageLogo(logo);
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        new FileUtility(OBConfigFileProvider.getInstance().getServletContext().getRealPath("/"),
            path, false, true).dumpFile(bout);
        bout.close();
        imageByte = bout.toByteArray();
      } else {
        OBContext.setAdminMode(true);
        try {
          img = OBDal.getInstance().get(Image.class, img.getId());
          imageByte = img.getBindaryData();
        } catch (Throwable t) {
          System.err.println(t.getMessage());
          log.error("Could not load logo from database: " + logo + ", " + org, t);
          throw new OBException(t.getMessage());
        } finally {
          OBContext.restorePreviousMode();
        }
      }

    } catch (Exception e) {
      log.error("Could not load logo from database: " + logo + ", " + org, e);
      imageByte = getBlankImage();
    }
    OBContext.getOBContext().restorePreviousMode();
    return imageByte;
  }

  static Image getImageLogoObject(String strImageName, String strOrg) {
    Image img = null;
    String strImageId = null;
    if (strOrg != null && !strOrg.equalsIgnoreCase("00")) {
      Organization org = OBDal.getInstance().get(Organization.class, strOrg);
      OrganizationInformation orgInfo = org.getOrganizationInformationList().get(0);
      img = orgInfo.getYourCompanyDocumentImage();
      if ("CompanyDocumentImage1".equalsIgnoreCase(strImageName)) {
        img = orgInfo.getSmCompanyDocumentImage1();
      }
    } else {
      Client client = OBDal.getInstance().get(Client.class,
          OBContext.getOBContext().getCurrentClient().getId());
      ClientInformation clientInfo = client.getClientInformationList().get(0);
      img = clientInfo.getYourCompanyDocumentImage();
      if ("CompanyDocumentImage1".equalsIgnoreCase(strImageName)) {
        img = clientInfo.getSMYourCompanyDocumentImage1();

        strImageId = img.getId();
        Image image = OBDal.getInstance().get(Image.class, strImageId);
        return image;
      }
    }
    // img = OBDal.getInstance().get(Image.class, strImageId);
    return img;
  }

  /**
   * Provides the image logo as a byte array for the indicated parameters.
   * 
   * @param logo
   *          The name of the logo to display This can be one of the following: yourcompanylogin,
   *          youritservicelogin, yourcompanymenu, yourcompanybig or yourcompanydoc
   * @param org
   *          The organization id used to get the logo In the case of requesting the yourcompanydoc
   *          logo you can indicate the organization used to request the logo.
   * @return The image requested
   */
  public static String getDefaultImageLogo(String logo) {

    String defaultImagePath = null;

    if (logo == null) {
      defaultImagePath = "web/images/blank.gif";
    } else if ("yourcompanylogin".equals(logo)) {
      defaultImagePath = "web/images/CompanyLogo_big.png";
    } else if ("youritservicelogin".equals(logo)) {
      defaultImagePath = "web/images/SupportLogo_big.png";
    } else if ("yourcompanymenu".equals(logo)) {
      defaultImagePath = "web/images/CompanyLogo_small.png";
    } else if ("yourcompanybig".equals(logo)) {
      defaultImagePath = "web/skins/ltr/Default/Login/initialOpenbravoLogo.png";
    } else if ("yourcompanydoc".equals(logo)) {
      defaultImagePath = "web/images/CompanyLogo_big.png";
    } else if ("banner-production".equals(logo)) {
      defaultImagePath = "web/images/blank.gif";
    } else if ("yourcompanylegal".equals(logo)) {
      defaultImagePath = "web/images/CompanyLogo_big.png";
    } else {
      defaultImagePath = "web/images/blank.gif";
    }

    return defaultImagePath;

  }

  public static byte[] getBlankImage() {

    try {
      ByteArrayOutputStream bout = new ByteArrayOutputStream();
      new FileUtility(OBConfigFileProvider.getInstance().getServletContext().getRealPath("/"),
          getDefaultImageLogo("Empty"), false, true).dumpFile(bout);
      bout.close();
      return bout.toByteArray();
    } catch (IOException ex) {
      log.error("Could not load blank image.");
      return new byte[0];
    }
  }
}
