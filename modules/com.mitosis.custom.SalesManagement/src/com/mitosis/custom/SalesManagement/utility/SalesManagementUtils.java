package com.mitosis.custom.SalesManagement.utility;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.service.db.CallStoredProcedure;

/**
 * It's containing "Sales Management Customizations" module's general(commonly used) methods.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public class SalesManagementUtils {
  /**
   * Updating invoice reports Print Copies(Number of copies are already printed)
   * 
   * @author Anbukkani Gajendiran
   * @param invoiceId
   * @return String
   */
  public static String updateInvoicePrintCopy(String invoiceId) {
    Invoice invoice = OBDal.getInstance().get(Invoice.class, invoiceId);
    OBDal.getInstance().save(invoice);
    OBDal.getInstance().flush();
    return "";
  }

  public static DocumentType getDocumentType(String clientId, String orgId, String docBaseType) {
    return getDocumentType(clientId, orgId, docBaseType, null);
  }

  public static DocumentType getDocumentType(String clientId, String orgId, String docBaseType,
      String docSubType) {
    @SuppressWarnings("rawtypes")
    final List parametersForDocumentType = new ArrayList();
    parametersForDocumentType.add(clientId);
    parametersForDocumentType.add(orgId);
    parametersForDocumentType.add(docBaseType);
    if (docSubType != null) {
      parametersForDocumentType.add(docSubType);
    }
    final String procedureForDocumentType = "ad_get_doctype";
    final String documentTypeId = (String) CallStoredProcedure.getInstance().call(
        procedureForDocumentType, parametersForDocumentType, null, false);
    final DocumentType documentType = OBDal.getInstance().get(DocumentType.class, documentTypeId);
    return documentType;
  }
}
