package com.mitosis.custom.SalesManagement.utility;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.alert.Alert;
import org.openbravo.model.ad.alert.AlertRule;

public class AlertUtility {

  public static void CreateAlert(String description, AlertRule alertRule, String Referencekey_ID) {
    Alert alert = OBProvider.getInstance().get(Alert.class);
    alert.setAlertRule(alertRule);
    alert.setDescription(description);
    if (Referencekey_ID != null) {
      alert.setReferenceSearchKey(Referencekey_ID);
      alert.setRecordID(Referencekey_ID);
    } else {
      throw new OBException("Reference Search Key should not be null");
    }
    OBDal.getInstance().save(alert);
    OBDal.getInstance().flush();
  }

  public static AlertRule getAlertRule(String alertName) {
    if (alertName != null) {
      OBCriteria<AlertRule> alertRuleCriteria = OBDal.getInstance().createCriteria(AlertRule.class);
      alertRuleCriteria.add(Restrictions.eq(AlertRule.PROPERTY_NAME, alertName));
      List<AlertRule> alertRuleList = alertRuleCriteria.list();
      if (alertRuleList.size() > 0) {
        return alertRuleList.get(0);
      } else {
        throw new OBException("No AlertRule found in given name :" + alertName);
      }
    } else {
      throw new OBException("AlertRule name should not be null");
    }
  }

}
