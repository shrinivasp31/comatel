/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2014 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.mitosis.custom.SalesManagement.callouts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.OBMessageUtils;

public class SM_Order_ValidUntil extends SimpleCallout {
  private static final long serialVersionUID = 1L;
  private static final Logger log = Logger.getLogger(SM_Order_ValidUntil.class);

  @Override
  protected void execute(CalloutInfo info) throws ServletException {

    final String dateQuotation = info.vars.getStringParameter("inpdateordered");
    final String dateUntil = info.vars.getStringParameter("inpvaliduntil");
    SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
    try {
      Date fechaQuotation = formateador.parse(dateQuotation);
      Date fechaUntil = formateador.parse(dateUntil);

      if (fechaUntil.before(fechaQuotation)) {
        info.addResult("ERROR", OBMessageUtils.messageBD("SM_NotValidUntil"));

      }
    } catch (ParseException e) {
      log.error(e.getMessage());
    }

  }
}
