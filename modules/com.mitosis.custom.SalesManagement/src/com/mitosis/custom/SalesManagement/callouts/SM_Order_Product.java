package com.mitosis.custom.SalesManagement.callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.businessUtility.PAttributeSet;
import org.openbravo.erpCommon.businessUtility.PAttributeSetData;
import org.openbravo.erpCommon.businessUtility.PriceAdjustment;
import org.openbravo.erpCommon.businessUtility.Tax;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductByPriceAndWarehouse;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.xmlEngine.XmlDocument;

public class SM_Order_Product extends HttpSecureAppServlet {
	Logger log = Logger.getLogger(SM_Order_Product.class);
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) {
		super.init(config);
		boolHist = false;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);
		if (vars.commandIn("DEFAULT")) {
			String strChanged = vars.getStringParameter("inpLastFieldChanged");
			log4j.debug("CHANGED: " + strChanged);
			String strUOM = vars.getStringParameter("inpmProductId_UOM");
			String strPriceList = vars.getNumericParameter("inpmProductId_PLIST");
			String strPriceStd = vars.getNumericParameter("inpmProductId_PSTD");
			String strPriceLimit = vars.getNumericParameter("inpmProductId_PLIM");
			String strCurrency = vars.getStringParameter("inpmProductId_CURR");
			String strQty = vars.getNumericParameter("inpqtyordered");

			String strMProductID = vars.getStringParameter("inpmProductId");
			String strCBPartnerLocationID = vars.getStringParameter("inpcBpartnerLocationId");
			String strADOrgID = vars.getStringParameter("inpadOrgId");
			String strMWarehouseID = vars.getStringParameter("inpmWarehouseId");
			String strCOrderId = vars.getStringParameter("inpcOrderId");
			String strWindowId = vars.getStringParameter("inpwindowId");
			String strIsSOTrx = Utility.getContext(this, vars, "isSOTrx", strWindowId);
			String cancelPriceAd = vars.getStringParameter("inpcancelpricead");

			try {
				printPage(response, vars, strUOM, strPriceList, strPriceStd, strPriceLimit, strCurrency, strMProductID,
						strCBPartnerLocationID, strADOrgID, strMWarehouseID, strCOrderId, strIsSOTrx, strQty,
						cancelPriceAd);
			} catch (ServletException ex) {
				log.error(ex.getMessage());
				pageErrorCallOut(response);
			}
		} else
			pageError(response);
	}

	private void printPage(HttpServletResponse response, VariablesSecureApp vars, String _strUOM, String strPriceList,
			String _strPriceStd, String _strPriceLimit, String strCurrency, String strMProductID,
			String strCBPartnerLocationID, String strADOrgID, String strMWarehouseID, String strCOrderId,
			String strIsSOTrx, String strQty, String cancelPriceAd) throws IOException, ServletException {
		log4j.debug("Output: dataSheet");
		XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_callouts/CallOut")
				.createXmlDocument();
		String orderlineId = vars.getStringParameter("C_OrderLine_ID");
		String strPriceActual = "";
		String strHasSecondaryUOM = "";
		String strUOM = _strUOM;
		String strPriceLimit = _strPriceLimit;
		String strPriceStd = _strPriceStd;
		String strNetPriceList = strPriceList;
		String strGrossPriceList = strPriceList;
		String strGrossBaseUnitPrice = _strPriceStd;
		if (strPriceList.startsWith("\"")) {
			strNetPriceList = strPriceList.substring(1, strPriceList.length() - 1);
			strGrossPriceList = strPriceList.substring(1, strPriceList.length() - 1);
		}
		if (_strPriceStd.startsWith("\"")) {
			strPriceStd = _strPriceStd.substring(1, _strPriceStd.length() - 1);
		}
		boolean isTaxIncludedPriceList = OBDal.getInstance().get(Order.class, strCOrderId).getPriceList()
				.isPriceIncludesTax();

		if (!strMProductID.equals("")) {

			Order order = OBDal.getInstance().get(Order.class, strCOrderId);
			Product product = OBDal.getInstance().get(Product.class, strMProductID);

			if (!"Y".equals(cancelPriceAd)) {
				if (isTaxIncludedPriceList) {
					strPriceActual = PriceAdjustment
							.calculatePriceActual(order, product,
									"".equals(strQty) ? BigDecimal.ZERO : new BigDecimal(strQty),
									new BigDecimal(strGrossBaseUnitPrice.equals("") ? "0" : strGrossBaseUnitPrice))
							.toString();
					strNetPriceList = "0";
				} else {
					strPriceActual = PriceAdjustment.calculatePriceActual(order, product,
							"".equals(strQty) ? BigDecimal.ZERO : new BigDecimal(strQty),
							new BigDecimal(strPriceStd.equals("") ? "0" : strPriceStd)).toString();
					strGrossPriceList = "0";
				}
			} else {
				if (isTaxIncludedPriceList)
					strPriceActual = strGrossBaseUnitPrice;
				else
					strPriceActual = strPriceList;
			}
		} else {
			strUOM = strNetPriceList = strGrossPriceList = strPriceLimit = strPriceStd = "";
		}
		StringBuffer resultado = new StringBuffer();
		// Discount...
		BigDecimal discount = BigDecimal.ZERO;
		BigDecimal priceStd = null;
		if (isTaxIncludedPriceList) {
			BigDecimal priceList = (strGrossPriceList.equals("") ? BigDecimal.ZERO
					: new BigDecimal(strGrossPriceList.replace(",", "")));
			BigDecimal grossBaseUnitPrice = (strGrossBaseUnitPrice.equals("") ? BigDecimal.ZERO
					: new BigDecimal(strGrossBaseUnitPrice));
			if (priceList.compareTo(BigDecimal.ZERO) != 0) {
				discount = priceList.subtract(grossBaseUnitPrice).multiply(new BigDecimal("100")).divide(priceList, 2,
						BigDecimal.ROUND_HALF_UP);
			}
		} else {
			BigDecimal priceList = (strNetPriceList.equals("") ? BigDecimal.ZERO : new BigDecimal(strNetPriceList));
			priceStd = (strPriceStd.equals("") ? BigDecimal.ZERO : new BigDecimal(strPriceStd));
			if (priceList.compareTo(BigDecimal.ZERO) != 0) {
				discount = priceList.subtract(priceStd).multiply(new BigDecimal("100")).divide(priceList, 2,
						BigDecimal.ROUND_HALF_UP);
			}
		}

		resultado.append("var calloutName='SL_Order_Product';\n\n");
		resultado.append("var respuesta = new Array(");
		resultado.append("new Array(\"inpcUomId\", \"" + strUOM + "\"),");

		String strTreeOrg = SMOrderProductData.treeOrg(this, OBContext.getOBContext().getCurrentClient().getId());
		String orgs = Tree.getMembers(this, strTreeOrg, OBContext.getOBContext().getCurrentOrganization().getId());
		// Set Product search key in order line
		Product product = OBDal.getInstance().get(Product.class, strMProductID);
		resultado.append("new Array(\"inpemPmProValue\", \"" + product.getSearchKey() + "\"),\n");
		// Standard Product Name Process.
		if (product.isSmIsstandardProduct() != null) {
			resultado.append("new Array(\"inpemSmIsstandardProduct\", \"" + product.isSmIsstandardProduct() + "\"),");
		}
		// Append last purchase amount in order line
		String lastPurchaseAmount = SMOrderProductData.getLastPurchaseAmount(this, orgs, strMProductID);
		if (lastPurchaseAmount.equalsIgnoreCase("0")) {
			lastPurchaseAmount = null;
		}

		if (isTaxIncludedPriceList) {
			resultado.append(
					"new Array(\"inpgrossUnitPrice\", " + (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
			resultado.append("new Array(\"inpgrosspricelist\", "
					+ (strGrossPriceList.equals("") ? "0" : strGrossPriceList) + "),");
			resultado.append("new Array(\"inpgrosspricestd\", "
					+ (strGrossBaseUnitPrice.equals("") ? "0" : strGrossBaseUnitPrice) + "),");
			Organization org = OBDal.getInstance().get(Organization.class, vars.getStringParameter("inpadOrgId"));
			List<OrganizationInformation> orgInformationList = org.getOrganizationInformationList();
			if (orgInformationList.size() > 0) {
				BigDecimal salesProfitPercent = orgInformationList.get(0).getSmSalesProfitPercent();
				if (salesProfitPercent != null) {
					resultado.append("new Array(\"inpemSmPercentageMuTotal\", "
							+ (salesProfitPercent.equals("") ? "0" : salesProfitPercent) + "),");
					strPriceActual = (lastPurchaseAmount == null || lastPurchaseAmount.equals("")) ? "0"
							: lastPurchaseAmount;

					if (!salesProfitPercent.equals(new BigDecimal("0"))) {
						strPriceActual = "" + new BigDecimal(strPriceActual)
								.divide(new BigDecimal("1").subtract((salesProfitPercent.divide(new BigDecimal(100)))));
						// strPriceActual = ""
						// + new
						// BigDecimal(strPriceActual).add((salesProfitPercent
						// .divide(new BigDecimal(100))).multiply(new
						// BigDecimal(strPriceActual)));
						resultado.append("new Array(\"inppriceactual\", "
								+ (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
					} else {
						resultado.append("new Array(\"inppriceactual\", "
								+ (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
						// resultado.append("new Array(\"inppriceactual\", "
						// + (strPriceActual.equals("") ? "0" : strPriceActual)
						// + "),");
					}
				} else {
					resultado.append("new Array(\"inppriceactual\", "
							+ ((lastPurchaseAmount == null || lastPurchaseAmount.equals("")) ? "0" : lastPurchaseAmount)
							+ "),");
				}

			}
		} else {
			resultado.append(
					"new Array(\"inppricelist\", " + (strNetPriceList.equals("") ? "0" : strNetPriceList) + "),");
			resultado.append("new Array(\"inppricelimit\", " + (strPriceLimit.equals("") ? "0" : strPriceLimit) + "),");
			resultado.append("new Array(\"inppricestd\", " + (strPriceStd.equals("") ? "0" : strPriceStd) + "),");

			// Append Last purchase amount in order line
			// String lastPurchaseAmount =
			// SMOrderProductData.getLastPurchaseAmount(this, orgs,
			// strMProductID);
			// resultado.append("new Array(\"inpemSmLastPurchaseInvAmt\", " +
			// lastPurchaseAmount + "),");
			// // Append Last purchase date in order line
			// String lastPurchaseInvoiceDate =
			// SMOrderProductData.getLastPurchaseInvoiceDate(this, orgs,
			// strMProductID);
			// resultado.append("new Array(\"inpemSmLastPurchaseInvDate\", " +
			// lastPurchaseInvoiceDate
			// + "),");
			// Append Sales profit percentage in order line
			Organization org = OBDal.getInstance().get(Organization.class, vars.getStringParameter("inpadOrgId"));
			List<OrganizationInformation> orgInformationList = org.getOrganizationInformationList();
			if (orgInformationList.size() > 0) {
				BigDecimal SalesProfitPercent = orgInformationList.get(0).getSmSalesProfitPercent();
				if (SalesProfitPercent != null) {
					resultado.append("new Array(\"inpemSmPercentageMuTotal\", "
							+ (SalesProfitPercent.equals("") ? "0" : SalesProfitPercent) + "),");
					strPriceActual = (lastPurchaseAmount == null || lastPurchaseAmount.equals("")) ? "0"
							: lastPurchaseAmount;

					if (!SalesProfitPercent.equals(new BigDecimal("0"))) {
						strPriceActual = ""
								+ new BigDecimal(strPriceActual).add((SalesProfitPercent.divide(new BigDecimal(100)))
										.multiply(new BigDecimal(strPriceActual)));
						resultado.append("new Array(\"inppriceactual\", "
								+ (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
					} else {
						resultado.append("new Array(\"inppriceactual\", "
								+ (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
					}
				} else {
					resultado.append("new Array(\"inppriceactual\", "
							+ ((lastPurchaseAmount == null || lastPurchaseAmount.equals("")) ? "0" : lastPurchaseAmount)
							+ "),");
				}

			}
			// resultado.append("new Array(\"inppriceactual\", "
			// + (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
		}
		if (!"".equals(strCurrency)) {
			resultado.append("new Array(\"inpcCurrencyId\", \"" + strCurrency + "\"),");
		}

		log.info("Update % M.U total in sales order header.");
		// Update % M.U total in sales order header.
		updateHeaderMUPersentage(strCOrderId, strQty, orderlineId, strPriceActual, lastPurchaseAmount);

		resultado.append("new Array(\"inpemSmLastPurchaseInvAmt\", " + lastPurchaseAmount + "),");

		// Append last purchase date in order line
		String lastPurchaseInvoiceDate = SMOrderProductData.getLastPurchaseInvoiceDate(this, orgs, strMProductID);
		if (!lastPurchaseInvoiceDate.equalsIgnoreCase("0") && !lastPurchaseInvoiceDate.equalsIgnoreCase("")) {
			lastPurchaseInvoiceDate = getDate(lastPurchaseInvoiceDate);
			resultado.append("new Array(\"inpemSmLastPurchaseInvDate\", \"" + lastPurchaseInvoiceDate + "\"),");
		} else {
			resultado.append("new Array(\"inpemSmLastPurchaseInvDate\", " + null + "),");
		}
		// resultado.append("new Array(\"inpemSmLastPurchaseInvDate\", " +
		// lastPurchaseInvoiceDate +
		// "),");

		// Append last sales amount in order line
		Order order = OBDal.getInstance().get(Order.class, strCOrderId);
		String strBPartnerId = order.getBusinessPartner().getId();
		String lastSalesAmount = SMOrderProductData.getLastSalesAmount(this, orgs, strMProductID, strBPartnerId);
		if (lastSalesAmount.equalsIgnoreCase("0")) {
			lastSalesAmount = null;
		}
		resultado.append("new Array(\"inpemSmLastSalesInvAmt\", " + lastSalesAmount + "),");

		// Append last sales date in order line
		String lastSalesInvoiceDate = SMOrderProductData.getLastSalesInvoiceDate(this, orgs, strMProductID,
				strBPartnerId);

		if (!lastSalesInvoiceDate.equalsIgnoreCase("0") && !lastSalesInvoiceDate.equalsIgnoreCase("")) {
			lastSalesInvoiceDate = getDate(lastSalesInvoiceDate);
		} else {
			lastSalesInvoiceDate = "";
		}
		resultado.append("new Array(\"inpemSmLastSalesInvDate\", \"" + lastSalesInvoiceDate + "\"),");
		// resultado.append("new Array(\"inpemSmLastSalesInvDate\", " +
		// "01-01-2014" + "),");

		// Append available quantity

		Warehouse warehouse = OBDal.getInstance().get(Warehouse.class, strMWarehouseID);
		// Calculating product quantity availablity and onhand status.
		BigDecimal availableQty = new BigDecimal(0);
		BigDecimal onHandQty = new BigDecimal(0);
		OBCriteria<ProductByPriceAndWarehouse> productByPriceAndWarehouseCretria = OBDal.getInstance()
				.createCriteria(ProductByPriceAndWarehouse.class);
		productByPriceAndWarehouseCretria
				.add(Restrictions.and(Restrictions.eq(ProductByPriceAndWarehouse.PROPERTY_WAREHOUSE, warehouse),
						Restrictions.eq(ProductByPriceAndWarehouse.PROPERTY_PRODUCT, product)));
		List<ProductByPriceAndWarehouse> warehouseProductDetailsList = productByPriceAndWarehouseCretria.list();
		if (warehouseProductDetailsList.size() >= 0) {
			ProductByPriceAndWarehouse warehouseProductDetails = warehouseProductDetailsList.get(0);
			availableQty = warehouseProductDetails.getAvailable();
			onHandQty = warehouseProductDetails.getQtyOnHand();
		}

		// BigDecimal totalQty = getAvailableQty(strMProductID, warehouse);
		resultado.append("new Array(\"inpemSmInventory\", " + availableQty + "),");
		resultado.append("new Array(\"inpemSmQtyOnHand\", " + onHandQty + "),");

		resultado.append("new Array(\"inpdiscount\", " + discount.toString() + "),");
		if (!strMProductID.equals("")) {
			PAttributeSetData[] dataPAttr = PAttributeSetData.selectProductAttr(this, strMProductID);
			if (dataPAttr != null && dataPAttr.length > 0 && dataPAttr[0].attrsetvaluetype.equals("D")) {
				PAttributeSetData[] data2 = PAttributeSetData.select(this, dataPAttr[0].mAttributesetId);
				if (PAttributeSet.isInstanceAttributeSet(data2)) {
					resultado.append("new Array(\"inpmAttributesetinstanceId\", \"\"),");
					resultado.append("new Array(\"inpmAttributesetinstanceId_R\", \"\"),");
				} else {
					resultado.append("new Array(\"inpmAttributesetinstanceId\", \""
							+ dataPAttr[0].mAttributesetinstanceId + "\"),");
					resultado.append("new Array(\"inpmAttributesetinstanceId_R\", \""
							+ FormatUtilities.replaceJS(dataPAttr[0].description) + "\"),");
				}
			} else {
				resultado.append("new Array(\"inpmAttributesetinstanceId\", \"\"),");
				resultado.append("new Array(\"inpmAttributesetinstanceId_R\", \"\"),");
			}
			resultado.append("new Array(\"inpattributeset\", \""
					+ FormatUtilities.replaceJS(dataPAttr[0].mAttributesetId) + "\"),\n");
			resultado.append("new Array(\"inpattrsetvaluetype\", \""
					+ FormatUtilities.replaceJS(dataPAttr[0].attrsetvaluetype) + "\"),\n");
			strHasSecondaryUOM = SMOrderProductData.hasSecondaryUOM(this, strMProductID);
			resultado.append("new Array(\"inphasseconduom\", " + strHasSecondaryUOM + "),\n");
		}

		String strCTaxID = "";
		String orgLocationID = SMOrderProductData.getOrgLocationId(this,
				Utility.getContext(this, vars, "#User_Client", "SLOrderProduct"), "'" + strADOrgID + "'");
		if (orgLocationID.equals("")) {
			resultado
					.append("new Array('MESSAGE', \""
							+ FormatUtilities
									.replaceJS(Utility.messageBD(this, "NoLocationNoTaxCalculated", vars.getLanguage()))
							+ "\"),\n");
		} else {
			SMOrderTaxData[] data = SMOrderTaxData.select(this, strCOrderId);
			strCTaxID = Tax.get(this, strMProductID, data[0].dateordered, strADOrgID, strMWarehouseID,
					(data[0].billtoId.equals("") ? strCBPartnerLocationID : data[0].billtoId), strCBPartnerLocationID,
					data[0].cProjectId, strIsSOTrx.equals("Y"), "Y".equals(data[0].iscashvat));
		}
		if (!strCTaxID.equals("")) {
			resultado.append("new Array(\"inpcTaxId\", \"" + strCTaxID + "\"),\n");
		}

		resultado.append("new Array(\"inpmProductUomId\", ");
		// if (strUOM.startsWith("\""))
		// strUOM=strUOM.substring(1,strUOM.length()-1);
		// String strmProductUOMId =
		// SLOrderProductData.strMProductUOMID(this,strMProductID,strUOM);
		if (vars.getLanguage().equals("en_US")) {
			FieldProvider[] tld = null;
			try {
				ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "", "M_Product_UOM", "",
						Utility.getContext(this, vars, "#AccessibleOrgTree", "SLOrderProduct"),
						Utility.getContext(this, vars, "#User_Client", "SLOrderProduct"), 0);
				Utility.fillSQLParameters(this, vars, null, comboTableData, "SLOrderProduct", "");
				tld = comboTableData.select(false);
				comboTableData = null;
			} catch (Exception ex) {
				log.error(ex.getMessage());
				throw new ServletException(ex);
			}

			if (tld != null && tld.length > 0) {
				resultado.append("new Array(");
				for (int i = 0; i < tld.length; i++) {
					resultado.append("new Array(\"" + tld[i].getField("id") + "\", \""
							+ FormatUtilities.replaceJS(tld[i].getField("name")) + "\", \"false\")");
					if (i < tld.length - 1) {
						resultado.append(",\n");
					}
				}
				resultado.append("\n)");
			} else {
				resultado.append("null");
			}
			resultado.append("\n),");
		} else {
			FieldProvider[] tld = null;
			try {
				ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "", "M_Product_UOM", "",
						Utility.getContext(this, vars, "#AccessibleOrgTree", "SLOrderProduct"),
						Utility.getContext(this, vars, "#User_Client", "SLOrderProduct"), 0);
				Utility.fillSQLParameters(this, vars, null, comboTableData, "SLOrderProduct", "");
				tld = comboTableData.select(false);
				comboTableData = null;
			} catch (Exception ex) {
				log.error(ex.getMessage());
				throw new ServletException(ex);
			}

			if (tld != null && tld.length > 0) {
				resultado.append("new Array(");
				for (int i = 0; i < tld.length; i++) {
					resultado.append("new Array(\"" + tld[i].getField("id") + "\", \""
							+ FormatUtilities.replaceJS(tld[i].getField("name")) + "\", \"false\")");
					if (i < tld.length - 1) {
						resultado.append(",\n");
					}
				}
				resultado.append("\n)");
			} else {
				resultado.append("null");
			}
			resultado.append("\n),");
		}
		resultado.append("new Array(\"EXECUTE\", \"displayLogic();\"),\n");
		// Para posicionar el cursor en el campo de cantidad
		resultado.append("new Array(\"CURSOR_FIELD\", \"inpqtyordered\")\n");
		if (!strHasSecondaryUOM.equals("0")) {
			resultado.append(", new Array(\"CURSOR_FIELD\", \"inpquantityorder\")\n");
		}

		resultado.append(");");
		xmlDocument.setParameter("array", resultado.toString());
		xmlDocument.setParameter("frameName", "appFrame");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println(xmlDocument.print());
		out.close();
	}

	/**
	 * Updating % M.U total in sales order header.
	 * 
	 * @param strCOrderId
	 * @param strQty
	 * @param orderlineId
	 * @param strPriceActual
	 * @param lastPurchaseAmount
	 * @author Anbukkani Gajendiran
	 */
	public void updateHeaderMUPersentage(String strCOrderId, String strQty, String orderlineId, String strPriceActual,
			String lastPurchaseAmount) {
		log.info("Inside updateHeaderMUPersentage");
		log.info("Inside updateHeaderMUPersentage Parameter Values : " + strCOrderId + "," + strQty + "," + orderlineId
				+ "," + strPriceActual + "," + lastPurchaseAmount);
		Order order = OBDal.getInstance().get(Order.class, strCOrderId);
		List<OrderLine> orderLineList = order.getOrderLineList();
		BigDecimal totalPurchaseAmount = new BigDecimal(0);
		BigDecimal totalSalesAmount = new BigDecimal(0);
		for (OrderLine orderLine : orderLineList) {
			log.info("Inside for loop");
			BigDecimal lastPurchasePrice = orderLine.getSmLastPurchaseInvAmt();
			BigDecimal salesPrice = orderLine.getUnitPrice();
			BigDecimal qty = orderLine.getOrderedQuantity();
			log.debug("orderLine: " + orderLine.getId() + " - ordeLineId: " + orderlineId);
			if (orderLine.getId().equalsIgnoreCase(orderlineId)) {
				continue;
			}
			BigDecimal purchaseAmount = BigDecimal.ONE;
			if (lastPurchasePrice != null) {
				purchaseAmount = qty.multiply(lastPurchasePrice);
			}
			BigDecimal salesAmount = BigDecimal.ONE;
			if (salesAmount != null)
				salesAmount = qty.multiply(salesPrice);
			totalPurchaseAmount = totalPurchaseAmount.add(purchaseAmount);
			totalSalesAmount = totalSalesAmount.add(salesAmount);
			log.info("Loop values totalPurchaseAmount :" + totalPurchaseAmount + " totalPurchaseAmount : "
					+ totalPurchaseAmount);
		}

		BigDecimal lastPurchasePrice = (lastPurchaseAmount != null && !"".equalsIgnoreCase(lastPurchaseAmount))
				? new BigDecimal(lastPurchaseAmount.replace(",", ""))
				: new BigDecimal(0);
		BigDecimal salesPrice = new BigDecimal(strPriceActual.replace(",", ""));

		BigDecimal qty = new BigDecimal(strQty);
		BigDecimal purchaseAmount = qty.multiply(lastPurchasePrice);
		BigDecimal salesAmount = qty.multiply(salesPrice);
		totalPurchaseAmount = totalPurchaseAmount.add(purchaseAmount);
		totalSalesAmount = totalSalesAmount.add(salesAmount);

		log.debug("totalPurchaseAmount: " + totalPurchaseAmount + " - totalSalesAmount: " + totalSalesAmount);
		if (totalPurchaseAmount != null && totalSalesAmount != null && !totalPurchaseAmount.equals(new BigDecimal(0))
				&& !totalSalesAmount.equals(new BigDecimal(0))) {
			BigDecimal resultValue = totalPurchaseAmount.divide(totalSalesAmount, 4, BigDecimal.ROUND_HALF_UP);
			log.debug("resultValue: " + resultValue);
			BigDecimal headerSalesProfitPercent = new BigDecimal(1).subtract(resultValue);
			headerSalesProfitPercent = headerSalesProfitPercent.multiply(new BigDecimal(100));
			log.info(" headerSalesProfitPercent :" + headerSalesProfitPercent);
			order.setSMMUTotal(headerSalesProfitPercent);
			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
		}
	}

	// private BigDecimal getAvailableQty(String strMProductID, Warehouse
	// warehouse) {
	// List<Locator> locatorList = warehouse.getLocatorList();
	// BigDecimal totalQty = new BigDecimal(0);
	// for (Locator locator : locatorList) {
	// List<StorageDetail> storageDetailList =
	// locator.getMaterialMgmtStorageDetailList();
	// for (StorageDetail storageDetail : storageDetailList) {
	// if (storageDetail.getProduct().getId().equalsIgnoreCase(strMProductID)) {
	// totalQty = totalQty.add(storageDetail.getQuantityOnHand());
	// }
	// }
	// }
	// return totalQty;
	// }

	public String getDate(String strDate) {
		String str = strDate;

		int date = Integer.parseInt(str.substring(8, 10));

		int month = Integer.parseInt(str.substring(5, 7));

		int year = Integer.parseInt(str.substring(0, 4));

		Date d = new Date(year - 1900, month - 1, date);

		String DATE_FORMAT = "dd-MM-yyyy";
		// Create object of SimpleDateFormat and pass the desired date format.
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		log.info("Date is " + sdf.format(d));
		// info.addResult("inpdateacct",sdf.format(d));
		return sdf.format(d);
	}

}