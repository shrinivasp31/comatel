package com.mitosis.custom.SalesManagement.callouts;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;

/**
 * Set Actual price based on Sales profit margin
 * 
 * @author Anbukkani Gajendiran
 * 
 */
public class SM_SalesProfitPercentage extends SimpleCallout {
  Logger log = Logger.getLogger(SM_SalesProfitPercentage.class);

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    try {
      String lastFieldChanged = info.vars.getStringParameter("inpLastFieldChanged");
      if (lastFieldChanged.equalsIgnoreCase("inpemSmPercentageMuTotal")) {
        String strProductId = info.vars.getStringParameter("inpmProductId");
        if (strProductId != null && !strProductId.equalsIgnoreCase("")) {
          String strPriceActual = info.vars.getStringParameter("inppriceactual");
          String strPriceListPrice = info.vars.getStringParameter("inppricelist");
          String strSalesProfitPercentage = info.vars
              .getStringParameter("inpemSmPercentageMuTotal");
          if (strPriceListPrice != null && !strPriceListPrice.equalsIgnoreCase("")) {
            String newPrice = strPriceListPrice;
            if (strSalesProfitPercentage != null && !strSalesProfitPercentage.equalsIgnoreCase("")
                && !strPriceListPrice.equalsIgnoreCase("0")) {
              newPrice = ""
                  + new BigDecimal(strPriceListPrice).add(new BigDecimal(strSalesProfitPercentage)
                      .divide(new BigDecimal(100)).multiply(new BigDecimal(strPriceListPrice)));
            }
            log.info("The price has been changed based on sales profit margin " + strPriceActual
                + " to " + newPrice);
            info.addResult("inppriceactual", newPrice);
          }
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new OBException(e.getMessage());
    }
  }
}
