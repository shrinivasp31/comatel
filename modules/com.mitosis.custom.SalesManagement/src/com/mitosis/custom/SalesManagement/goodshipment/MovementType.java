package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for MovementType types. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum MovementType {

  CUSTOMER_SHIPMENT("Customer Shipment", "C-"), CUSTOMER_RETURNS("Customer Returns", "C+"), VENDOR_RECEIPTS(
      "Vendor Receipts", "V+"), VENDOR_RETURNS("Vendor Returns", "V-"), INVENTORY_OUT(
      "Inventory Out", "I-"), INVENTORY_IN("Inventory In", "I+"), MOVEMENT_FROM("Movement From",
      "M-"), MOVEMENT_TO("Movement To", "M+"), PRODUCTION("Production +", "P+"), PRODUCTION_NEGATIVE(
      "Product -", "P-"), WORK_ORDER("Work Order +", "W+"), WORK_ORDER_NEGATIVE("Work Order -",
      "W-"), INTERNAL_CONSUMPTION("Internal Consumption +", "D+"), INTERNAL_CONSUMPTION_NEGATIVE(
      "Internal Consumption -", "D-");
  ;

  private String name;
  private String abbreviation;

  public String getName() {
    return name;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }

  public String toString() {
    return name;
  }

  private MovementType(String name, String abbreviation) {
    this.name = name;
    this.abbreviation = abbreviation;
  }
}
