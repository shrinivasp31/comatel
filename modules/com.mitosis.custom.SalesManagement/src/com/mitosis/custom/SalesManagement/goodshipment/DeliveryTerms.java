package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for DeliveryTerms. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum DeliveryTerms {
  AVAILABILITY("A");
  private String abbreviation;

  private DeliveryTerms(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }

}
