package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for FrieghtCostRule. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum FrieghtCostRule {
  FRIEGHT_INCLUDED("I");
  private String abbreviation;

  private FrieghtCostRule(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }
}
