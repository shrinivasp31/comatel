package com.mitosis.custom.SalesManagement.goodshipment;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.OrgWarehouse;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;

public class GoodsShipmentUtil {
  protected Logger logger = Logger.getLogger(this.getClass());

  public static BigDecimal getMovementQty(OrderLine salesOrderLine) throws SQLException {

    BigDecimal movementQty = new BigDecimal(0);
    movementQty = salesOrderLine.getOrderQuantity();
    return movementQty;
  }

  public static Locator findStorageBinWithSufficientInventory(OrderLine salesOrderLine,
      Organization organization) {
    Product product = salesOrderLine.getProduct();
    BigDecimal orderedQuantity = salesOrderLine.getOrderedQuantity();
    List<OrgWarehouse> organizationWarehouseList = organization.getOrganizationWarehouseList();
    final Map<String, Long> warehouseIdToPriorityMap = new HashMap<String, Long>();
    for (OrgWarehouse orgWarehouse : organizationWarehouseList) {
      warehouseIdToPriorityMap.put(orgWarehouse.getWarehouse().getId(), orgWarehouse.getPriority());
    }

    // let's find all storage bins that have sufficient inventory
    final OBCriteria<StorageDetail> criteria = OBDal.getInstance().createCriteria(
        StorageDetail.class);
    criteria.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT, product));
    criteria.add(Restrictions.ge(StorageDetail.PROPERTY_QUANTITYONHAND, orderedQuantity));
    criteria.add(Restrictions.ne("bin." + Locator.PROPERTY_SEARCHKEY, "dropship"));
    List<StorageDetail> listOfStorageDetail = criteria.list();

    if (listOfStorageDetail.size() == 0)
      return null;
    if (listOfStorageDetail.size() == 1)
      return listOfStorageDetail.get(0).getStorageBin();

    // if we have more than one storage bin returned, lets prioritize by warehouse priority
    Collections.sort(listOfStorageDetail, new Comparator<StorageDetail>() {

      @Override
      public int compare(StorageDetail o1, StorageDetail o2) {
        Long o1Priority = warehouseIdToPriorityMap.get(o1.getStorageBin().getWarehouse().getId());
        Long o2Priority = warehouseIdToPriorityMap.get(o2.getStorageBin().getWarehouse().getId());
        if (o1Priority == null) {
          if (o2Priority == null)
            return 0;
          else
            return -1;
        }
        return o1Priority.compareTo(o2Priority);
      }
    });

    return listOfStorageDetail.get(0).getStorageBin();
  }

  public static Locator getStorageBin(OrderLine salesOrderLine, Warehouse warehouse) {
    Product product = salesOrderLine.getProduct();
    BigDecimal orderedQuantity = salesOrderLine.getOrderedQuantity();
    Locator locator = null;
    final OBCriteria<StorageDetail> storageDetailList = OBDal.getInstance().createCriteria(
        StorageDetail.class);
    storageDetailList.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT, product));
    List<StorageDetail> listOfStorageDetail = storageDetailList.list();
    for (StorageDetail storageDetail : listOfStorageDetail) {
      if (!"dropship".equalsIgnoreCase(storageDetail.getStorageBin().getSearchKey()))
        if (storageDetail.getQuantityOnHand().compareTo(orderedQuantity) >= 0) {
          if (storageDetail.getStorageBin().getWarehouse().equals(warehouse)) {
            locator = storageDetail.getStorageBin();
            break;
          }
        }
    }
    return locator;
  }
}