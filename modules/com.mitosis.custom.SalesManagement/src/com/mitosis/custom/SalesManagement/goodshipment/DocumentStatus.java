package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for DocumentStatus. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum DocumentStatus {

  DRAFT("DR"), COMPLETE("CO");
  private String abbreviation;

  private DocumentStatus(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }
}
