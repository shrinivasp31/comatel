package com.mitosis.custom.SalesManagement.goodshipment;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.db.DalConnectionProvider;

import com.mitosis.custom.SalesManagement.utility.SalesManagementUtils;

/**
 * Creating Goods Shipment from order.
 * 
 * @author Anbukkani Gajendiran
 *
 */

public class CreateGoodsShipment {
  private static Logger logger = Logger.getLogger(CreateGoodsShipment.class);
  private static final String SHIPMENT_POST_PROCEDURE_NAME = "M_InOut_Post0";

  /**
   * Create or get existing GoodsShipment
   * 
   * @param salesOrder
   * @param warehouse
   * @return ShipmentInOut
   */
  public static ShipmentInOut getOrCreateShipment(Order salesOrder, Warehouse warehouse) {
    ShipmentInOut goodShipment = null;
    try {
      List<ShipmentInOut> shipmentInOutList = salesOrder.getMaterialMgmtShipmentInOutList();
      if (shipmentInOutList != null && shipmentInOutList.size() > 0) {
        goodShipment = shipmentInOutList.get(0);
      } else {
        goodShipment = OBProvider.getInstance().get(ShipmentInOut.class);

        final DocumentType docType = SalesManagementUtils.getDocumentType(salesOrder.getClient()
            .getId(), salesOrder.getOrganization().getId(), ShipmentDocumentType.DOC_BASE_TYPE
            .getAbbreviation());

        Date now = new Date();
        goodShipment.setOrganization(salesOrder.getOrganization());
        // TODO: Need to remove variable secure apps and analyse any defect if we remove
        VariablesSecureApp vars = new VariablesSecureApp(salesOrder.getCreatedBy().getId(),
            salesOrder.getClient().getId(), salesOrder.getOrganization().getId());
        goodShipment.setDocumentNo(getNextDocumentNo("M_InOut", docType.getId(), vars));
        goodShipment.setDocumentType(docType);
        goodShipment.setWarehouse(warehouse);
        goodShipment.setBusinessPartner(salesOrder.getBusinessPartner());
        goodShipment.setPartnerAddress(salesOrder.getPartnerAddress());
        goodShipment.setShippingCompany(salesOrder.getShippingCompany());
        goodShipment.setMovementDate(now);
        goodShipment.setAccountingDate(now);
        goodShipment.setClient(salesOrder.getClient());
        goodShipment.setSalesOrder(salesOrder);
        goodShipment.setDocumentAction(DocumentStatus.COMPLETE.getAbbreviation());
        goodShipment.setDocumentStatus(DocumentStatus.DRAFT.getAbbreviation());
        goodShipment.setMovementType(MovementType.CUSTOMER_SHIPMENT.getAbbreviation());
        goodShipment.setFreightCostRule(FrieghtCostRule.FRIEGHT_INCLUDED.getAbbreviation());
        goodShipment.setDeliveryTerms(DeliveryTerms.AVAILABILITY.getAbbreviation());
        goodShipment.setDeliveryMethod(DeliveryMethod.PICKUP.getAbbreviation());
        goodShipment.setSalesOrder(salesOrder);
        OBDal.getInstance().save(goodShipment);
        OBDal.getInstance().flush();
      }
      if (goodShipment.getMaterialMgmtShipmentInOutLineList().size() == 0) {
        Long lineNo = new Long(10);

        // Retrieving product(Item) order lines
        // TODO:Now orderLine list is null.
        List<OrderLine> orderLineList = salesOrder.getOrderLineList();
        if (orderLineList.size() == 0) {
          OBCriteria<OrderLine> orderCriteria = OBDal.getInstance().createCriteria(OrderLine.class);
          orderCriteria.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, salesOrder));
          orderLineList = orderCriteria.list();
        }
        // First inserting product lines
        String itemType = ProductType.ITEM.getAbbreviation();
        for (OrderLine productOrderLine : orderLineList) {
          if (!itemType.equals(productOrderLine.getProduct().getProductType()))
            continue;
          BigDecimal movementQty = productOrderLine.getOrderedQuantity();
          createShipmentInOutLine(salesOrder, productOrderLine, goodShipment, lineNo, movementQty);
          lineNo = lineNo + new Long(10);

        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
    return goodShipment;
  }

  public static boolean createShipmentInOutLine(Order salesOrder, OrderLine salesOrderLine,
      ShipmentInOut goodShipment, Long lineNo, BigDecimal movementQty) throws SQLException {
    boolean lineCreated = false;

    ShipmentInOutLine goodShipmentLine = OBProvider.getInstance().get(ShipmentInOutLine.class);

    goodShipmentLine.setClient(salesOrder.getClient());
    goodShipmentLine.setOrganization(salesOrder.getOrganization());
    goodShipmentLine.setShipmentReceipt(goodShipment);
    goodShipmentLine.setLineNo(lineNo);
    goodShipmentLine.setProduct(salesOrderLine.getProduct());
    goodShipmentLine.setUOM(salesOrderLine.getProduct().getUOM());
    goodShipmentLine.setMovementQuantity(movementQty);
    goodShipmentLine.setSalesOrderLine(salesOrderLine);
    Locator storageBin = GoodsShipmentUtil.getStorageBin(salesOrderLine,
        goodShipment.getWarehouse());
    if (storageBin == null) {
      storageBin = (goodShipment.getWarehouse() != null && goodShipment.getWarehouse()
          .getLocatorList().size() > 0) ? (goodShipment.getWarehouse().getLocatorList().get(0))
          : null;
    }
    if (storageBin == null)
      return false;
    goodShipmentLine.setStorageBin(storageBin);
    OBDal.getInstance().save(goodShipmentLine);
    OBDal.getInstance().flush();
    lineCreated = true;
    return lineCreated;
  }

  /**
   * Get next Document number
   * 
   * @param tableName
   * @param docTypeId
   * @param vars
   * @return DocumentNo as String
   */
  public static String getNextDocumentNo(String tableName, String docTypeId, VariablesSecureApp vars) {
    try {
      final String documentNo = Utility.getDocumentNo(
          new DalConnectionProvider(false).getConnection(), new DalConnectionProvider(), vars, "",
          tableName, docTypeId, docTypeId, false, true);
      return documentNo;
    } catch (Exception e) {
      logger.error("Exception in GoodShipment Creation Process--->"
          + "Could not generate next documentNo");
    }
    return null;
  }

  /**
   * Get Document Type
   * 
   * @param clientID
   * @param docBaseType
   * @return DocumentType
   */
  private static DocumentType getDocumentType(String clientID, String docBaseType) {

    OBQuery<DocumentType> documentTypeQuery = OBDal.getInstance()
        .createQuery(
            DocumentType.class,
            " as doctype where doctype.client='" + clientID + "'and doctype.name='" + docBaseType
                + "'");
    DocumentType documentType = documentTypeQuery.list().get(0);
    return documentType;
  }

}
