package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for Shipment Document types. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum ShipmentDocumentType {

  MATERIAL_DELIVERY("MM Shipment"), MATERIAL_RECEIVED("MM Receipt"), DOC_BASE_TYPE("MMS");
  private String abbreviation;

  private ShipmentDocumentType(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }

}
