package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for DeliveryMethod. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum DeliveryMethod {
  PICKUP("P"), DELIVERY("D"), SHIPPER("R");
  private String abbreviation;

  private DeliveryMethod(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }

}
