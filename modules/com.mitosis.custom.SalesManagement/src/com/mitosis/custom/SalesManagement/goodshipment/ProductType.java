package com.mitosis.custom.SalesManagement.goodshipment;

/**
 * enum for Product types. Its used for avoiding hard code in many place.
 * 
 * @author Anbukkani Gajendiran
 *
 */
public enum ProductType {
  ITEM("I"), SERVICE("S");
  private String abbreviation;

  private ProductType(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return this.abbreviation;
  }

}
