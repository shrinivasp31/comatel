package com.mitosis.custom.SalesManagement.process;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;
import org.openbravo.model.materialmgmt.onhandquantity.ReservationStock;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;

public class ReservedQtyReleaseProcess extends DalBaseProcess {
  protected void doExecute(ProcessBundle bundle) throws Exception {
    ProcessLogger processLogger = bundle.getLogger();
    OBCriteria<Order> orderCri = OBDal.getInstance().createCriteria(Order.class);
    orderCri.add(Restrictions.eq(Order.PROPERTY_DOCUMENTSTATUS, "CO"));
    // orderCri.add(Restrictions.eq(Order.PROPERTY_, orderCri));
    List<Order> orderList = orderCri.list();
    try {
      for (Order order : orderList) {
        if (order.getMaterialMgmtShipmentInOutList().size() == 0) {
          Long qtyReleaseDays = order.getOrganization().getSmQtyReleaseDays() != null ? order
              .getOrganization().getSmQtyReleaseDays() : 0;
          SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
          Date orderDate = order.getOrderDate();
          String strOrderDate = format.format(orderDate);
          // System.out.println("Before : " + strOrderDate);
          Calendar c = Calendar.getInstance();
          c.setTime(orderDate);
          c.add(Calendar.DATE, qtyReleaseDays.intValue());
          orderDate = c.getTime();
          String strOrderDate1 = format.format(orderDate);
          // System.out.println("After : " + strOrderDate1);
          Date date = new Date();
          String strToday = format.format(date);
          if (format.parse(strOrderDate1).compareTo(format.parse(strToday)) < 0) {
            List<OrderLine> orderLineList = order.getOrderLineList();
            for (OrderLine orderLine : orderLineList) {
              List<Reservation> reservationList = orderLine.getMaterialMgmtReservationList();
              for (Reservation reservation : reservationList) {
                List<ReservationStock> reservationStockList = reservation
                    .getMaterialMgmtReservationStockList();
                BigDecimal orderedQuantity = orderLine.getOrderedQuantity();
                for (ReservationStock reservationStock : reservationStockList) {
                  BigDecimal releaseQuantity = null;
                  if (orderedQuantity.compareTo(reservationStock.getQuantity()) <= 0) {
                    releaseQuantity = orderedQuantity;
                    orderedQuantity = new BigDecimal(0);
                  } else if (orderedQuantity.compareTo(reservationStock.getQuantity()) > 0) {
                    orderedQuantity = orderedQuantity.subtract(reservationStock.getQuantity());
                    releaseQuantity = reservationStock.getQuantity();
                  }
                  reservationStock.setReleased(releaseQuantity);
                  OBDal.getInstance().save(reservationStock);
                  OBDal.getInstance().flush();
                }
              }
            }
          }
        }
        // OBCriteria<ShipmentInOut>
        // inoutCri=OBDal.getInstance().createCriteria(ShipmentInOut.class);
        // inoutCri.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESORDER,order));
      }
    } catch (Exception e) {
      processLogger.log(e.getMessage());
      throw e;// Exception
    }
  }
}
