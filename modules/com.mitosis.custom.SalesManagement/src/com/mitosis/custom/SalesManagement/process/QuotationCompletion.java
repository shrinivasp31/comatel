package com.mitosis.custom.SalesManagement.process;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.alert.AlertRule;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.order.RejectReason;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.procurement.Requisition;
import org.openbravo.model.procurement.RequisitionLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.CallProcess;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

import com.mitosis.custom.SalesManagement.restrictions.RestrictionType;
import com.mitosis.custom.SalesManagement.restrictions.RestrictionUtils;
import com.mitosis.custom.SalesManagement.utility.AlertUtility;

public class QuotationCompletion extends DalBaseProcess {
	Logger log = Logger.getLogger(QuotationCompletion.class);

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {

		try {
			OBContext.setAdminMode();
			String msg = "";
			String orderID = (String) bundle.getParams().get("C_Order_ID");
			BigDecimal orderCreditLimit;
			BigDecimal orderTotalGrossAmount;
			String errorMessage;
			// To check total gross amount with credit line amount
			Order order = OBDal.getInstance().get(Order.class, orderID);

			boolean isStandardBP = (order.getBusinessPartner().isSMStandardBusinessPartner() != null
					&& order.getBusinessPartner().isSMStandardBusinessPartner() == true) ? true : false;
			if (order.isSalesTransaction() && isStandardBP) {
				OBError message = new OBError();
				message.setType("Error");
				message.setMessage(OBMessageUtils.messageBD("SM_StandardBusinessPartner"));
				bundle.setResult(message);
				OBDal.getInstance().rollbackAndClose();
				return;
			}
			if (order.getOrderLineList().size() == 0) {
				throw new OBException("Order Line should not be empty");
			}
			BusinessPartner businessPartner = order.getBusinessPartner();
			orderCreditLimit = (businessPartner.getCreditLimit() != null ? businessPartner.getCreditLimit()
					: new BigDecimal("0"))
							.subtract((businessPartner.getCreditUsed() != null ? businessPartner.getCreditUsed()
									: new BigDecimal("0")));
			orderTotalGrossAmount = order.getGrandTotalAmount();
			if (orderCreditLimit.compareTo(orderTotalGrossAmount) == -1 && RestrictionUtils
					.checkRestrictionForUser(OBContext.getOBContext().getUser(), RestrictionType.RESTRICTION_CUPO)) {
				msg = RestrictionType.RESTRICTION_CUPO.getAbbreviation();
			}
			if (msg.length() > 0) {
				errorMessage = String.format(Utility.messageBD(new DalConnectionProvider(), "SM_RestrictionError",
						OBContext.getOBContext().getLanguage().getLanguage()));
				throw new OBException(errorMessage);
			}
			// To check pending goods shipment for business partner
			OBCriteria<ShipmentInOut> goodShipmentCriteria = OBDal.getInstance().createCriteria(ShipmentInOut.class);
			// BusinessPartner businessPartner = order.getBusinessPartner();
			goodShipmentCriteria.add(Restrictions.eq(ShipmentInOut.PROPERTY_BUSINESSPARTNER, businessPartner));
			goodShipmentCriteria.add(Restrictions.eq(ShipmentInOut.PROPERTY_DOCUMENTSTATUS, "CO"));
			List<ShipmentInOut> goodShipmentList = goodShipmentCriteria.list();
			// Checking the good shipment creation date and restrict the sales
			// order.
			Organization organization = OBContext.getOBContext().getCurrentOrganization();
			Long restrictAfterDays = organization.getSmNonrestrictionDays();
			int rowCount = 0;

			for (ShipmentInOut goodShipment : goodShipmentList) {
				Date shipmentCreationDate = goodShipment.getMovementDate();
				if (restrictAfterDays == null) {
					restrictAfterDays = (long) 0;
				}
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(shipmentCreationDate);
				calendar.add(Calendar.DATE, restrictAfterDays.intValue());
				Date restrictionDate = calendar.getTime();
				SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy");
				String strDate = dateFormater.format(restrictionDate);
				Date restrictionStartDate = dateFormater.parse(strDate);
				if (restrictionStartDate.compareTo(new Date()) < 0) {
					List<ShipmentInOutLine> shipmentLines = goodShipment.getMaterialMgmtShipmentInOutLineList();
					for (ShipmentInOutLine shipmentLine : shipmentLines) {
						OBCriteria<InvoiceLine> invoiceLineCrit = OBDal.getInstance().createCriteria(InvoiceLine.class);
						invoiceLineCrit.add(Restrictions.eq(InvoiceLine.PROPERTY_GOODSSHIPMENTLINE, shipmentLine));
						List<InvoiceLine> invoiceLines = invoiceLineCrit.list();
						if (invoiceLines.size() == 0) {
							rowCount++;
						}
					}

				}

			}
			if (rowCount > 0 && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
					RestrictionType.RESTRICTION_RC_SIN_FACT)) {
				OBCriteria<Invoice> invoiceCrit = OBDal.getInstance().createCriteria(Invoice.class);
				invoiceCrit.list().get(0).getMaterialMgmtShipmentInOutList();
				if (msg.length() > 0) {
					msg = msg + ", " + RestrictionType.RESTRICTION_RC_SIN_FACT.getAbbreviation();
				} else {
					msg = RestrictionType.RESTRICTION_RC_SIN_FACT.getAbbreviation();
				}
			}

			msg = getCarteraRestriction(order, msg);
			order.setSmRestrictions(msg);

			// Displaying result of validation
			if (msg.length() > 0) {
				SessionHandler.getInstance().commitAndStart();
				errorMessage = String.format(Utility.messageBD(new DalConnectionProvider(), "SM_RestrictionError",
						OBContext.getOBContext().getLanguage().getLanguage()));

				// TODO:sabari
				Boolean isRestriction = true;
				OBCriteria<Order> orderDetailCrit = OBDal.getInstance().createCriteria(Order.class);
				orderDetailCrit.add(Restrictions.and(Restrictions.eq(Order.PROPERTY_DOCUMENTSTATUS, "CO"),
						Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true)));
				orderDetailCrit.add(Restrictions.eq(Order.PROPERTY_BUSINESSPARTNER, businessPartner));
				List<Order> orderList = orderDetailCrit.list();
				for (Order orderDetail : orderList) {
					List<ShipmentInOut> shipmentList = orderDetail.getMaterialMgmtShipmentInOutList();
					int shipmentSize = orderDetail.getMaterialMgmtShipmentInOutList().size();
					List<Invoice> invoiceList = orderDetail.getInvoiceList();
					Date sodate = orderDetail.getOrderDate();
					Long sorestrictAfterDays = orderDetail.getOrganization().getSmOrderrestrictionDays() == null ? 0
							: orderDetail.getOrganization().getSmOrderrestrictionDays();
					orderDetail.getCreationDate();
					if (shipmentSize > 0) {
						for (ShipmentInOut shipmentDetail : shipmentList) {
							Invoice invoiceDetails = shipmentDetail.getInvoice();
							if (invoiceDetails == null) {
								if (sorestrictAfterDays > 0) {
									Calendar calendar = Calendar.getInstance();
									calendar.setTime(sodate);
									calendar.add(Calendar.DATE, sorestrictAfterDays.intValue());
									Date sorestrictionDate = calendar.getTime();
									SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy");
									String strDate = dateFormater.format(sorestrictionDate);
									Date sorestrictionStartDate = dateFormater.parse(strDate);
									if (sorestrictionStartDate.compareTo(new Date()) > 0) {
										isRestriction = false;
									} else {
										isRestriction = true;
										throw new OBException(errorMessage);
									}
								}
							}
						}
					} else if (invoiceList.size() == 0) {

						if (sorestrictAfterDays > 0) {
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(sodate);
							calendar.add(Calendar.DATE, sorestrictAfterDays.intValue());
							Date sorestrictionDate = calendar.getTime();
							SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy");
							String strDate = dateFormater.format(sorestrictionDate);
							Date sorestrictionStartDate = dateFormater.parse(strDate);
							if (sorestrictionStartDate.compareTo(new Date()) > 0) {
								isRestriction = false;
							} else {
								isRestriction = true;
								throw new OBException(errorMessage);
							}
						}
					}
				}
				// storageDetailCrit.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT,
				// product));
				if (isRestriction) {
					throw new OBException(errorMessage);
				}
			}
			// To check the total % M.U. with line % M.U.
			BigDecimal profitPercentage = new BigDecimal(0);
			BigDecimal totalLineProfit = new BigDecimal(0);
			BigDecimal bPartnerMinProfit;
			BigDecimal totalNetUnitPrice = new BigDecimal(0);
			BigDecimal totalNetListPrice = new BigDecimal(0);
			BigDecimal totalProfit;
			BigDecimal zeroValue = new BigDecimal(0);
			int count = 1;
			if (order.getSMMUTotal() != null) {
				bPartnerMinProfit = order.getSMMUTotal();
			} else {
				bPartnerMinProfit = zeroValue;
			}
			// List<OrderLine> orderLineList = order.getOrderLineList();
			// for (OrderLine ol : orderLineList) {
			// BigDecimal netUnitPrice = ol.getUnitPrice();
			// BigDecimal netListPrice = ol.getListPrice();
			// if (totalNetUnitPrice.equals(zeroValue) &&
			// totalNetListPrice.equals(zeroValue)) {
			// totalNetUnitPrice = netUnitPrice;
			// totalNetListPrice = netListPrice;
			// count++;
			// } else {
			// totalNetUnitPrice = totalNetUnitPrice.add(netUnitPrice);
			// totalNetListPrice = totalNetListPrice.add(netListPrice);
			// count++;
			// }
			// }
			// if (!totalNetUnitPrice.equals(zeroValue) &
			// totalNetListPrice.equals(zeroValue)) {
			// profitPercentage = new BigDecimal(100);
			// } else if (totalNetUnitPrice.equals(zeroValue) &
			// !totalNetListPrice.equals(zeroValue)) {
			// profitPercentage = new BigDecimal(-100);
			// } else {
			//
			// totalLineProfit = totalNetUnitPrice.subtract(totalNetListPrice);
			// BigDecimal multiplicant = new BigDecimal(100);
			// totalProfit = (totalLineProfit.divide(totalNetListPrice, 2));
			// profitPercentage = totalProfit.multiply(multiplicant);
			// // totalNetUnitPriceAvg = totalNetUnitPrice.divide(new
			// BigDecimal(count), 2);
			// // totalNetListPriceAvg = totalNetListPrice.divide(new
			// BigDecimal(count), 2);
			// // totalLineProfit =
			// totalNetUnitPriceAvg.subtract(totalNetListPriceAvg);
			// // BigDecimal multiplicant = new BigDecimal(100);
			// // profitPercentage =
			// //
			// ((totalLineProfit.multiply(multiplicant)).divide(totalNetListPriceAvg,
			// // 2));
			//
			// }

			profitPercentage = order.getSMMUTotal() != null ? order.getSMMUTotal() : new BigDecimal(0);
			bPartnerMinProfit = new BigDecimal(order.getBusinessPartner().getBusinessPartnerCategory().getSMMU() != null
					? order.getBusinessPartner().getBusinessPartnerCategory().getSMMU()
					: "0");
			if (profitPercentage.compareTo(bPartnerMinProfit) == -1
					&& (RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
							RestrictionType.MU_TOTAL_RESTRICTION_BASED_ON_BP_CATEGORY))) {
				errorMessage = String.format(Utility.messageBD(new DalConnectionProvider(), "SM_MUPercentageError",
						OBContext.getOBContext().getLanguage().getLanguage()));
				throw new OBException(errorMessage);
			} else {

				BigDecimal bPartnerMaxSalesAmt = (order.getBusinessPartner() != null
						&& order.getBusinessPartner().getBusinessPartnerCategory() != null
						&& order.getBusinessPartner().getBusinessPartnerCategory().getSMSalesMaxAmount() != null)
								? order.getBusinessPartner().getBusinessPartnerCategory().getSMSalesMaxAmount()
								: new BigDecimal(0);

				List<Map<OrderLine, BigDecimal>> valueList = new ArrayList<Map<OrderLine, BigDecimal>>();
				List<OrderLine> orderlines = order.getOrderLineList();
				OBContext.restorePreviousMode();
				OBContext.setAdminMode();
				for (OrderLine orderline : orderlines) {
					BigDecimal stock = new BigDecimal(0);
					Product product = orderline.getProduct();
					List<Locator> locators = order.getWarehouse().getLocatorList();
					for (Locator locator : locators) {
						OBCriteria<StorageDetail> storageDetailCrit = OBDal.getInstance()
								.createCriteria(StorageDetail.class);
						storageDetailCrit.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT, product));
						storageDetailCrit.add(Restrictions.eq(StorageDetail.PROPERTY_STORAGEBIN, locator));
						List<StorageDetail> storageDetails = storageDetailCrit.list();
						for (StorageDetail storageDetail : storageDetails) {
							BigDecimal QuantityOnHand = storageDetail.getQuantityOnHand();
							stock = stock.add(QuantityOnHand);
						}

					}
					BigDecimal minimumStock = product.getMinQuantity() != null ? product.getMinQuantity()
							: new BigDecimal(0);
					BigDecimal orderQty = orderline.getOrderedQuantity();
					if (orderQty.add(minimumStock).compareTo(stock) > 0) {
						BigDecimal qty = orderQty.add(minimumStock).subtract(stock);
						Map<OrderLine, BigDecimal> valueMap = new HashMap<OrderLine, BigDecimal>();
						valueMap.put(orderline, qty);
						valueList.add(valueMap);
					}
				}
				String reqResult = "";
				if (valueList.size() > 0) {
					VariablesSecureApp vars = new VariablesSecureApp(order.getCreatedBy().getId(),
							order.getClient().getId(), order.getOrganization().getId());
					OBCriteria<Requisition> requisitionCrit = OBDal.getInstance().createCriteria(Requisition.class);
					requisitionCrit.add(Restrictions.eq(Requisition.PROPERTY_PMORDER, order));
					List<Requisition> requisitions = requisitionCrit.list();
					if (requisitions.size() > 0) {
						reqResult = "La Requisición ha sido creada con éxito. Consulte el documento no : "
								+ requisitions.get(0).getDocumentNo();
					} else {
						// Requisition creation.
						Requisition requisition = OBProvider.getInstance().get(Requisition.class);
						// requisition.setBusinessPartner(businessPartner);
						// TODO: Need to add document type id as parameter.
						String documentNo = getNextDocumentNo(order);
						requisition.setDocumentNo(documentNo != null ? documentNo : "1000000");
						requisition.setPmOrder(order);
						requisition.setOrganization(order.getOrganization());
						requisition.setPmSalaesBpartner(order.getBusinessPartner());
						requisition.setUserContact(OBContext.getOBContext().getUser());
						// Setting Status Bar Field Value in requisition.
						requisition.setPmRestrictions(order.getSmRestrictions());
						requisition.setPmAsesor(order.getSalesRepresentative());
						requisition.setPmPercentageMuTotal(order.getSMMUTotal());
						requisition.setPmCreditLineLimit(order.getSMCreditLineLimit());
						requisition.setPmBpcreditlimit(order.getSmBpcreditbalance());
						requisition.setPmCupo(order.getSmCupo());
						requisition.setPmTotal(order.getGrandTotalAmount());
						requisition.setPmSubtotal(order.getSummedLineAmount());
						requisition.setPmDocumentdate(new Date());
						requisition.setPmCPaymentterm(order.getBusinessPartner().getPaymentTerms());
						OBDal.getInstance().save(requisition);
						OBDal.getInstance().flush();

						Long line = new Long(1);
						StringBuffer sb = new StringBuffer();
						for (OrderLine orderline : orderlines) {
							BigDecimal qty = valueList.get(line.intValue() - 1).get(orderline);
							if (qty != null && qty.intValue() > 0) {
								sb.append("Producto ---->" + orderline.getProduct().getName());
								sb.append("\t");
								sb.append("Cantidad ---->" + orderline.getOrderedQuantity());
								sb.append("\n");
								RequisitionLine requisitionLine = OBProvider.getInstance().get(RequisitionLine.class);
								if (orderline.getProduct().isSmIsstandardProduct() != null
										&& orderline.getProduct().isSmIsstandardProduct()) {
									throw new OBException(
											"No puede crear la requisición ya que la línea de pedido de venta tiene un producto estándar");
								}
								requisitionLine.setOrganization(orderline.getOrganization());
								requisitionLine.setRequisition(requisition);
								requisitionLine.setPmSearchkey(orderline.getProduct().getSearchKey());
								requisitionLine.setProduct(orderline.getProduct());
								requisitionLine.setQuantity(qty);
								requisitionLine.setPmAvlbqty(orderline.getSmQtyOnHand());
								requisitionLine.setPmLastpiamount(orderline.getSmLastPurchaseInvAmt());
								requisitionLine.setPmLastpidate(orderline.getSmLastPurchaseInvDate());
								requisitionLine.setPmLastsiamount(orderline.getSmLastSalesInvAmt());
								requisitionLine.setPmLastsidate(orderline.getSmLastSalesInvDate());
								requisitionLine.setPmQtyonhand(orderline.getSmQtyOnHand());
								requisitionLine.setLineNo(line * 10);
								requisitionLine.setUOM(orderline.getUOM());
								requisitionLine.setNeedByDate(new Date());
								requisitionLine.setPmOrderline(orderline);
								OBDal.getInstance().save(requisitionLine);
								OBDal.getInstance().flush();
								line++;
							}
						}
						// TODO:need to complete requisition
						CallProcess callProcess = new CallProcess();
						ProcessInstance instance = callProcess.call("M_Requisition_Post", requisition.getId(), null);
						if (instance.getResult() == 1) {
							reqResult = "La solicitud ha sido creada con éxito. Por favor, consulte el documento no : \n  "
									+ requisition.getDocumentNo();
							// String successMsg =
							// String.format(Utility.messageBD(new
							// DalConnectionProvider(),
							// "SM_SuccessfullyBook",
							// OBContext.getOBContext().getLanguage().getLanguage()));
							OBError successMessage = new OBError();
							successMessage.setTitle("Success");
							successMessage.setType("Success");
							successMessage.setMessage(reqResult);
							bundle.setResult(successMessage);
							order.setDescription(reqResult + "\n" + sb.toString());
						} else {
							log.error("Error while calling the function M_Requisition_Post.");
							String processCallErrorMsg = String.format(
									Utility.messageBD(new DalConnectionProvider(), instance.getErrorMsg().split("@")[2],
											OBContext.getOBContext().getLanguage().getLanguage()));
							throw new OBException(processCallErrorMsg);
						}

					}
					// Enable the Has Requisition check box for avoiding
					// reactive the sales order.
					order.setSmHasrequisition(true);
				}
				order.setSmCupo("" + businessPartner.getCreditUsed());
				OBDal.getInstance().save(order);
				OBDal.getInstance().flush();
				ProcessInstance instance = procedureCall(order);
				if (instance.getResult() == 1) {
					String successMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
							"SM_SuccessfullyBook", OBContext.getOBContext().getLanguage().getLanguage()));
					OBError successMessage = new OBError();
					successMessage.setTitle("Success");
					successMessage.setType("Success");
					successMessage.setMessage(successMsg + " " + reqResult);
					bundle.setResult(successMessage);
					alertCreation(orderID, orderTotalGrossAmount, bPartnerMaxSalesAmt);
				} else {
					log.error("Error while calling the function c_order_post.");
					String[] errorMsgArray = instance.getErrorMsg().split("@");
					String errorMsg = "";
					for (String error : errorMsgArray) {
						errorMsg = errorMsg + error;
					}
					String processCallErrorMsg = String.format(Utility.messageBD(new DalConnectionProvider(), errorMsg,
							OBContext.getOBContext().getLanguage().getLanguage()));
					throw new OBException(processCallErrorMsg);
				}
				log.info("Sales Order Successfully Booked");

				List<String> orderIds = new ArrayList<String>();
				for (OrderLine orderline : order.getOrderLineList()) {
					if (orderline.getQuotationLine() != null
							&& !orderIds.contains(orderline.getQuotationLine().getSalesOrder().getId())) {
						orderIds.add(orderline.getQuotationLine().getSalesOrder().getId());
					}
					if (orderline.getSOPOReference() != null
							&& !orderIds.contains(orderline.getSOPOReference().getSalesOrder().getId())) {
						orderIds.add(orderline.getSOPOReference().getSalesOrder().getId());
					}
				}

				if (!orderIds.isEmpty()) {
					for (String orderId : orderIds) {
						Order quotation = OBDal.getInstance().get(Order.class, orderId);
						quotation.setDocumentAction("CL");
						OBCriteria<RejectReason> rejectReasonCriteria = OBDal.getInstance().createCriteria(RejectReason.class);
						rejectReasonCriteria
								.add(Restrictions.eq(RejectReason.PROPERTY_SEARCHKEY, "Auto Close Purchase Order"));
						if (!rejectReasonCriteria.list().isEmpty()) {
							quotation.setRejectReason(rejectReasonCriteria.list().get(0));
						}
						OBDal.getInstance().save(quotation);
						OBDal.getInstance().flush();
						procedureCall(quotation);
					}
				}
			}
		} catch (Exception e) {
			String message = e.getMessage().toString();
			OBError errorMessage = new OBError();
			errorMessage.setTitle("Error");
			errorMessage.setType("Error");
			errorMessage.setMessage(message);
			bundle.setResult(errorMessage);
			OBDal.getInstance().rollbackAndClose();
			e.printStackTrace();
		} finally {
			OBContext.restorePreviousMode();
		}
	}

	private String getCarteraRestriction(Order order, String msg) {
		int rowCount = 0;

		OBCriteria<Invoice> invoiceCriteria = OBDal.getInstance().createCriteria(Invoice.class);
		invoiceCriteria.add(Restrictions.eq(Invoice.PROPERTY_BUSINESSPARTNER, order.getBusinessPartner()));
		invoiceCriteria.add(Restrictions.eq(Invoice.PROPERTY_SALESTRANSACTION, true));
		invoiceCriteria.add(Restrictions.eq(Invoice.PROPERTY_DOCUMENTSTATUS, "CO"));

		long daysGSRestriction = order.getOrganization().getSmNonrestrictionDays() == null ? 0
				: order.getOrganization().getSmNonrestrictionDays();

		if (!invoiceCriteria.list().isEmpty()) {
			for (Invoice invoice : invoiceCriteria.list()) {
				Date invoiceDate = invoice.getInvoiceDate();
				long paymentTermDays = invoice.getPaymentTerms().getOverduePaymentDaysRule();
				long resultantDays = paymentTermDays + daysGSRestriction;
				Calendar cal = Calendar.getInstance();
				cal.setTime(invoiceDate);
				cal.add(Calendar.DAY_OF_MONTH, (int) resultantDays);
				if (cal.getTime().compareTo(new Date()) == -1 || cal.getTime().compareTo(new Date()) == 1) {
					rowCount++;
				}
			}
		}

		if (rowCount > 0 && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
				RestrictionType.CARTERA)) {
			if (msg.length() > 0) {
				msg = msg + ", " + RestrictionType.CARTERA.getAbbreviation();
			} else {
				msg = RestrictionType.CARTERA.getAbbreviation();
			}
		}
		return msg;
	}

	public static ProcessInstance procedureCall(Order order) {
		// Long result;
		String orderID = order.getId();
		CallProcess callProcess = new CallProcess();
		ProcessInstance instance = callProcess.call("C_Order_Post", orderID, null);
		// result=instance.getResult();
		return instance;

	}

	public static void alertCreation(String orderID, BigDecimal orderTotalGrossAmount, BigDecimal bPartnerMaxSalesAmt) {

		if (bPartnerMaxSalesAmt != null && bPartnerMaxSalesAmt.compareTo(orderTotalGrossAmount) == -1) {
			String alertRuleName = "Higher Amount Sales Quotation";
			String alertDescription = String.format(Utility.messageBD(new DalConnectionProvider(),
					"SM_SQHigerAmtAlertDescription", OBContext.getOBContext().getLanguage().getLanguage()));
			AlertRule alertRule = AlertUtility.getAlertRule(alertRuleName);
			if (alertRule == null) {
				try {
					OBContext.setAdminMode();
					AlertRule alerRule = OBProvider.getInstance().get(AlertRule.class);
				} finally {

				}
			}
			AlertUtility.CreateAlert(alertDescription, alertRule, orderID);
		}
	}

	public String getNextDocumentNo(Order order) {
		try {
			OBCriteria<Sequence> sequenceCrit = OBDal.getInstance().createCriteria(Sequence.class);
			sequenceCrit.add(Restrictions.eq(Sequence.PROPERTY_NAME, "Purchase Requisition"));
			List<Sequence> listOfSequence = sequenceCrit.list();
			if (listOfSequence.size() > 0) {
				Sequence sequence = listOfSequence.get(0);
				String documentNo = ((sequence.getPrefix() != null ? sequence.getPrefix() : "")
						+ (sequence.getNextAssignedNumber())
						+ (sequence.getSuffix() != null ? sequence.getSuffix() : ""));
				sequence.setNextAssignedNumber(sequence.getNextAssignedNumber() + 1);
				OBDal.getInstance().save(sequence);
				// OBDal.getInstance().flush();
				return documentNo;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception in GoodShipment Creation Process--->" + "Could not generate next documentNo");
		}
		return null;
	}
}
