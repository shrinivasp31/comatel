package com.mitosis.custom.SalesManagement.process;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

public class OrderClosingProccess extends DalBaseProcess {
	Logger log = Logger.getLogger(OrderClosingProccess.class);

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {
		ProcessLogger processLogger = bundle.getLogger();
		String errorMsg = "";
		// TODO Auto-generated method stub
		OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
		orderCriteria.add(Restrictions.eq(Order.PROPERTY_DOCUMENTSTATUS, "CO"));
		orderCriteria.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));

		List<Order> orderList = orderCriteria.list();
		for (Order order : orderList) {
			try {
				if (order.getMaterialMgmtShipmentInOutList().size() == 0) {
					Long closingDays = order.getOrganization().getSmOrderClosingDays() != null
							? order.getOrganization().getSmOrderClosingDays() : 0;
					SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
					Date orderDate = order.getOrderDate();
					Calendar c = Calendar.getInstance();
					c.setTime(orderDate);
					c.add(Calendar.DATE, closingDays.intValue());
					orderDate = c.getTime();
					String strOrderDate1 = format.format(orderDate);
					Date date = new Date();
					String strToday = format.format(date);
					if (format.parse(strOrderDate1).compareTo(format.parse(strToday)) == -1) {
						order.setDocumentAction("CL");
						OBDal.getInstance().save(order);
						OBDal.getInstance().flush();
						QuotationCompletion quotationCompletion = new QuotationCompletion();
						ProcessInstance instance = quotationCompletion.procedureCall(order);
						if (instance.getResult() != 1) {
							SessionHandler.getInstance().commitAndStart();
							log.error("Error while calling the function c_order_post.");
							String processCallErrorMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
									instance.getErrorMsg(), OBContext.getOBContext().getLanguage().getLanguage()));
							throw new OBException("Exception Occured while closing order : " + order.getDocumentNo()
									+ " ERROR: " + processCallErrorMsg.substring(7));
						}
						log.info("Sales Order Successfully Closed");
					} else if (order.getTransactionDocument().getSOSubType() != null
							&& order.getTransactionDocument().getSOSubType().equals("SOO")) {
						OBCriteria<Order> quotationCriteria = OBDal.getInstance().createCriteria(Order.class);
						quotationCriteria.add(Restrictions.eq(Order.PROPERTY_DOCUMENTSTATUS, "CO"));
						quotationCriteria.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));
						quotationCriteria.add(Restrictions.eq(Order.PROPERTY_QUOTATION, order));
						if (!quotationCriteria.list().isEmpty()) {
							order.setDocumentAction("CL");
							OBDal.getInstance().save(order);
							OBDal.getInstance().flush();
							QuotationCompletion quotationCompletion = new QuotationCompletion();
							ProcessInstance instance = quotationCompletion.procedureCall(order);
							if (instance.getResult() != 1) {
								SessionHandler.getInstance().commitAndStart();
								log.error("Error while calling the function c_order_post.");
								String processCallErrorMsg = String
										.format(Utility.messageBD(new DalConnectionProvider(), instance.getErrorMsg(),
												OBContext.getOBContext().getLanguage().getLanguage()));
								throw new OBException("Exception Occured while closing Quotation : "
										+ order.getDocumentNo()
										+ " ERROR: " + processCallErrorMsg.substring(7));
							}
							log.info("Sales Quotation Successfully Closed");
						}
					}
				}
			} catch (Exception ex) {
				if (errorMsg.length() > 0)
					errorMsg = errorMsg + "," + ex;
				else
					errorMsg = ex + "";
			}
		}
		if (errorMsg.length() > 0) {
			processLogger.log(errorMsg);
			throw new OBException(errorMsg);
		}
	}
}
