package com.mitosis.custom.SalesManagement.process;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.alert.AlertRule;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.order.Order;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

import com.mitosis.custom.SalesManagement.utility.AlertUtility;

public class QuotationExpireDateAlert extends DalBaseProcess {

  @SuppressWarnings("null")
  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
    Logger log = Logger.getLogger(QuotationExpireDateAlert.class);
    ProcessLogger pLog = bundle.getLogger();
    try {

      Organization organization = OBContext.getOBContext().getCurrentOrganization();
      Long alertBeforeDays = organization.getSmSqCoAlertDays();
      if (alertBeforeDays != null) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, alertBeforeDays.intValue());
        Date expireDate = calendar.getTime();
        SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = dateFormater.format(expireDate);
        Date expDate = dateFormater.parse(strDate);
        String orderId;
        String alertRuleName = "Sales Quotation Expire Date";
        String alertDescription = String.format(Utility
            .messageBD(new DalConnectionProvider(), "SM_SQExpireDateAlertDescription", OBContext
                .getOBContext().getLanguage().getLanguage()));
        OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
        orderCriteria.add(Restrictions.and(Restrictions.eq(Order.PROPERTY_VALIDUNTIL, expDate),
            Restrictions.eq(Order.PROPERTY_ORGANIZATION, organization)));
        List<Order> orderList = orderCriteria.list();
        for (Order order : orderList) {
          orderId = order.getId();
          AlertRule alertRule = AlertUtility.getAlertRule(alertRuleName);
          AlertUtility.CreateAlert(alertDescription, alertRule, orderId);
        }
      }
      log.info("Sales Quotation Expire Date Alert Created Successfully");
    } catch (Exception e) {
      String msg = "Error :" + e.getMessage();
      pLog.log(msg);
      throw e;
    }
  }

}