package com.mitosis.custom.SalesManagement.process;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalConnectionProvider;

public class SalesQuotationBooking extends QuotationCompletion {
	protected void doExecute(ProcessBundle bundle) throws Exception {
		try {
			OBContext.setAdminMode();
			Logger log = Logger.getLogger(SalesQuotationBooking.class);
			String orderID = (String) bundle.getParams().get("C_Order_ID");
			Order order = OBDal.getInstance().get(Order.class, orderID);
			if (order.getOrderLineList().size() == 0) {
				throw new OBException("Order Line should not be empty");
			}

			BigDecimal bpCategoryMu = order.getBusinessPartner().getBusinessPartnerCategory().getCmcSalesQuoteMinMu();
			if (bpCategoryMu != null & order.getTransactionDocument().getSOSubType() != null
					&& order.getTransactionDocument().getSOSubType().equalsIgnoreCase("OB")) {
				for (OrderLine orderLine : order.getOrderLineList()) {
					if (orderLine.getSMMUTotal().compareTo(bpCategoryMu) == -1) {
						throw new OBException("Línea no " + orderLine.getLineNo().toString()
								+ " mayor que la cotización de ventas MU% definida en la categoría de socio comercial. ["
								+ order.getBusinessPartner().getBusinessPartnerCategory().getName() + " ]");
					}
				}
			}

			BigDecimal orderTotalGrossAmount = order.getGrandTotalAmount();
			BigDecimal bpMaxSalesAmount = (order.getBusinessPartner() != null
					&& order.getBusinessPartner().getBusinessPartnerCategory() != null
					&& order.getBusinessPartner().getBusinessPartnerCategory().getSMSalesMaxAmount() != null)
							? order.getBusinessPartner().getBusinessPartnerCategory().getSMSalesMaxAmount()
							: new BigDecimal(0);
			updateOrderDate(order);
			ProcessInstance instance = procedureCall(order);
			if (instance.getResult() == 1) {
				String successMsg = String.format(Utility.messageBD(new DalConnectionProvider(), "SM_SuccessfullyBook",
						OBContext.getOBContext().getLanguage().getLanguage()));
				OBError successMessage = new OBError();
				successMessage.setTitle("Success");
				successMessage.setType("Success");
				successMessage.setMessage(successMsg);
				bundle.setResult(successMessage);

				alertCreation(orderID, orderTotalGrossAmount, bpMaxSalesAmount);
			} else {
				log.error("Error while calling the function c_order_post.");
				String processCallErrorMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
						instance.getErrorMsg().split("@")[2], OBContext.getOBContext().getLanguage().getLanguage()));
				throw new OBException(processCallErrorMsg);
			}
			log.info("Sales Order Successfully Booked");
		} finally {
			OBContext.restorePreviousMode();
		}
	}

	public void updateOrderDate(Order order) {
		try {
			order.setOrderDate(new Date());
			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
		} catch (Throwable e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			throw new OBException(e.getMessage());

		}
	}
}
