package com.mitosis.custom.SalesManagement.process;

import org.apache.log4j.Logger;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * Creating Goods Shipment from order.
 * 
 * @author Anbbukkani Gajendiran
 *
 */
public class CreateGoodsShipment extends DalBaseProcess {
	Logger log = Logger.getLogger(CreateGoodsShipment.class);

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {
		final OBError successMsg = new OBError();

		try {
			OBContext.setAdminMode();
			String c_order_id = (String) bundle.getParams().get("C_Order_ID");
			Order order = OBDal.getInstance().get(Order.class, c_order_id);
			com.mitosis.custom.SalesManagement.goodshipment.CreateGoodsShipment createGoodShipment = new com.mitosis.custom.SalesManagement.goodshipment.CreateGoodsShipment();
			ShipmentInOut shipmentInOut = createGoodShipment.getOrCreateShipment(order, order.getWarehouse());
			// shipmentInOut.getId();
			successMsg.setType("Success");
			successMsg.setTitle("Goods Shippment Creation");
			successMsg.setMessage("Goods Shipment Created Successfully in Draft. Refernce Document No "
					+ shipmentInOut.getDocumentNo());
		} finally {
			bundle.setResult(successMsg);
			OBContext.restorePreviousMode();
		}
	}

}
