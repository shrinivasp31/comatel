package com.mitosis.custom.SalesManagement.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.UUID;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * Print Shipping report in configured organization printer.
 * 
 * @author Sabarinath M S
 *
 */
public class PrintGoodShipment extends DalBaseProcess {
  Logger log = Logger.getLogger(PrintGoodShipment.class);
  public static final String ATTACH_PATH = OBPropertiesProvider.getInstance()
      .getOpenbravoProperties().getProperty("attach.path");

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {

    try {
      String inoutID = (String) bundle.getParams().get("M_InOut_ID");
      ShipmentInOut shipmentInOut = OBDal.getInstance().get(ShipmentInOut.class, inoutID);
      String printerName = shipmentInOut.getOrganization().getSmStickerPrinter();
      if (printerName == null) {
        log.error("Printer is not configured. Please configure in organization window.");
        throw new OBException("Printer is not configured. Please configure in organization window.");
      } else {
        InputStream inputStream = this
            .getClass()
            .getClassLoader()
            .getResourceAsStream(
                "com/mitosis/custom/SalesManagement/reports/SM_PrintShipment.jrxml");

        HashMap<String, Object> parameters = new HashMap<String, Object>();
        BusinessPartner businessPartner = shipmentInOut.getBusinessPartner();

        parameters.put("ORGANIZATIONID", (shipmentInOut.getOrganization() != null) ? shipmentInOut
            .getOrganization().getId() : "");
        parameters.put("docno", shipmentInOut.getDocumentNo());
        parameters.put(
            "bpartner",
            (((businessPartner != null && businessPartner.getName() != null) ? businessPartner
                .getName() : "") + (businessPartner.getName2() != null ? " "
                + businessPartner.getName2() : "")));
        parameters.put("bpcontact", (businessPartner.getADUserList() != null && businessPartner
            .getADUserList().get(0).getName() != null) ? businessPartner.getADUserList().get(0)
            .getName() : "");
        parameters.put("bpphone", (businessPartner.getADUserList() != null && businessPartner
            .getADUserList().get(0).getPhone() != null) ? businessPartner.getADUserList().get(0)
            .getPhone() : "");

        Location locationAddress = shipmentInOut.getPartnerAddress() != null ? shipmentInOut
            .getPartnerAddress().getLocationAddress() : null;

        String bpAddress = getAddress(locationAddress);

        parameters.put("bpaddress", bpAddress);
        parameters.put("observation",
            shipmentInOut.getDescription() != null ? shipmentInOut.getDescription() : "");
        Organization org = shipmentInOut.getOrganization();
        parameters.put("orgname", (org != null ? org.getName() : ""));
        OrganizationInformation orgInfo = org.getOrganizationInformationList().get(0);
        Location orgLocationAddress = orgInfo.getLocationAddress();

        String orgAddress = getAddress(orgLocationAddress);
        parameters.put("orgaddress", orgAddress);

        // phone and fax info
        String orgdetail1 = (((orgInfo.getUserContact() != null && orgInfo.getUserContact()
            .getPhone() != null) ? "Phone: " + orgInfo.getUserContact().getPhone() : "") + ((orgInfo
            .getUserContact() != null && orgInfo.getUserContact().getFax() != null) ? ", Fax: "
            + orgInfo.getUserContact().getFax() : ""));
        parameters.put("orgdetail1", orgdetail1);

        // email
        String orgdetail2 = (((orgInfo.getUserContact() != null && orgInfo.getUserContact()
            .getEmail() != null) ? "Email: " + orgInfo.getUserContact().getEmail() : ""));
        parameters.put("orgdetail2", orgdetail2);

        JasperDesign jasperDesign = JRXmlLoader.load(inputStream);

        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JREmptyDataSource ds = new JREmptyDataSource();

        UUID uuid = new UUID(16, 8);
        String fileName = ATTACH_PATH + "/" + "Shippment_" + uuid + ".pdf";

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);

        File file = new File(fileName);
        if (!file.exists()) {
          log.error("Pdf is not created properly. Try to print again");
          throw new OBException("Pdf is not created properly. Try to print again");
        }

        printShipment(file, printerName);

        if (file.exists()) {
          file.delete();
        }

      }
    } catch (Exception ex) {
      log.error(ex.getMessage());
      throw ex;
    }
  }

  /**
   * Get Address from location
   * 
   * @param locationAddress
   * @return String
   */
  private String getAddress(Location locationAddress) {
    String address = "";

    if (locationAddress != null) {

      if (locationAddress.getAddressLine1() != null)
        address = address + locationAddress.getAddressLine1() + ",";
      if (locationAddress.getAddressLine2() != null)
        address = address + locationAddress.getAddressLine2() + ",";
      if (locationAddress.getCity() != null)
        address = address + locationAddress.getCity().getName() + ",";
      if (locationAddress.getCountry() != null)
        address = address + locationAddress.getCountry().getISOCountryCode();
    }
    return address;
  }

  /**
   * Print PDF as hard copy in specified printer for given file.
   * 
   * @param file
   * @param printerName
   * @throws Exception
   */
  private void printShipment(File file, String printerName) throws Exception {
    FileInputStream inputStream = new FileInputStream(file);
    DocFlavor psInFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
    Doc myDoc = new SimpleDoc(inputStream, psInFormat, null);
    PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
    PrintService[] services = PrintServiceLookup.lookupPrintServices(psInFormat, aset);

    // this step is necessary because I have several printers configured
    PrintService myPrinter = null;
    for (int i = 0; i < services.length; i++) {
      String svcName = services[i].getName();
      if (svcName.contains(printerName)) {
        myPrinter = services[i];
        log.info("my printer is found in : " + svcName);
        break;
      }
    }
    if (myPrinter != null) {
      DocPrintJob job = myPrinter.createPrintJob();

      job.print(myDoc, aset);
      inputStream.close();

    } else {
      log.error("The printer " + printerName + "is not available in this system");
      throw new OBException("The printer " + printerName + "is not available in this system");
    }
  }
}
