/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SL 
 * All portions are Copyright (C) 2001-2006 Openbravo SL 
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 * 	Fecha       Version   		Program. 	Descripcion de cambio
	============================================================================
	25-08-2009  2.35.0.0.0		AGF     	Organizador corresponde al mismo codigo
											de la cuenta en el SELECT, se hizo por
											que la funcion addFilter, tiene que dejar
											en balnco este codigo, pero es necesario
											para organizar el arbol de las cuentas .

 */
package com.mitosis.custom.financial.reports;

import java.util.Comparator;

public class ReportTrialRazonDataComparator implements Comparator<Object> {
  public int compare(Object obj1, Object obj2) {
    String code1 = ((ReportTrialRazonData) obj1).organizador.toUpperCase();
    String code2 = ((ReportTrialRazonData) obj2).organizador.toUpperCase();
    return code1.compareTo(code2);
  }
}
