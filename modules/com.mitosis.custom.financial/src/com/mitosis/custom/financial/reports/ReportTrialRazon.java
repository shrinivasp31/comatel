/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SL 
 * All portions are Copyright (C) 2001-2006 Openbravo SL 
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 * 	Fecha       Version   		Program. 	Descripcion de cambio
	============================================================================
	19-04-2010  2.4.0.0.2		AGF			Se pasan parametros del NIT de la compania,
											y el nombre.  
								
	25-08-2009  2.35.0.0.0		AGF     	Se creo la funcion addFilter,que me
	 										filtra dependiendo el nivel escogido,
	 										ademas le quita a los campos que tengan
	 										movimiento en la fecha especificada, el
	 										numero de cuenta y el nombre.
	 										
	25-08-2009  2.35.0.0.0		AGF			Se adiciona en la impresion un archivo JRXML 
	 										en cambio del archivo .FO. (El codigo esta por
	 										si se quiere interactuar con este).
    
 */
package com.mitosis.custom.financial.reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_combos.OrganizationComboData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class ReportTrialRazon extends HttpSecureAppServlet {

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "ReportTrialRazon|DateFrom", "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "ReportTrialRazon|DateTo", "");
      String strOnly = vars.getGlobalVariable("inpOnly", "ReportTrialRazon|Only", "-1");
      String strOrg = vars.getGlobalVariable("inpOrg", "ReportTrialRazon|Org", "");
      String strLevel = vars.getGlobalVariable("inpLevel", "ReportTrialRazon|Level", "");
      String strAccountFrom = vars.getGlobalVariable("inpAccountFrom",
          "ReportTrialRazon|AccountFrom", "");
      String strAccountTo = vars.getGlobalVariable(
          "inpAccountTo",
          "ReportTrialRazon|AccountTo",
          ReportTrialRazonData.selectLastAccount(this,
              Utility.getContext(this, vars, "#User_Org", "Account"),
              Utility.getContext(this, vars, "#User_Client", "Account")));

      String strcBpartnerId = vars.getRequestInGlobalVariable("inpcBPartnerId_IN",
          "ReportTrialRazon|cBpartnerId");
      String strAll = vars.getStringParameter("inpAll");

      printPageDataSheet(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strOnly,
          strAccountFrom, strAccountTo, strAll, strcBpartnerId);
    } else if (vars.commandIn("FIND")) {
      String strDateFrom = vars
          .getRequestGlobalVariable("inpDateFrom", "ReportTrialRazon|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "ReportTrialRazon|DateTo");
      String strOnly = vars.getRequestGlobalVariable("inpOnly", "ReportTrialRazon|Only");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "ReportTrialRazon|Org");
      String strLevel = vars.getRequestGlobalVariable("inpLevel", "ReportTrialRazon|Level");
      String strAccountFrom = vars.getRequestGlobalVariable("inpAccountFrom",
          "ReportTrialRazon|AccountFrom");
      String strAccountTo = vars.getRequestGlobalVariable("inpAccountTo",
          "ReportTrialRazon|AccountTo");

      String strcBpartnerId = vars.getRequestInGlobalVariable("inpcBPartnerId_IN",
          "ReportTrialRazon|cBpartnerId");
      String strAll = vars.getStringParameter("inpAll");
      printPageDataSheet(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strOnly,
          strAccountFrom, strAccountTo, strAll, strcBpartnerId);
    } else if (vars.commandIn("PDF")) {
      String strDateFrom = vars
          .getRequestGlobalVariable("inpDateFrom", "ReportTrialRazon|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "ReportTrialRazon|DateTo");
      String strOnly = vars.getRequestGlobalVariable("inpOnly", "ReportTrialRazon|Only");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "ReportTrialRazon|Org");
      String strLevel = vars.getRequestGlobalVariable("inpLevel", "ReportTrialRazon|Level");
      String strAccountFrom = vars.getRequestGlobalVariable("inpAccountFrom",
          "ReportTrialRazon|AccountFrom");
      String strAccountTo = vars.getRequestGlobalVariable("inpAccountTo",
          "ReportTrialRazon|AccountTo");

      String strcBpartnerId = vars.getRequestInGlobalVariable("inpcBPartnerId_IN",
          "ReportTrialRazon|cBpartnerId");
      String strAll = vars.getStringParameter("inpAll");
      printPageDataPDF(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strOnly,
          strAccountFrom, strAccountTo, strAll, strcBpartnerId);
    } else
      pageError(response);
  }

  void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strOrg, String strLevel, String strOnly,
      String strAccountFrom, String strAccountTo, String strAll, String strcBpartnerId)
      throws IOException, ServletException {
    String strMessage = "";
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    if (log4j.isDebugEnabled())
      log4j.debug("strAll:" + strAll + " - strLevel:" + strLevel + " - strOnly:" + strOnly);
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    String discard[] = { "sectionDiscard", "sectionBP" };
    XmlDocument xmlDocument = null;
    String strTreeOrg = ReportTrialRazonData.treeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);
    String strTreeAccount = ReportTrialRazonData.treeAccount(this, vars.getClient());
    String strcBpartnerIdAux = strcBpartnerId;

    BigDecimal v_amtacctdr = new BigDecimal("0.0");
    BigDecimal v_amtacctcr = new BigDecimal("0.0");
    BigDecimal v_saldoinicial = new BigDecimal("0.0");
    BigDecimal v_Saldofinal = new BigDecimal("0.0");

    String v_Samtacctdr = "";
    String v_Samtacctcr = "";
    String v_Ssaldoinicial = "";
    String v_Ssaldofinal = "";

    ReportTrialRazonData[] data2 = null;
    ReportTrialRazonData[] data = null;

    if (strDateFrom.equals("") && strDateTo.equals("")) {
      xmlDocument = xmlEngine.readXmlTemplate(
          "com/mitosis/custom/financial/reports/ReportTrialRazon", discard).createXmlDocument();
      data = ReportTrialRazonData.set();
      if (vars.commandIn("FIND")) {
        strMessage = Utility.messageBD(this, "BothDatesCannotBeBlank", vars.getLanguage());
        log4j.warn("Both dates are blank");
      }
    } else {
      if (!strLevel.equals("S"))
        discard[0] = "selEliminarField";
      else
        discard[0] = "discard";

      if (log4j.isDebugEnabled())
        log4j.debug("printPageDataSheet - strOrgFamily = " + strOrgFamily);

      if (strLevel.equals("S") && strOnly.equals("-1")) {

        if (log4j.isDebugEnabled())
          log4j.debug("strcBpartnerId:" + strcBpartnerId + " - strAll:" + strAll);
        if (!(strAll.equals("") && (strcBpartnerId.equals("")))) {
          if (log4j.isDebugEnabled())
            log4j.debug("Select BP, strcBpartnerId:" + strcBpartnerId + " - strAll:" + strAll);
          if (!strAll.equals(""))
            strcBpartnerId = "";
          discard[1] = "sectionNoBP";
          data = ReportTrialRazonData.selectBP(this, strDateFrom, strDateTo, strOrg, strOrgFamily,
              Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"),
              Utility.getContext(this, vars, "#User_Org", "ReportTrialRazon"), strDateFrom,
              strDateTo, strAccountFrom, strAccountTo, strcBpartnerId);

        } else {
          data = ReportTrialRazonData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount,
              strOrgFamily, Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"),
              Utility.getContext(this, vars, "#User_Org", "ReportTrialRazon"), strDateFrom,
              strDateTo);
        }
      } else {
        data = ReportTrialRazonData
            .select(this, strDateFrom, strDateTo, strOrg, strTreeAccount, strOrgFamily,
                Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"),
                Utility.getContext(this, vars, "#User_Org", "ReportTrialRazon"), strDateFrom,
                strDateTo);
      }

      if (log4j.isDebugEnabled())
        log4j.debug("Calculating tree...");

      data = calculateTree(data, null, new Vector<Object>());

      /* AGF */
      data = addFilter(data, strLevel);
      /* AGF */
      /*
       * Este fitro ya no se utiliza por que creamos el otro que ademas añade las cuentas que han
       * tenido movimiento data = levelFilter(data, null, false, strLevel);
       */
      data = dataFilter(data);

      /* SUMATORIA TOTAL INICIO - AGF */
      data2 = levelFilter(data, null, false, "E");
      NumberFormat nf = NumberFormat.getInstance();

      for (int i = 0; i < data2.length; i++) {
        v_amtacctdr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctdr)).add(v_amtacctdr);
        v_amtacctcr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctcr)).add(v_amtacctcr);
        v_saldoinicial = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoInicial)).add(
            v_saldoinicial);
        v_Saldofinal = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoFinal))
            .add(v_Saldofinal);
      }

      v_Samtacctdr = String.valueOf(nf.format(v_amtacctdr));
      v_Samtacctcr = String.valueOf(nf.format(v_amtacctcr));
      v_Ssaldoinicial = String.valueOf(nf.format(v_saldoinicial));
      v_Ssaldofinal = String.valueOf(nf.format(v_Saldofinal));

      /* SUMATORIA TOTAL FINAL - AGF */

      if (log4j.isDebugEnabled())
        log4j.debug("Tree calculated");
    }
    ReportTrialRazonData[] new_data = null;
    /*
     * AJGEste filtro bloquea la funcion addFilter, por que quita los campos que incluimos en esta
     * funcionif (strOnly.equals("-1") && data!=null && data.length>0)new_data = filterTree(data,
     * strLevel); else
     */
    new_data = data;

    /* AJG Funcion para el filtro de las cuentas INICIO */
    new_data = AccountFilter(data, null, false, strAccountFrom, strAccountTo);
    /* AJG Funcion para el filtro de las cuentas INICIO */
    if (log4j.isDebugEnabled())
      log4j.debug("Creating xmlengine");
    xmlDocument = xmlEngine.readXmlTemplate(
        "com/mitosis/custom/financial/reports/ReportTrialRazon", discard).createXmlDocument();
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "",
          "C_ElementValue level", "", Utility.getContext(this, vars, "#User_Org",
              "ReportTrialRazon"), Utility.getContext(this, vars, "#User_Client",
              "ReportTrialRazon"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "ReportTrialRazon", "");
      xmlDocument.setData("reportLevel", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ReportTrialRazon", false, "", "",
        "imprimir();return false;", false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();

    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      // TODO: Need to check the use of commande lines
      // KeyMap key = new KeyMap(this, vars, "ReportTrialRazon.html");
      // xmlDocument.setParameter("keyMap", key.getReportKeyMaps());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.mitosis.custom.financial.reports.ReportTrialRazon");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ReportTrialRazon.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "ReportTrialRazon.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("ReportTrialRazon");
      vars.removeMessage("ReportTrialRazon");

      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setData(
        "reportAccountFrom_ID",
        "liststructure",
        ReportTrialRazonData.selectAccount(this,
            Utility.getContext(this, vars, "#User_Org", "Account"),
            Utility.getContext(this, vars, "#User_Client", "Account"), ""));
    xmlDocument.setData(
        "reportAccountTo_ID",
        "liststructure",
        ReportTrialRazonData.selectAccount(this,
            Utility.getContext(this, vars, "#User_Org", "Account"),
            Utility.getContext(this, vars, "#User_Client", "Account"), ""));
    xmlDocument.setData("reportAD_ORGID", "liststructure",
        OrganizationComboData.selectCombo(this, vars.getRole()));
    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("Only", strOnly);
    xmlDocument.setParameter("adOrgId", strOrg);
    xmlDocument.setParameter("Level", strLevel);
    xmlDocument.setParameter("accountFrom", strAccountFrom);
    xmlDocument.setParameter("accountTo", strAccountTo);

    /* Envia los Totales - AGF */
    xmlDocument.setParameter("TotalAmtacctdr", v_Samtacctdr);
    xmlDocument.setParameter("TotalAmtacctcr", v_Samtacctcr);
    xmlDocument.setParameter("TotalSaldoInicial", v_Ssaldoinicial);
    xmlDocument.setParameter("TotalSaldoFinal", v_Ssaldofinal);
    /* Envia los totales - AGF */

    xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage
        + "');"));
    xmlDocument.setParameter("paramAll0", strAll.equals("") ? "0" : "1");
    xmlDocument.setData("reportCBPartnerId_IN", "liststructure", ReportTrialRazonData
        .selectBpartner(this, Utility.getContext(this, vars, "#User_Org", ""),
            Utility.getContext(this, vars, "#User_Client", ""), strcBpartnerIdAux));

    if (log4j.isDebugEnabled())
      log4j.debug("filling structure, data.length:" + new_data.length);
    if (discard[1].equals("sectionNoBP")) {
      if (log4j.isDebugEnabled())
        log4j.debug("without BPs");
      xmlDocument.setData("structure2", new_data);
    } else {
      if (log4j.isDebugEnabled())
        log4j.debug("with BPs");
      if (strOnly.equals("-1"))
        Arrays.sort(new_data, new ReportTrialRazonDataComparator());
      for (int i = 0; i < new_data.length; i++) {
        new_data[i].rownum = "" + i;
      }
      /* ORDENAR COMIENZO */
      Arrays.sort(new_data, new ReportTrialRazonDataComparator());
      /* ORDENAR FIN */

      if (log4j.isDebugEnabled())
        log4j.debug("Rownum calculated");
      xmlDocument.setData("structure1", new_data);

    }
    out.println(xmlDocument.print());
    out.close();
  }

  void printPageDataPDF(HttpServletResponse response, VariablesSecureApp vars, String strDateFrom,
      String strDateTo, String strOrg, String strLevel, String strOnly, String strAccountFrom,
      String strAccountTo, String strAll, String strcBpartnerId) throws IOException,
      ServletException {
    BigDecimal v_amtacctdr = new BigDecimal("0.0");
    BigDecimal v_amtacctcr = new BigDecimal("0.0");
    BigDecimal v_saldoinicial = new BigDecimal("0.0");
    BigDecimal v_Saldofinal = new BigDecimal("0.0");

    String v_Samtacctdr = "";
    String v_Samtacctcr = "";
    String v_Ssaldoinicial = "";
    String v_Ssaldofinal = "";

    ReportTrialRazonData[] data = null;
    ReportTrialRazonData[] data2 = null;

    String strTreeOrg = ReportTrialRazonData.treeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);
    String strTreeAccount = ReportTrialRazonData.treeAccount(this, vars.getClient());
    String strcBpartnerIdAux = strcBpartnerId;

    data = ReportTrialRazonData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount,
        strOrgFamily, Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"),
        Utility.getContext(this, vars, "#User_Org", "ReportTrialRazon"), strDateFrom, strDateTo);

    data = calculateTree(data, null, new Vector<Object>());
    data = addFilter(data, strLevel);
    /*
     * Este fitro ya no se utiliza por que creamos el otro que ademas añade las cuentas que han
     * tenido movimiento data = levelFilter(data, null, false, strLevel);
     */
    data = dataFilter(data);
    /* SUMATORIA TOTAL INICIO - AGF */
    data2 = levelFilter(data, null, false, "E");
    NumberFormat nf = NumberFormat.getInstance();

    for (int i = 0; i < data2.length; i++) {
      v_amtacctdr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctdr)).add(v_amtacctdr);
      v_amtacctcr = BigDecimal.valueOf(Double.parseDouble(data2[i].amtacctcr)).add(v_amtacctcr);
      v_saldoinicial = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoInicial)).add(
          v_saldoinicial);
      v_Saldofinal = BigDecimal.valueOf(Double.parseDouble(data2[i].saldoFinal)).add(v_Saldofinal);
    }

    v_Samtacctdr = String.valueOf(nf.format(v_amtacctdr));
    v_Samtacctcr = String.valueOf(nf.format(v_amtacctcr));
    v_Ssaldoinicial = String.valueOf(nf.format(v_saldoinicial));
    v_Ssaldofinal = String.valueOf(nf.format(v_Saldofinal));

    /* SUMATORIA TOTAL FINAL - AGF */

    /* AJG Funcion para el filtro de las cuentas INICIO */
    data = AccountFilter(data, null, false, strAccountFrom, strAccountTo);
    /* AJG Funcion para el filtro de las cuentas INICIO */

    /* Envia los Totales - AGF */

    /*
     * xmlDocument.setParameter("TotalAmtacctdr", v_Samtacctdr);
     * xmlDocument.setParameter("TotalAmtacctcr", v_Samtacctcr);
     * xmlDocument.setParameter("TotalSaldoInicial", v_Ssaldoinicial);
     * xmlDocument.setParameter("TotalSaldoFinal", v_Ssaldofinal);
     */
    /* Envia los totales - AGF */

    /* ORDENAR COMIENZO */
    Arrays.sort(data, new ReportTrialRazonDataComparator());
    /* ORDENAR FIN */
    String strSubtitle = Utility.messageBD(this, "Period", vars.getLanguage()) + ": " + strDateFrom
        + " - " + strDateTo;

    String strOutput = vars.commandIn("PDF") ? "pdf" : "xls";
    String strReportName = "";

    strReportName = "@basedesign@/com/mitosis/custom/financial/reports/ReportTrialRazon.jrxml";

    /************************ 19-04-2010 2.4.0.0.2 *****************************************/
    // Tomamos el Nombre de cliente y el NIT.
    ReportTrialRazonData[] org_data = null;
    org_data = ReportTrialRazonData.selectCompany(this, vars.getClient());
    String strcompany = org_data[0].organizacion;
    String strnit = org_data[0].nit;
    /***********************************************************************************/

    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("Title", classInfo.name);
    parameters.put("company", strcompany);
    parameters.put("nit", strnit);
    parameters.put("Subtitle", strSubtitle);
    /* PASA LOS TOTALES AL JRXML INICIO AGF */
    parameters.put("TOTALDEBITO", v_Samtacctdr);
    parameters.put("TOTALCREDITO", v_Samtacctcr);
    parameters.put("TOTALSALDOINICIAL", v_Ssaldoinicial);
    parameters.put("TOTALSALDOFINAL", v_Ssaldofinal);
    /* PASA LOS TOTALES AL JRXML FINAL AGF */
    renderJR(vars, response, strReportName, strOutput, parameters, data, null);

    /* AGF */
    /*
     * LO QUE ESTA ACA ABJAO ES PARA EVNIAR EL ARCHIVO A UN DOCUMENTO .FO, PERO YA NO SE HACE POR
     * QUE SE ENVIA POR JRXML
     */

    /* if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet"); */
    /*
     * response.setContentType("text/html; charset=UTF-8"); PrintWriter out = response.getWriter();
     */
    /*
     * String discard[]={"selEliminar","sectionBP"};
     * 
     * XmlDocument xmlDocument=null; String strTreeOrg = ReportTrialRazonData.treeOrg(this,
     * vars.getClient()); String strOrgFamily = getFamily(strTreeOrg, strOrg); String strTreeAccount
     * = ReportTrialRazonData.treeAccount(this, vars.getClient()); ReportTrialRazonData [] data =
     * null; if (strDateFrom.equals("") && strDateTo.equals("")) { xmlDocument =
     * xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_reports/ReportTrialRazonPDF",
     * discard).createXmlDocument(); data = ReportTrialRazonData.set(); } else { if
     * (!strLevel.equals("S")) discard[0] = "selEliminarField"; else discard[0] = "discard";
     * 
     * 
     * if(strLevel.equals("S") && strOnly.equals("-1") ){
     * 
     * if (!(strAll.equals("")&&(strcBpartnerId.equals("")))) { if (log4j.isDebugEnabled())
     * log4j.debug("Select BP, strcBpartnerId:"+strcBpartnerId+" - strAll:"+strAll); if
     * (!strAll.equals("")) strcBpartnerId=""; discard[1] = "sectionNoBP"; data =
     * ReportTrialRazonData.selectBP(this, strDateFrom, strDateTo, strOrg, strOrgFamily,
     * Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"), Utility.getContext(this,
     * vars, "#User_Org", "ReportTrialRazon"), strDateFrom, DateTimeData.nDaysAfter(this,
     * strDateTo,"1"), strAccountFrom, strAccountTo,strcBpartnerId); } else { data =
     * ReportTrialRazonData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount,
     * strOrgFamily, Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"),
     * Utility.getContext(this, vars, "#User_Org", "ReportTrialRazon"), strDateFrom,
     * DateTimeData.nDaysAfter(this, strDateTo,"1"), strAccountFrom, strAccountTo); } }else{ data =
     * ReportTrialRazonData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount,
     * strOrgFamily, Utility.getContext(this, vars, "#User_Client", "ReportTrialRazon"),
     * Utility.getContext(this, vars, "#User_Org", "ReportTrialRazon"), strDateFrom,
     * DateTimeData.nDaysAfter(this, strDateTo,"1"),"",""); }
     * 
     * 
     * data = calculateTree(data, null, new Vector<Object>()); data = levelFilter(data, null, false,
     * strLevel); data = dataFilter(data); } ReportTrialRazonData [] new_data = null; if
     * (strOnly.equals("-1") && data!=null && data.length>0)new_data = filterTree(data, strLevel);
     * else new_data = data;
     * 
     * 
     * 
     * xmlDocument =
     * xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_reports/ReportTrialRazonPDF",
     * discard).createXmlDocument(); xmlDocument.setParameter("companyName",
     * ReportTrialRazonData.selectCompany(this, vars.getClient())); xmlDocument.setParameter("date",
     * DateTimeData.today(this)); xmlDocument.setParameter("period", strDateFrom + " - " +
     * strDateTo); if (strLevel.equals("S")) xmlDocument.setParameter("accounting",
     * "Cuenta inicio: "+ReportTrialRazonData.selectAccountingName(this,
     * strAccountFrom)+" - Cuenta fin: "+ReportTrialRazonData.selectAccountingName(this,
     * strAccountTo)); else xmlDocument.setParameter("accounting", "");
     * 
     * if (log4j.isDebugEnabled()) log4j.debug("filling structure, data.length:"+new_data.length);
     * if (log4j.isDebugEnabled()) log4j.debug("discard:"+discard[0]+","+discard[1]);
     * 
     * if (discard[1].equals("sectionNoBP")) xmlDocument.setData("structure2", new_data); else {
     * if(strOnly.equals("-1")) Arrays.sort(new_data, new ReportTrialRazonDataComparator());
     * xmlDocument.setData("structure1", new_data); }
     */
    /* ORDENAR COMIENZO */
    /* Arrays.sort(new_data, new ReportTrialRazonDataComparator()); */
    /* ORDENAR FIN */

    /*
     * String strResult = xmlDocument.print(); renderFO(strResult, response);
     */
    /*
     * out.println(xmlDocument.print()); out.close();
     */
    /* AGF */
  }

  private ReportTrialRazonData[] filterTree(ReportTrialRazonData[] data, String strLevel) {
    ArrayList<Object> arrayList = new ArrayList<Object>();
    for (int i = 0; data != null && i < data.length; i++) {
      if (data[i].elementlevel.equals(strLevel))
        arrayList.add(data[i]);
    }
    ReportTrialRazonData[] new_data = new ReportTrialRazonData[arrayList.size()];
    arrayList.toArray(new_data);
    return new_data;
  }

  private ReportTrialRazonData[] calculateTree(ReportTrialRazonData[] data, String indice,
      Vector<Object> vecTotal) {
    if (indice == null)
      indice = "0";
    ReportTrialRazonData[] result = null;
    Vector<Object> vec = new Vector<Object>();
    // if (log4j.isDebugEnabled())
    // log4j.debug("ReportTrialMajorBalanceData.calculateTree() - data: " + data.length);
    if (vecTotal == null)
      vecTotal = new Vector<Object>();
    if (vecTotal.size() == 0) {
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
    }
    double totalDR = Double.valueOf((String) vecTotal.elementAt(0)).doubleValue();
    double totalCR = Double.valueOf((String) vecTotal.elementAt(1)).doubleValue();
    double totalInicial = Double.valueOf((String) vecTotal.elementAt(2)).doubleValue();
    double totalFinal = Double.valueOf((String) vecTotal.elementAt(3)).doubleValue();
    boolean encontrado = false;
    for (int i = 0; i < data.length; i++) {
      if (data[i].parentId.equals(indice)) {
        encontrado = true;
        Vector<Object> vecParcial = new Vector<Object>();
        vecParcial.addElement("0");
        vecParcial.addElement("0");
        vecParcial.addElement("0");
        vecParcial.addElement("0");
        ReportTrialRazonData[] dataChilds = calculateTree(data, data[i].id, vecParcial);
        double parcialDR = Double.valueOf((String) vecParcial.elementAt(0)).doubleValue();
        double parcialCR = Double.valueOf((String) vecParcial.elementAt(1)).doubleValue();
        double parcialInicial = Double.valueOf((String) vecParcial.elementAt(2)).doubleValue();
        double parcialFinal = Double.valueOf((String) vecParcial.elementAt(3)).doubleValue();
        data[i].amtacctdr = Double.toString(Double.valueOf(data[i].amtacctdr).doubleValue()
            + parcialDR);
        data[i].amtacctcr = Double.toString(Double.valueOf(data[i].amtacctcr).doubleValue()
            + parcialCR);
        data[i].saldoInicial = Double.toString(Double.valueOf(data[i].saldoInicial).doubleValue()
            + parcialInicial);
        data[i].saldoFinal = Double.toString(Double.valueOf(data[i].saldoFinal).doubleValue()
            + parcialFinal);

        totalDR += Double.valueOf(data[i].amtacctdr).doubleValue();
        totalCR += Double.valueOf(data[i].amtacctcr).doubleValue();
        totalInicial += Double.valueOf(data[i].saldoInicial).doubleValue();
        totalFinal += Double.valueOf(data[i].saldoFinal).doubleValue();

        vec.addElement(data[i]);
        if (dataChilds != null && dataChilds.length > 0) {
          for (int j = 0; j < dataChilds.length; j++)
            vec.addElement(dataChilds[j]);
        }
      } else if (encontrado)
        break;
    }
    vecTotal.set(0, Double.toString(totalDR));
    vecTotal.set(1, Double.toString(totalCR));
    vecTotal.set(2, Double.toString(totalInicial));
    vecTotal.set(3, Double.toString(totalFinal));
    result = new ReportTrialRazonData[vec.size()];
    vec.copyInto(result);
    return result;
  }

  private ReportTrialRazonData[] dataFilter(ReportTrialRazonData[] data) {
    if (data == null || data.length == 0)
      return data;
    Vector<Object> dataFiltered = new Vector<Object>();
    for (int i = 0; i < data.length; i++) {
      if (Double.valueOf(data[i].amtacctdr).doubleValue() != 0.0
          || Double.valueOf(data[i].amtacctcr).doubleValue() != 0.0
          || Double.valueOf(data[i].saldoInicial).doubleValue() != 0.0
          || Double.valueOf(data[i].saldoFinal).doubleValue() != 0.0) {
        dataFiltered.addElement(data[i]);
      }
    }
    ReportTrialRazonData[] result = new ReportTrialRazonData[dataFiltered.size()];
    dataFiltered.copyInto(result);
    return result;
  }

  /* AJG-- INICIO FUNCION DE LOS FILTROS DE LAS CUENTAS */

  private ReportTrialRazonData[] AccountFilter(ReportTrialRazonData[] data, String indice,
      boolean found, String strAccountFrom, String strAccountTo) {
    if (data == null || data.length == 0 || strAccountFrom == null || strAccountFrom.equals("")
        || strAccountTo == null || strAccountTo.equals(""))
      return data;
    Vector<Object> dataFiltered = new Vector<Object>();
    /* INICIO Convierte a Long el Codigo de la cuenta que viene String */
    long DESDE = Long.parseLong(strAccountFrom);
    long HASTA = Long.parseLong(strAccountTo);
    /* FIN Convierte a Long el Codigo de la cuenta que viene String */
    for (int i = 0; i < data.length; i++) {
      if (data[i].organizador != null && (Long.parseLong(data[i].organizador) >= DESDE)
          && (Long.parseLong(data[i].organizador) <= HASTA)) {
        dataFiltered.addElement(data[i]);
      }
    }
    ReportTrialRazonData[] result = new ReportTrialRazonData[dataFiltered.size()];
    dataFiltered.copyInto(result);
    return result;
  }

  /* AJG-- FIN FUNCION DE LOS FILTROS DE LAS CUENTAS */

  private ReportTrialRazonData[] levelFilter(ReportTrialRazonData[] data, String indice,
      boolean found, String strLevel) {
    if (data == null || data.length == 0 || strLevel == null || strLevel.equals(""))
      return data;
    ReportTrialRazonData[] result = null;
    Vector<Object> vec = new Vector<Object>();
    // if (log4j.isDebugEnabled()) log4j.debug("ReportTrialMayorData.levelFilter() - data: " +
    // data.length);

    if (indice == null)
      indice = "0";
    for (int i = 0; i < data.length; i++) {
      if (data[i].parentId.equals(indice)
          && (!found || data[i].elementlevel.equalsIgnoreCase(strLevel))) {
        ReportTrialRazonData[] dataChilds = levelFilter(data, data[i].id,
            (found || data[i].elementlevel.equals(strLevel)), strLevel);
        vec.addElement(data[i]);
        if (dataChilds != null && dataChilds.length > 0)
          for (int j = 0; j < dataChilds.length; j++)
            vec.addElement(dataChilds[j]);
      }
    }
    result = new ReportTrialRazonData[vec.size()];
    vec.copyInto(result);
    vec.clear();
    return result;
  }

  private ReportTrialRazonData[] addFilter(ReportTrialRazonData[] data, String strLevel) {
    if (data == null || data.length == 0 || strLevel == null || strLevel.equals(""))
      return data;
    ReportTrialRazonData[] result = null;
    Vector<Object> addFiltered = new Vector<Object>();

    if ((strLevel).equals("B")) {
      for (int i = 0; i < data.length; i++) {
        if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
            || (data[i].elementlevel).equals("B") || (data[i].elementlevel).equals("S")
            || (data[i].elementlevel).equals("C") || (data[i].elementlevel).equals("D")
            || (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
          if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")
              && !(data[i].elementlevel).equals("B") && !(data[i].elementlevel).equals("S")
              && !(data[i].elementlevel).equals("C") && !(data[i].elementlevel).equals("D")
              && !(data[i].elementlevel).equals("E")) {
            (data[i].accountId) = "";
            (data[i].name) = "";
            addFiltered.addElement(data[i]);
          } else {
            addFiltered.addElement(data[i]);
          }
        }
      }
    }

    if ((strLevel).equals("E")) {
      for (int i = 0; i < data.length; i++) {
        if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
            || (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
          if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")) {
            (data[i].accountId) = "";
            (data[i].name) = "";
            addFiltered.addElement(data[i]);
          } else {
            addFiltered.addElement(data[i]);
          }

        }

      }
    }
    if ((strLevel).equals("C")) {
      for (int i = 0; i < data.length; i++) {
        if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
            || (data[i].elementlevel).equals("C") || (data[i].elementlevel).equals("D")
            || (data[i].elementlevel).equals("E") && (data[i].parentId).equals("0")) {
          if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")
              && !(data[i].elementlevel).equals("C") && !(data[i].elementlevel).equals("D")
              && !(data[i].elementlevel).equals("E")) {
            (data[i].accountId) = "";
            (data[i].name) = "";
            addFiltered.addElement(data[i]);
          } else {
            addFiltered.addElement(data[i]);
          }
        }
      }
    }

    if ((strLevel).equals("D")) {
      for (int i = 0; i < data.length; i++) {
        if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
            || (data[i].elementlevel).equals("D") || (data[i].elementlevel).equals("E")
            && (data[i].parentId).equals("0")) {
          if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")
              && !(data[i].elementlevel).equals("D") && !(data[i].elementlevel).equals("E")) {
            (data[i].accountId) = "";
            (data[i].name) = "";
            addFiltered.addElement(data[i]);
          } else {
            addFiltered.addElement(data[i]);
          }
        }
      }
    }

    if ((strLevel).equals("S")) {
      for (int i = 0; i < data.length; i++) {
        if (!(data[i].tipdoc).equals(".") || (data[i].elementlevel).equals(strLevel)
            || (data[i].elementlevel).equals("S") || (data[i].elementlevel).equals("C")
            || (data[i].elementlevel).equals("D") || (data[i].elementlevel).equals("E")
            && (data[i].parentId).equals("0")) {
          if (!(data[i].tipdoc).equals(".") || !(data[i].parentId).equals("0")
              && !(data[i].elementlevel).equals("C") && !(data[i].elementlevel).equals("S")
              && !(data[i].elementlevel).equals("D") && !(data[i].elementlevel).equals("E")) {
            // (data[i].accountId) = "";
            // (data[i].name) = "";
            addFiltered.addElement(data[i]);
          } else {
            addFiltered.addElement(data[i]);
          }
        }
      }
    }

    result = new ReportTrialRazonData[addFiltered.size()];
    addFiltered.copyInto(result);
    addFiltered.clear();
    return result;
  }

  public String getFamily(String strTree, String strChild) throws IOException, ServletException {
    return Tree.getMembers(this, strTree, strChild);
    /*
     * ReportGeneralLedgerData [] data = ReportGeneralLedgerData.selectChildren(this, strTree,
     * strChild); String strFamily = ""; if(data!=null && data.length>0) { for (int i =
     * 0;i<data.length;i++){ if (i>0) strFamily = strFamily + ","; strFamily = strFamily +
     * data[i].id; } return strFamily; }else return "'1'";
     */
  }

  public String getServletInfo() {
    return "Servlet ReportTrialRazon. This Servlet was made by Eduardo Argal";
  } // end of getServletInfo() method
}
