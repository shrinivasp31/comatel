/*Fecha       Version         Programador   Descripcion del cambio
========================================================================
18/08/2009  2.40.0.9180.1    WONG          Se agregaron los cruces al reporte 
 */

package com.mitosis.custom.financial.reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class ReportExtractoTercero extends HttpSecureAppServlet {
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "reportExtractoTercero|DateFrom",
          "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "reportExtractoTercero|DateTo", "");
      String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN",
          "reportExtractoTercero|cBpartnerId", "");
      String strInvoice = vars.getGlobalVariable("inpInvoice", "reportExtractoTercero|Invoice",
          "IN");
      String strBS = vars.getGlobalVariable("inpBS", "reportExtractoTercero|BS", "BS");
      String strND = vars.getGlobalVariable("inpND", "reportExtractoTercero|ND", "ND");
      String strNC = vars.getGlobalVariable("inpNC", "reportExtractoTercero|NC", "NC");
      String strANT = vars.getGlobalVariable("inpANT", "reportExtractoTercero|ANT", "ANT");
      String strCHQ = vars.getGlobalVariable("inpCHQ", "reportExtractoTercero|CHQ", "CHQ");
      String strCHD = vars.getGlobalVariable("inpCHD", "reportExtractoTercero|CHD", "CHD");
      String strCRU = vars.getGlobalVariable("inpCRU", "reportExtractoTercero|CRU", "CRU");
      String strDoc0 = vars.getGlobalVariable("inpDoc0", "reportExtractoTercero|Doc0", "");
      String strOrderRef = vars.getGlobalVariable("inpOrderRef", "reportExtractoTercero|OrderRef",
          "");
      String strisSoTrx = vars.getGlobalVariable("inpisSoTrx", "reportExtractoTercero|isSoTrx", "");
      printPageDataSheet(response, vars, strDateFrom, strDateTo, strcBpartnerId, strInvoice, strBS,
          strND, strNC, strANT, strCHQ, strCHD, strCRU, strDoc0, strisSoTrx, strOrderRef);
    } else if (vars.commandIn("FIND")) {
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "reportExtractoTercero|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "reportExtractoTercero|DateTo");
      String strcBpartnerId = vars.getRequestInGlobalVariable("inpcBPartnerId_IN",
          "reportExtractoTercero|cBpartnerId");
      String strInvoice = vars.getRequestGlobalVariable("inpInvoice",
          "reportExtractoTercero|Invoice");
      String strBS = vars.getRequestGlobalVariable("inpBS", "reportExtractoTercero|BS");
      String strND = vars.getRequestGlobalVariable("inpND", "reportExtractoTercero|ND");
      String strNC = vars.getRequestGlobalVariable("inpNC", "reportExtractoTercero|NC");
      String strANT = vars.getRequestGlobalVariable("inpANT", "reportExtractoTercero|ANT");
      String strCHQ = vars.getRequestGlobalVariable("inpCHQ", "reportExtractoTercero|CHQ");
      String strCHD = vars.getRequestGlobalVariable("inpCHD", "reportExtractoTercero|CHD");
      String strCRU = vars.getRequestGlobalVariable("inpCRU", "reportExtractoTercero|CRU");
      String strDoc0 = vars.getRequestGlobalVariable("inpDoc0", "reportExtractoTercero|Doc0");
      String strOrderRef = vars.getRequestGlobalVariable("inpOrderRef",
          "reportExtractoTercero|OrderRef");
      String strisSoTrx = vars.getRequestGlobalVariable("inpisSoTrx",
          "reportExtractoTercero|isSoTrx");
      printPageDataSheet(response, vars, strDateFrom, strDateTo, strcBpartnerId, strInvoice, strBS,
          strND, strNC, strANT, strCHQ, strCHD, strCRU, strDoc0, strisSoTrx, strOrderRef);
      // setHistoryCommand(request, "FIND");
    } else if (vars.commandIn("XLS", "PDF")) {
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "reportExtractoTercero|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "reportExtractoTercero|DateTo");
      String strcBpartnerId = vars.getRequestInGlobalVariable("inpcBPartnerId_IN",
          "reportExtractoTercero|cBpartnerId");
      String strInvoice = vars.getRequestGlobalVariable("inpInvoice",
          "reportExtractoTercero|Invoice");
      String strBS = vars.getRequestGlobalVariable("inpBS", "reportExtractoTercero|BS");
      String strND = vars.getRequestGlobalVariable("inpND", "reportExtractoTercero|ND");
      String strNC = vars.getRequestGlobalVariable("inpNC", "reportExtractoTercero|NC");
      String strANT = vars.getRequestGlobalVariable("inpANT", "reportExtractoTercero|ANT");
      String strCHQ = vars.getRequestGlobalVariable("inpCHQ", "reportExtractoTercero|CHQ");
      String strCHD = vars.getRequestGlobalVariable("inpCHD", "reportExtractoTercero|CHD");
      String strCRU = vars.getRequestGlobalVariable("inpCRU", "reportExtractoTercero|CRU");
      String strDoc0 = vars.getRequestGlobalVariable("inpDoc0", "reportExtractoTercero|Doc0");
      String strOrderRef = vars.getRequestGlobalVariable("inpOrderRef",
          "reportExtractoTercero|OrderRef");
      String strisSoTrx = vars.getRequestGlobalVariable("inpisSoTrx",
          "reportExtractoTercero|isSoTrx");
      try {
        printPageExcel(response, vars, strDateFrom, strDateTo, strcBpartnerId, strInvoice, strBS,
            strND, strNC, strANT, strCHQ, strCHD, strCRU, strDoc0, strisSoTrx, strOrderRef);
      } catch (ParseException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    } else
      pageError(response);
  }

  void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strcBpartnerId, String strInvoice, String strBS,
      String strND, String strNC, String strANT, String strCHQ, String strCHD, String strCRU,
      String strDoc0, String strIsSoTrx, String strOrderRef) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = null;
    strIsSoTrx = (strIsSoTrx.equals("") ? "N" : strIsSoTrx);
    strDoc0 = (strDoc0.equals("N") ? "" : strDoc0);
    ReportExtractoTerceroData[] data = null;
    String strCols = new String("");
    String discard[] = { "" };
    if ((strDateFrom.equals("") && strDateTo.equals(""))) {
      data = ReportExtractoTerceroData.set();
      discard[0] = "sectionPartner";
    } else {
      String strDocTypes = "'" + strInvoice + "','" + strBS + "','" + strNC + "','" + strND + "','"
          + strANT + "','" + strCHQ + "','" + strCHD + "','" + strCRU + "'";
      // System.out.println("Doco= " + strDoc0);
      String order = null;
      if (strOrderRef.equals("Y")) {
        order = "NAME,DOCREF,DATETRX,EFECTOS.DOCTYPE";
      } else {
        order = "NAME,DATETRX,EFECTOS.DOCTYPE";
      }

      data = ReportExtractoTerceroData.select(this,/* strcBpartnerId, */strIsSoTrx, strDateFrom,
          strDateTo,/* strDoc0, */strcBpartnerId, strDocTypes, order);

    }

    String strInvoiceId = new String(), strumNotaId = new String(), strcCashId = new String(), strcBankstatementId = new String();

    for (int i = 0; i < data.length; i++) {
      if (data[i].doc.equals("IN")) {
        strInvoiceId = data[i].invoiceId;
        strCols = "c_invoice_id";
      } else if (data[i].doc.equals("NC") || data[i].doc.equals("ND")) {
        if (data[i].invoiceId.equals("")) {
          strumNotaId = data[i].documentId;
          strCols = "um_notacredito_id";
        } else {
          strumNotaId = "";
        }
      } else {
        if (data[i].url.contains("Cash")) {
          strcCashId = data[i].invoiceId;
          strCols = " c_cashline_id";
        } else {
          strcBankstatementId = data[i].invoiceId;
          strCols = "c_bankstatementline_id";
        }

      }

      if (strInvoiceId.equals("") && strumNotaId.equals("") && strcCashId.equals("")
          && strcBankstatementId.equals("")) {
        data[i].saldo = "0";
      } else {
        data[i].saldo = ReportExtractoTerceroData.getSaldo(this, strcBpartnerId, strInvoiceId,
            strumNotaId, strcCashId, strcBankstatementId);
        // ReportExtractoTerceroData.get
        // (this, strCols, data[i].cBpartnerId,
        // strInvoiceId, strumNotaId, strcCashId, strcBankstatementId, strDoc0);
      }

      strInvoiceId = "";
      strumNotaId = "";
      strcCashId = "";
      strcBankstatementId = "";

    }
    if (strDoc0.equals("Y")) {
      data = filtrarDatos(data);
    }

    xmlDocument = xmlEngine.readXmlTemplate(
        "com/mitosis/custom/financial/reports/ReportExtractoTercero", discard).createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "reportExtractoTercero", true, "", "",
        "imprimir();return true;", false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    toolbar
        .prepareRelationBarTemplate(false, false,
            "submitCommandForm('XLS', false, frmMain, 'ReportExtractoTercero.xls', 'EXCEL');return false;");
    xmlDocument.setParameter("toolbar", toolbar.toString());
    try {
      // KeyMap key = new KeyMap(this, vars, "ReportExtractoTercero.html");
      // xmlDocument.setParameter("keyMap", key.getReportKeyMaps());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.mitosis.custom.financial.reports.ReportExtractoTercero");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ReportExtractoTercero.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "ReportExtractoTercero.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("reportExtractoTercero");
      vars.removeMessage("reportExtractoTercero");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("BS", strBS);
    xmlDocument.setParameter("ND", strND);
    xmlDocument.setParameter("NC", strNC);
    xmlDocument.setParameter("ANT", strANT);
    xmlDocument.setParameter("CHQ", strCHQ);
    xmlDocument.setParameter("CHD", strCHD);
    xmlDocument.setParameter("CRU", strCRU);
    xmlDocument.setParameter("Doc0", strDoc0);
    xmlDocument.setParameter("OrderRef", strOrderRef);
    xmlDocument.setParameter("isSoTrx", strIsSoTrx);
    xmlDocument.setParameter("Invoice", strInvoice);
    xmlDocument.setData(
        "reportCBPartnerId_IN",
        "liststructure",
        ReportExtractoTerceroData.selectBpartner(this,
            Utility.getContext(this, vars, "#User_Org", ""),
            Utility.getContext(this, vars, "#User_Client", ""), strcBpartnerId));
    xmlDocument.setData("structure1", data);
    out.println(xmlDocument.print());
    out.close();
  }

  void printPageExcel(HttpServletResponse response, VariablesSecureApp vars, String strDateFrom,
      String strDateTo, String strcBpartnerId, String strInvoice, String strBS, String strND,
      String strNC, String strANT, String strCHQ, String strCHD, String strCRU, String strDoc0,
      String strIsSoTrx, String strOrderRef) throws IOException, ServletException, ParseException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Excel");
    strIsSoTrx = (strIsSoTrx.equals("") ? "N" : strIsSoTrx);
    strDoc0 = (strDoc0.equals("N") ? "" : strDoc0);
    ReportExtractoTerceroData[] data = null;
    String strCols = new String("");
    String discard[] = { "" };
    if ((strDateFrom.equals("") && strDateTo.equals(""))) {
      data = ReportExtractoTerceroData.set();
      discard[0] = "sectionPartner";
    } else {
      String strDocTypes = "'" + strInvoice + "','" + strBS + "','" + strNC + "','" + strND + "','"
          + strANT + "','" + strCHQ + "','" + strCHD + "','" + strCRU + "'";
      // System.out.println("Doco= " + strDoc0);
      String order = null;
      if (strOrderRef.equals("Y")) {
        order = "NAME,DOCREF,DATETRX,EFECTOS.DOCTYPE";
      } else {
        order = "NAME,DATETRX,EFECTOS.DOCTYPE";
      }

      data = ReportExtractoTerceroData.select(this,/* strcBpartnerId, */strIsSoTrx, strDateFrom,
          strDateTo,/* strDoc0, */strcBpartnerId, strDocTypes, order);

    }
    String strInvoiceId = new String(), strumNotaId = new String(), strcCashId = new String(), strcBankstatementId = new String();

    for (int i = 0; i < data.length; i++) {
      if (data[i].doc.equals("IN")) {
        strInvoiceId = data[i].invoiceId;
        strCols = "c_invoice_id";
      } else if (data[i].doc.equals("NC") || data[i].doc.equals("ND")) {
        if (data[i].invoiceId.equals("")) {
          strumNotaId = data[i].documentId;
          strCols = "um_notacredito_id";
        } else {
          strumNotaId = "";
        }
      } else {
        if (data[i].url.contains("Cash")) {
          strcCashId = data[i].invoiceId;
          strCols = " c_cashline_id";
        } else {
          strcBankstatementId = data[i].invoiceId;
          strCols = "c_bankstatementline_id";
        }
      }

      if (strInvoiceId.equals("") && strumNotaId.equals("") && strcCashId.equals("")
          && strcBankstatementId.equals("")) {
        data[i].saldo = "0";
      } else {
        data[i].saldo = ReportExtractoTerceroData.getSaldo(this, strcBpartnerId, strInvoiceId,
            strumNotaId, strcCashId, strcBankstatementId);
        // ReportExtractoTerceroData.getSaldo(this, data[i].cBpartnerId, strInvoiceId,
        // strumNotaId, strcCashId, strcBankstatementId, strDoc0);
      }

      strInvoiceId = "";
      strumNotaId = "";
      strcCashId = "";
      strcBankstatementId = "";
    }
    if (strDoc0.equals("Y")) {
      data = filtrarDatos(data);
    }

    // JasperPrint jasperPrint;
    String strOutput = vars.commandIn("PDF") ? "pdf" : "xls";
    String strTitulo = strIsSoTrx.equals("Y") ? "Extracto de Cliente" : "Extracto de Proveedor";
    String strReportName = "@basedesign@/com/mitosis/custom/financial/reports/rptUmExtractoTercero.jrxml";
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date dteIni = dateFormat.parse(strDateFrom);
    Date dteFin = dateFormat.parse(strDateTo);

    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("ad_user_name", ReportExtractoTerceroData.getTitleUser(this, vars.getUser()));
    parameters.put("clientName", ReportExtractoTerceroData.getTitleClient(this, vars.getClient()));
    parameters.put("RoleName", ReportExtractoTerceroData.getTitleRole(this, vars.getRole()));
    parameters.put("NIT", ReportExtractoTerceroData.getTitleNIT(this, vars.getOrg()));
    parameters.put("titulo", strTitulo);
    parameters.put("FecIni", dteIni);
    parameters.put("FecFin", dteFin);
    renderJR(vars, response, strReportName, strOutput, parameters, data, null);
  }

  public String getServletInfo() {
    return "Servlet reportExtractoTercero. This Servlet was made by Diego Wong";
  } // end of getServletInfo() method

  private ReportExtractoTerceroData[] filtrarDatos(ReportExtractoTerceroData[] data) {
    if (data == null || data.length == 0)
      return data;
    Vector<Object> dataFiltered = new Vector<Object>();
    for (int i = 0; i < data.length; i++) {
      if (!data[i].saldo.equals("0")) {
        dataFiltered.addElement(data[i]);
      }
    }
    ReportExtractoTerceroData[] result = new ReportExtractoTerceroData[dataFiltered.size()];
    dataFiltered.copyInto(result);
    return result;
  }

}
