package com.mitosis.custom.warehousemgnt.ad_forms;

/************************************************************************
 Fecha       Version         Programador   Descripcion del cambio
 ========================================================================
 06/10/2009  2.40.0.9180.2   LSA			  Se verifican permisos de rol para escritura sobre el
 formulario.
 25/02/2009  2.40.0.9180.1   WONG          Creacion 
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.DateTimeData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class WM_StatusUpdateInOut extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {

      String strOrgId = vars.getGlobalVariable("inpOrgId", "WM_UpdateStatusInOut|inpOrgId", "");
      String strDateFrom = vars.getGlobalVariable("inpDateFrom",
          "WM_UpdateStatusInOut|inpDateFrom", "");
      String strEndDate = vars.getGlobalVariable("inpEndDate", "WM_UpdateStatusInOut|inpEndDate",
          "");
      String strBpartner = vars.getGlobalVariable("inpcBPartnerId",
          "WM_UpdateStatusInOut|inpcBPartnerId", "");
      String strEstadoRemi = vars.getGlobalVariable("inpEstado", "WM_UpdateStatusInOut|inpEstado",
          "");
      String strmInOutId = vars.getGlobalVariable("inpmInOutId",
          "WM_UpdateStatusInOut|inpmInOutId", "");
      String strRecoger = vars.getGlobalVariable("inpRecoger", "WM_UpdateStatusInOut|inpRecoger",
          "");
      String strCondutorId = vars.getGlobalVariable("inpConductorId",
          "WM_UpdateStatusInOut|inpConductorId", "");
      String strVehiculo = vars.getGlobalVariable("inpVehiculo",
          "WM_UpdateStatusInOut|inpVehiculo", "");

      printPageDataSheet(response, vars, strOrgId, strDateFrom, strEndDate, strBpartner,
          strEstadoRemi, strmInOutId, strRecoger, strCondutorId, strVehiculo);

    } else if (vars.commandIn("FIND")) {

      String strOrgId = vars.getRequestGlobalVariable("inpOrgId", "WM_UpdateStatusInOut|inpOrgId");
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "WM_UpdateStatusInOut|inpDateFrom");
      String strEndDate = vars.getRequestGlobalVariable("inpEndDate",
          "WM_UpdateStatusInOut|inpEndDate");
      String strBpartner = vars.getRequestGlobalVariable("inpcBPartnerId",
          "WM_UpdateStatusInOut|inpcBPartnerId");
      String strEstadoRemi = vars.getRequestGlobalVariable("inpEstado",
          "WM_UpdateStatusInOut|inpEstado");
      String strRemi = vars.getRequestGlobalVariable("inpmInOutId",
          "WM_UpdateStatusInOut|inpmInOutId");
      String strRecoger = vars.getRequestGlobalVariable("inpRecoger",
          "WM_UpdateStatusInOut|inpRecoger");
      String strCondutorId = vars.getRequestGlobalVariable("inpConductorId",
          "WM_UpdateStatusInOut|inpConductorId");
      String strVehiculo = vars.getRequestGlobalVariable("inpVehiculo",
          "WM_UpdateStatusInOut|inpVehiculo");

      printPageDataSheet(response, vars, strOrgId, strDateFrom, strEndDate, strBpartner,
          strEstadoRemi, strRemi, strRecoger, strCondutorId, strVehiculo);

    } else if (vars.commandIn("SAVE")) {

      String strOrgId = vars.getRequestGlobalVariable("inpOrgId", "WM_UpdateStatusInOut|inpOrgId");
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "WM_UpdateStatusInOut|inpDateFrom");
      String strEndDate = vars.getRequestGlobalVariable("inpEndDate",
          "WM_UpdateStatusInOut|inpEndDate");
      String strBpartner = vars.getRequestGlobalVariable("inpcBPartnerId",
          "WM_UpdateStatusInOut|inpcBPartnerId");
      String strEstadoRemi = vars.getRequestGlobalVariable("inpEstado",
          "WM_UpdateStatusInOut|inpEstado");
      String strRemi = vars.getRequestGlobalVariable("inpmInOutId",
          "WM_UpdateStatusInOut|inpmInOutId");
      String strRecoger = vars.getRequestGlobalVariable("inpRecoger",
          "WM_UpdateStatusInOut|inpRecoger");
      String strCondutorId = vars.getRequestGlobalVariable("inpConductorId",
          "WM_UpdateStatusInOut|inpConductorId");
      String strVehiculo = vars.getRequestGlobalVariable("inpVehiculo",
          "WM_UpdateStatusInOut|inpVehiculo");

      String strSelectedLines = vars.getRequiredInStringParameter("inpmInOut");
      OBError myMessage = processInOut(vars, strSelectedLines);
      vars.setMessage("WM_StatusUpdateInOut", myMessage);
      printPageDataSheet(response, vars, strOrgId, strDateFrom, strEndDate, strBpartner,
          strEstadoRemi, strRemi, strRecoger, strCondutorId, strVehiculo);

    } else
      pageError(response);
  }

  OBError processInOut(VariablesSecureApp vars, String strSelectedLines) throws IOException,
      ServletException {
    OBError myMessage = new OBError();
    Integer intUpdatedLines = 0;
    // Verificacion permisos de Rol
    String verifica = WMStatusUpdateInOutData.permisosRol(this, vars.getRole().toString());
    if (verifica.equals("N")) {
      myMessage.setTitle("Error");
      myMessage.setType("Error");
      myMessage
          .setMessage("No es posible efectuar cambios. El rol actual no tiene permisos de escritura.");
      return myMessage;
    }

    WMStatusUpdateInOutData data[] = WMStatusUpdateInOutData.selectedLines(this, strSelectedLines);
    String strRecoger = null, strSinAcuse = null, strShipped = null, strVehicle = null, strDriver = null, strDelivered = null, strTypDoc = null;

    for (int i = 0; i < data.length; i++) {
      strRecoger = vars.getStringParameter("inpRecogerGrid" + data[i].mInoutId, "N");
      strSinAcuse = vars.getStringParameter("inpSinAcuse" + data[i].mInoutId, "N");
      strShipped = vars.getStringParameter("inpTransporte" + data[i].mInoutId, "N");
      strDriver = vars.getStringParameter("inpConductorId" + data[i].mInoutId);
      strVehicle = vars.getStringParameter("inpVehiculoId" + data[i].mInoutId);
      strDelivered = vars.getStringParameter("inpEntregado" + data[i].mInoutId, "N");
      strTypDoc = "" + data[i].mInoutId.charAt(0);
      data[i].mInoutId = data[i].mInoutId.substring(1);

      if (strTypDoc.equals("O")) {
        intUpdatedLines += WMStatusUpdateInOutData.updateOrder(this, strRecoger, strDriver,
            strVehicle, data[i].mInoutId);
      } else {
        intUpdatedLines += WMStatusUpdateInOutData.updateInOut(this, strRecoger, strShipped,
            strSinAcuse, strDriver, strVehicle, strDelivered, data[i].mInoutId);
      }

    }
    myMessage.setTitle("");
    myMessage.setType("info");
    myMessage.setMessage("<ul> Se actualizaron " + intUpdatedLines + " Remisiones</ul>");
    return myMessage;
  }

  void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strOrgId,
      String strDateFrom, String strEndDate, String strBpartner, String strEstadoRemi,
      String strmInOutId, String strRecoger, String strCondutorId, String strVehiculo)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    if (strDateFrom.equals("")) {
      strDateFrom = DateTimeData.today(this);
    }

    if (strEndDate.equals("")) {
      strEndDate = DateTimeData.today(this);
    }

    if (strRecoger.equalsIgnoreCase("strRecoger")) {
      strRecoger = "Y";
    }

    XmlDocument xmlDocument = null;

    xmlDocument = xmlEngine.readXmlTemplate(
        "com/mitosis/custom/warehousemgnt/ad_forms/WM_StatusUpdateInOut").createXmlDocument();
    WMStatusUpdateInOutData data[] = WMStatusUpdateInOutData
        .select(this, strOrgId, strDateFrom, strEndDate, strBpartner, strEstadoRemi, strmInOutId,
            strVehiculo, strRecoger, strCondutorId);

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "WM_StatusUpdateInOut", false, "", "",
        "", false, "ad_forms", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.mitosis.custom.warehousemgnt.ad_forms.WM_StatusUpdateInOut");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "WM_StatusUpdateInOut.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "WM_StatusUpdateInOut.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("WM_StatusUpdateInOut");
      vars.removeMessage("WM_StatusUpdateInOut");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
          "", Utility.getContext(this, vars, "#User_Org", "WM_StatusUpdateInOut"),
          Utility.getContext(this, vars, "#User_Client", "WM_StatusUpdateInOut"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "WM_StatusUpdateInOut", "");
      xmlDocument.setData("reportAD_Org_ID", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("adOrgId", strOrgId);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("inpDateFrom", strDateFrom);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("inpEndDate", strEndDate);
    xmlDocument.setParameter("paramBPartnerId", strBpartner);
    xmlDocument.setParameter("paramBPartnerDescription", strBpartner.equals("") ? ""
        : WMStatusUpdateInOutData.bpDesc(this, strBpartner));
    xmlDocument.setParameter("paramInOutId", strmInOutId);
    xmlDocument.setParameter("paramInOutIdDes", strmInOutId.equals("") ? ""
        : WMStatusUpdateInOutData.inOutDesc(this, strmInOutId));
    xmlDocument.setParameter("paramRecoger", strRecoger);
    xmlDocument.setParameter("estado", strEstadoRemi);
    xmlDocument.setParameter("conductor", strCondutorId);
    xmlDocument.setParameter("Vehiculo", strVehiculo);

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "Estado Remision",
          "14D256D34CE244C3BECBAC21A839C8B0", "", Utility.getContext(this, vars, "#User_Org",
              "WM_StatusUpdateInOut"), Utility.getContext(this, vars, "#User_Client",
              "WM_StatusUpdateInOut"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "WM_StatusUpdateInOut", "");
      xmlDocument.setData("reportESTADO", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("paramInOutId", "");

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "Conductores",
          "050FDF60ADD84BE48F99916EF85363DE", "", Utility.getContext(this, vars, "#User_Org",
              "WM_StatusUpdateInOut"), Utility.getContext(this, vars, "#User_Client",
              "WM_StatusUpdateInOut"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "RequisitionToOrder", "");
      xmlDocument.setData("reportConductor", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "vehiculos",
          "D71C3E3509B347FBAFA9245BA1C65F2C", "", Utility.getContext(this, vars, "#User_Org",
              "WM_StatusUpdateInOut"), Utility.getContext(this, vars, "#User_Client",
              "WM_StatusUpdateInOut"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "WM_StatusUpdateInOut", "");
      xmlDocument.setData("reportVehiculo", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setData("structureInOut", data);

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "Conductores",
          "050FDF60ADD84BE48F99916EF85363DE", "", Utility.getContext(this, vars, "#User_Org",
              "WM_StatusUpdateInOut"), Utility.getContext(this, vars, "#User_Client",
              "WM_StatusUpdateInOut"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "RequisitionToOrder", "");
      xmlDocument.setData("reportConductorGrid", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "vehiculos",
          "D71C3E3509B347FBAFA9245BA1C65F2C", "", Utility.getContext(this, vars, "#User_Org",
              "WM_StatusUpdateInOut"), Utility.getContext(this, vars, "#User_Client",
              "WM_StatusUpdateInOut"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "RequisitionToOrder", "");
      xmlDocument.setData("reportVehiculoGrid", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    out.println(xmlDocument.print());
    out.close();
  }

}
