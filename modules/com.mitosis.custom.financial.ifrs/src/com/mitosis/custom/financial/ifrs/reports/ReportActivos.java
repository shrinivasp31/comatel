/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SL 
 * All portions are Copyright (C) 2001-2006 Openbravo SL 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.mitosis.custom.financial.ifrs.reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.businessUtility.AccountingSchemaMiscData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.TreeData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class ReportActivos extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "ReportActivos|DateFrom", "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "ReportActivos|DateTo", "");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "");
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId", "");
      String strcAssetCategoryId = vars.getRequestGlobalVariable("inpcAssetCategoryId", "");
      String strassetReportType = vars.getRequestGlobalVariable("inpreporttype", "");
      printPageDataSheet(response, vars, strDateFrom, strOrg, strDateTo, strcAcctSchemaId,
          strcAssetCategoryId, strassetReportType);
    } else if (vars.commandIn("FIND")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "ReportActivos|DateFrom", "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "ReportActivos|DateTo", "");
      String strValue = vars.getStringParameter("inpValue", "");
      String strDescription = vars.getStringParameter("inpDescription", "");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "ReportActivos|Org");
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "ReportActivos|cAcctSchemaId");
      String strcAssetCategoryId = vars.getRequestGlobalVariable("inpcAssetCategoryId",
          "ReportActivos|cAssetCategoryId");
      String strassetReportType = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "ReportActivos|assetReportType");
      printPageDataHtml(response, vars, strDateFrom, strDateTo, strValue, strDescription,
          strcAssetCategoryId, strcAcctSchemaId, strOrg);

    } else if (vars.commandIn("PDF", "EXCEL")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "ReportActivos|DateFrom", "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "ReportActivos|DateTo", "");
      String strValue = vars.getStringParameter("inpValue", "");
      String strDescription = vars.getStringParameter("inpDescription", "");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "ReportActivos|Org");
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "ReportActivos|cAcctSchemaId");
      String strcAssetCategoryId = vars.getRequestGlobalVariable("inpcAssetCategoryId",
          "ReportActivos|cAssetCategoryId");
      String strassetReportType = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "ReportActivos|assetReportType"); // Show
      // String strOnlyActiveMovement = vars.getRequestGlobalVariable("inpInvoice", "N");
      if (vars.commandIn("PDF")) {
        printPageDataPdf(response, vars, strDateFrom, strDateTo, strValue, strDescription,
            strcAssetCategoryId, strcAcctSchemaId, strOrg, strassetReportType);
      } else {
        printPageDataExcel(response, vars, strDateFrom, strDateTo, strValue, strDescription,
            strcAssetCategoryId, strcAcctSchemaId, strOrg, strassetReportType); // active
      }
    } else
      pageError(response);
  }

  private void printPageDataPdf(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strValue, String strDescription,
      String strcAssetCategoryId, String strcAcctSchemaId, String strOrg, String strassetReportType)
      throws IOException, ServletException {

    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");

    ReportActivosData[] pdfData = null;

    HashMap<String, Object> parameters = new HashMap<String, Object>();

    pdfData = ReportActivosData.select(this,
        Tree.getMembers(this, TreeData.getTreeOrg(this, vars.getClient()), strOrg), strOrg,
        strassetReportType, strDateFrom, strDateTo, strValue, strDescription, strcAssetCategoryId,
        strcAcctSchemaId);
    String strReportPath = "@basedesign@/com/mitosis/custom/financial/ifrs/reports/ReportActivos.jrxml";
    renderJR(vars, response, strReportPath, "pdf", parameters, pdfData, null);
    printPageDataSheet(response, vars, strDateFrom, strOrg, strDateTo, strcAcctSchemaId,
        strcAssetCategoryId, strassetReportType);
  } // end of the printPageDataPdf() method

  private void printPageDataExcel(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strValue, String strDescription,
      String strcAssetCategoryId, String strcAcctSchemaId, String strOrg, String strassetReportType)
      throws IOException, ServletException {

    if (log4j.isDebugEnabled())
      log4j.debug("Output: ExcelSheet");
    response.setContentType("text/html; charset=UTF-8");

    ReportActivosData[] excelData = null;

    HashMap<String, Object> parameters = new HashMap<String, Object>();

    excelData = ReportActivosData.select(this,
        Tree.getMembers(this, TreeData.getTreeOrg(this, vars.getClient()), strOrg), strOrg,
        strassetReportType, strDateFrom, strDateTo, strValue, strDescription, strcAssetCategoryId,
        strcAcctSchemaId);
    String strReportPath = "@basedesign@/com/mitosis/custom/financial/ifrs/reports/ReportActivos.jrxml";
    renderJR(vars, response, strReportPath, "xls", parameters, excelData, null);
  } // end of the printPageDataPdf() method

  void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strOrg, String strDateTo, String strcAcctSchemaId,
      String strcAssetCategoryId, String strassetReportType) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    String strMessage = "";

    XmlDocument xmlDocument = null;
    xmlDocument = xmlEngine.readXmlTemplate(
        "com/mitosis/custom/financial/ifrs/reports/ReportActivos").createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ReportActivos", false, "", "", "",
        false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.mitosis.custom.financial.reports.ReportActivos");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ReportActivos.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "ReportActivos.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("ReportActivos");
      vars.removeMessage("ReportActivos");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("adOrgId", strOrg);
    xmlDocument.setParameter("cAssetCategoryId", strcAssetCategoryId);
    xmlDocument.setParameter("cAcctschemaId", strcAcctSchemaId);
    xmlDocument.setParameter("assetReportType", strassetReportType);
    xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage
        + "');"));
    try {
      OBContext.setAdminMode();
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "Asset Report Type",
          "7EDBD909F3DA448AB6CC8314F3E25656", "", Utility.getContext(this, vars,
              "#AccessibleOrgTree", "ReportActivos"), Utility.getContext(this, vars,
              "#User_Client", "ReportActivos"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "", "");
      xmlDocument.setData("reportEM_CMCFI_REPORTTYPE", "liststructure",
          comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    OBContext.restorePreviousMode();

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_ORG_ID", "",
          "", Utility.getContext(this, vars, "#AccessibleOrgTree", "ReportActivos"),
          Utility.getContext(this, vars, "#User_Client", "ReportActivos"), '*');
      comboTableData.fillParameters(null, "ReportActivos", "");
      xmlDocument.setData("reportAD_ORG_ID", "liststructure", comboTableData.select(false));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
          "A_Asset_Group_ID", "", "", Utility.getContext(this, vars, "#AccessibleOrgTree", ""),
          Utility.getContext(this, vars, "#User_Client", ""), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "", "");
      xmlDocument.setData("reportA_ASSET_GROUP_ID", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setData(
        "reportC_ACCTSCHEMA_ID",
        "liststructure",
        AccountingSchemaMiscData.selectC_ACCTSCHEMA_ID(this,
            Utility.getContext(this, vars, "#AccessibleOrgTree", "ReportActivos"),
            Utility.getContext(this, vars, "#User_Client", "ReportActivos"), strcAcctSchemaId));
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageDataHtml(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strValue, String strDescription,
      String strcAssetCategoryId, String strcAcctSchemaId, String strOrg) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: HTML");
    response.setContentType("text/html; charset=UTF-8");

    ReportActivosData[] pdfData = null;

    HashMap<String, Object> parameters = new HashMap<String, Object>();
    //
    // pdfData = ReportActivosData.select(this,
    // Tree.getMembers(this, TreeData.getTreeOrg(this, vars.getClient()), strOrg));
    // String strReportPath =
    // "@basedesign@/com/mitosis/custom/financial/ifrs/reports/ReportActivos.jrxml";
    // renderJR(vars, response, strReportPath, "html", parameters, pdfData, null);
    // if (log4j.isDebugEnabled())
    // log4j.debug("Output: dataSheet");
    // response.setContentType("text/html; charset=UTF-8");
    // String strMessage = "";
    // // BigDecimal initialBalance= new BigDecimal(0);
    // ReportActivosData[] data = null;
    // if (strDateFrom.equals("")) {
    // String discard[] = { "sectionAmount" };
    // XmlDocument xmlDocument = null;
    // xmlDocument = xmlEngine.readXmlTemplate(
    // "com/mitosis/custom/financial/ifrs/reports/ReportActivos", discard).createXmlDocument();
    // data = ReportActivosData.set();
    // if (vars.commandIn("FIND")) {
    // strMessage = Utility.messageBD(this, "BothDatesCannotBeBlank", vars.getLanguage());
    // log4j.warn("Both dates are blank");
    // }
    //
    // ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ReportActivos", false, "", "", "",
    // false, "ad_reports", strReplaceWith, false, true);
    // toolbar.prepareSimpleToolBarTemplate();
    // xmlDocument.setParameter("toolbar", toolbar.toString());
    //
    // xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    // xmlDocument.setParameter("direction", "var baseDirection = \"" + strReplaceWith + "/\";\n");
    // xmlDocument.setParameter("paramLanguage", "LNG_POR_DEFECTO=\"" + vars.getLanguage() + "\";");
    // xmlDocument.setParameter("dateFrom", strDateFrom);
    // xmlDocument.setParameter("dateTo", strDateTo);
    // xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage
    // + "');"));
    // // xmlDocument.setData( "reportC_ACCOUNTNUMBER", "liststructure",
    // // AccountNumberComboData.select(this, Utility.getContext(this, vars, "#User_Client",
    // // "ReportActivos"), Utility.getContext(this, vars, "#User_Org", "ReportActivos")));
    // } else {
    // // data = ReportActivosData.select(this, vars.getUser());
    // String strFecha = new String(strDateFrom);
    // strDateFrom = ReportActivosData.getFecha(this, strDateFrom);
    // strDateFrom = "01" + strDateTo.substring(2);
    // strDateTo = ReportActivosData.getFecha(this, strDateFrom);
    // strDateTo = "01" + strDateTo.substring(2);
    // data = ReportActivosData.selectCorte(this, vars.getUser(), strFecha, strDateFrom);
    // }
    //
    // HashMap<String, Object> parameters = new HashMap<String, Object>();
    // parameters.put("user_id", vars.getUser());
    // String strReportPath =
    // "@basedesign@/com/mitosis/custom/financial/ifrs/reports/ReportActivos.jrxml";
    // renderJR(vars, response, strReportPath, "xls", parameters, data, null);
  }

  public String getServletInfo() {
    return "Servlet ReportActivos by LSA.";
  } // end of getServletInfo() method
}