package com.mitosis.custom.financial.ifrs.process;

import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.DocAmortizationTemplate;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.DocLine_Amortization;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.financialmgmt.assetmgmt.Asset;
import org.openbravo.model.financialmgmt.assetmgmt.AssetAccounts;

public class DocAmortization extends DocAmortizationTemplate {
  Logger log4jDocAmortization = Logger.getLogger(DocAmortization.class);

  @Override
  public Fact createFact(org.openbravo.erpCommon.ad_forms.DocAmortization docAmortization,
      AcctSchema as, ConnectionProvider conn, Connection con, VariablesSecureApp vars)
      throws ServletException {
    System.out.println("This is for testing purpose");
    Fact fact = null;
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    DocLine[] p_lines = docAmortization.p_lines;
    log4jDocAmortization.debug("createFact - object created");
    log4jDocAmortization.debug("createFact - p_lines.length - " + p_lines.length);
    // Lines
    String SeqNo = "0";
    fact = new Fact(docAmortization, as, Fact.POST_Actual);
    for (int i = 0; p_lines != null && i < p_lines.length; i++) {
      DocLine_Amortization line = (DocLine_Amortization) p_lines[i];
      fact.createLine(line, docAmortization.getAccount(docAmortization.getACCTTYPE_Depreciation(),
          line.m_A_Asset_ID, as, conn), line.m_C_Currency_ID, line.Amount, "", Fact_Acct_Group_ID,
          docAmortization.nextSeqNo(SeqNo), docAmortization.DocumentType, conn);
      fact.createLine(line, docAmortization.getAccount(
          docAmortization.getACCTTYPE_AccumDepreciation(), line.m_A_Asset_ID, as, conn),
          line.m_C_Currency_ID, "", line.Amount, Fact_Acct_Group_ID, docAmortization
              .nextSeqNo(SeqNo), docAmortization.DocumentType, conn);
      // Asset asset = OBDal.getInstance().get(Asset.class, line.m_A_Asset_ID);
      // String amount = DocAmortizationData.selectAssetAcctAmt(conn, line.m_A_Asset_ID);
      // String assetAcctAmt = ""
      // + asset.getDepreciationAmt().subtract(new BigDecimal(amount != "" ? amount : "0"));
      // fact.createLine(line, getAssertAccount(asset, as, conn), line.m_C_Currency_ID, "",
      // assetAcctAmt, Fact_Acct_Group_ID, docAmortization.nextSeqNo(SeqNo),
      // docAmortization.DocumentType, conn);
    }
    SeqNo = "0";
    return fact;
    // return null;
  }

  public Account getAssertAccount(Asset asset, AcctSchema as, ConnectionProvider conn) {
    Account account = null;
    OBCriteria<AssetAccounts> assetAccoutsCriteria = OBDal.getInstance().createCriteria(
        AssetAccounts.class);
    assetAccoutsCriteria.add(Restrictions.eq(AssetAccounts.PROPERTY_ASSET, asset));
    assetAccoutsCriteria.add(Restrictions.eq(
        AssetAccounts.PROPERTY_ACCOUNTINGSCHEMA,
        OBDal.getInstance().get(org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
            as.m_C_AcctSchema_ID)));
    List<AssetAccounts> assetAccoutsList = assetAccoutsCriteria.list();
    if (assetAccoutsList.size() == 0 && assetAccoutsList.get(0).getCmcfiAssetAcct() == null) {
      throw new OBException("Please configure asset account in accounting tab in Asset window.");
    }
    try {
      account = Account.getAccount(conn, assetAccoutsList.get(0).getCmcfiAssetAcct().getId());
    } catch (ServletException e) {
      log4jDocAmortization.warn(e);
    }

    return account;
  }
}
