/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2013 Openbravo SLU 
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravofreelancer.accountingreports.ad_reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.AccountingSchemaMiscData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.TreeData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.info.SelectorUtilityData;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.DateTimeData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;
import org.openbravo.xmlEngine.XmlDocument;

public class AdvReportTrialBalance extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strcAcctSchemaId = vars.getGlobalVariable("inpcAcctSchemaId",
          "AdvReportTrialBalance|cAcctSchemaId", "");
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "AdvReportTrialBalance|DateFrom", "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "AdvReportTrialBalance|DateTo", "");
      String strPageNo = vars.getGlobalVariable("inpPageNo", "AdvReportTrialBalance|PageNo", "1");
      String strOrg = vars.getGlobalVariable("inpOrg", "AdvReportTrialBalance|Org", "");
      String strLevel = vars.getGlobalVariable("inpLevel", "AdvReportTrialBalance|Level", "");
      String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN",
          "AdvReportTrialBalance|cBpartnerId", "", IsIDFilter.instance);
      String strmProductId = vars.getInGlobalVariable("inpmProductId_IN",
          "AdvReportTrialBalance|mProductId", "", IsIDFilter.instance);
      String strcProjectId = vars.getInGlobalVariable("inpcProjectId_IN",
          "AdvReportTrialBalance|cProjectId", "", IsIDFilter.instance);
      String strGroupBy = vars.getGlobalVariable("inpGroupBy", "AdvReportTrialBalance|GroupBy", "");
      String strcElementValueFrom = vars.getGlobalVariable("inpcElementValueIdFrom",
          "AdvReportTrialBalance|C_ElementValue_IDFROM", "");
      String strcElementValueTo = vars.getGlobalVariable(
          "inpcElementValueIdTo",
          "AdvReportTrialBalance|C_ElementValue_IDTO",
          AdvReportTrialBalanceData.selectLastAccount(this,
              Utility.getContext(this, vars, "#AccessibleOrgTree", "Account"),
              Utility.getContext(this, vars, "#User_Client", "Account")));
      String strNotInitialBalance = vars.getGlobalVariable("inpNotInitialBalance",
          "AdvReportTrialBalance|notInitialBalance", "Y");
      String strcElementValueFromDes = "", strcElementValueToDes = "";
      if (!strcElementValueFrom.equals(""))
        strcElementValueFromDes = AdvReportTrialBalanceData.selectSubaccountDescription(this,
            strcElementValueFrom);
      if (!strcElementValueTo.equals(""))
        strcElementValueToDes = AdvReportTrialBalanceData.selectSubaccountDescription(this,
            strcElementValueTo);
      strcElementValueFromDes = (strcElementValueFromDes == null) ? "" : strcElementValueFromDes;
      strcElementValueToDes = (strcElementValueToDes == null) ? "" : strcElementValueToDes;
      vars.setSessionValue("inpElementValueIdFrom_DES", strcElementValueFromDes);
      vars.setSessionValue("inpElementValueIdTo_DES", strcElementValueToDes);

      printPageDataSheet(response, vars, strDateFrom, strDateTo, strPageNo, strOrg, strLevel,
          strcElementValueFrom, strcElementValueTo, strcElementValueFromDes, strcElementValueToDes,
          strcBpartnerId, strmProductId, strcProjectId, strcAcctSchemaId, strNotInitialBalance,
          strGroupBy);

    } else if (vars.commandIn("FIND")) {
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "AdvReportTrialBalance|cAcctSchemaId");
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "AdvReportTrialBalance|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "AdvReportTrialBalance|DateTo");
      String strPageNo = vars.getRequestGlobalVariable("inpPageNo", "AdvReportTrialBalance|PageNo");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "AdvReportTrialBalance|Org");
      String strLevel = vars.getRequestGlobalVariable("inpLevel", "AdvReportTrialBalance|Level");
      String strcBpartnerId = vars.getRequestInGlobalVariable("inpcBPartnerId_IN",
          "AdvReportTrialBalance|cBpartnerId", IsIDFilter.instance);
      String strmProductId = vars.getRequestInGlobalVariable("inpmProductId_IN",
          "AdvReportTrialBalance|mProductId", IsIDFilter.instance);
      String strcProjectId = vars.getRequestInGlobalVariable("inpcProjectId_IN",
          "AdvReportTrialBalance|cProjectId", IsIDFilter.instance);
      String strGroupBy = vars.getRequestGlobalVariable("inpGroupBy", "AdvReportTrialBalance|GroupBy");
      String strcElementValueFrom = vars.getRequestGlobalVariable("inpcElementValueIdFrom",
          "AdvReportTrialBalance|C_ElementValue_IDFROM");
      String strcElementValueTo = vars.getRequestGlobalVariable("inpcElementValueIdTo",
          "AdvReportTrialBalance|C_ElementValue_IDTO");
      String strNotInitialBalance = vars.getStringParameter("inpNotInitialBalance", "N");
      vars.setSessionValue("AdvReportTrialBalance|notInitialBalance", strNotInitialBalance);
      String strcElementValueFromDes = "", strcElementValueToDes = "";
      if (!strcElementValueFrom.equals(""))
        strcElementValueFromDes = AdvReportTrialBalanceData.selectSubaccountDescription(this,
            strcElementValueFrom);
      if (!strcElementValueTo.equals(""))
        strcElementValueToDes = AdvReportTrialBalanceData.selectSubaccountDescription(this,
            strcElementValueTo);
      vars.setSessionValue("inpElementValueIdFrom_DES", strcElementValueFromDes);
      vars.setSessionValue("inpElementValueIdTo_DES", strcElementValueToDes);

      printPageDataSheet(response, vars, strDateFrom, strDateTo, strPageNo, strOrg, strLevel,
          strcElementValueFrom, strcElementValueTo, strcElementValueFromDes, strcElementValueToDes,
          strcBpartnerId, strmProductId, strcProjectId, strcAcctSchemaId, strNotInitialBalance,
          strGroupBy);

    } else if (vars.commandIn("PDF", "XLS")) {
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "AdvReportTrialBalance|cAcctSchemaId");
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "AdvReportTrialBalance|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "AdvReportTrialBalance|DateTo");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "AdvReportTrialBalance|Org");
      String strLevel = vars.getRequestGlobalVariable("inpLevel", "AdvReportTrialBalance|Level");
      String strcElementValueFrom = vars.getGlobalVariable("inpcElementValueIdFrom",
          "AdvReportTrialBalance|C_ElementValue_IDFROM", "");
      String strcElementValueTo = vars.getGlobalVariable("inpcElementValueIdTo",
          "AdvReportTrialBalance|C_ElementValue_IDTO", "");
      String strcElementValueFromDes = "", strcElementValueToDes = "";
      if (!strcElementValueFrom.equals(""))
        strcElementValueFromDes = AdvReportTrialBalanceData.selectSubaccountDescription(this,
            strcElementValueFrom);
      if (!strcElementValueTo.equals(""))
        strcElementValueToDes = AdvReportTrialBalanceData.selectSubaccountDescription(this,
            strcElementValueTo);
      strcElementValueFromDes = (strcElementValueFromDes == null) ? "" : strcElementValueFromDes;
      strcElementValueToDes = (strcElementValueToDes == null) ? "" : strcElementValueToDes;
      String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN",
          "AdvReportTrialBalance|cBpartnerId", "", IsIDFilter.instance);
      String strmProductId = vars.getInGlobalVariable("inpmProductId_IN",
          "AdvReportTrialBalance|mProductId", "", IsIDFilter.instance);
      String strcProjectId = vars.getInGlobalVariable("inpcProjectId_IN",
          "AdvReportTrialBalance|cProjectId", "", IsIDFilter.instance);
      String strGroupBy = vars.getRequestGlobalVariable("inpGroupBy", "AdvReportTrialBalance|GroupBy");
      String strPageNo = vars.getRequestGlobalVariable("inpPageNo", "AdvReportTrialBalance|PageNo");
      String strNotInitialBalance = vars.getStringParameter("inpNotInitialBalance", "N");
      vars.setSessionValue("AdvReportTrialBalance|notInitialBalance", strNotInitialBalance);

      if (vars.commandIn("PDF"))
        printPageDataPDF(request, response, vars, strDateFrom, strDateTo, strOrg, strLevel,
            strcElementValueFrom, strcElementValueFromDes, strcElementValueTo,
            strcElementValueToDes, strcBpartnerId, strmProductId, strcProjectId, strcAcctSchemaId,
            strNotInitialBalance, strGroupBy, strPageNo);
      else
        printPageDataXLS(request, response, vars, strDateFrom, strDateTo, strOrg, strLevel,
            strcElementValueFrom, strcElementValueTo, strcBpartnerId, strmProductId, strcProjectId,
            strcAcctSchemaId, strNotInitialBalance, strGroupBy);

    } else if (vars.commandIn("OPEN")) {
      String strAccountId = vars.getRequiredStringParameter("inpcAccountId");
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "AdvReportTrialBalance|cAcctSchemaId");
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "AdvReportTrialBalance|DateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo", "AdvReportTrialBalance|DateTo");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "AdvReportTrialBalance|Org");
      String strLevel = vars.getRequestGlobalVariable("inpLevel", "AdvReportTrialBalance|Level");
      String strcBpartnerId = vars.getInGlobalVariable("inpcBPartnerId_IN",
          "AdvReportTrialBalance|cBpartnerId", "", IsIDFilter.instance);
      String strmProductId = vars.getInGlobalVariable("inpmProductId_IN",
          "AdvReportTrialBalance|mProductId", "", IsIDFilter.instance);
      String strcProjectId = vars.getInGlobalVariable("inpcProjectId_IN",
          "AdvReportTrialBalance|cProjectId", "", IsIDFilter.instance);
      String strGroupBy = vars.getRequestGlobalVariable("inpGroupBy", "AdvReportTrialBalance|GroupBy");
      String strNotInitialBalance = vars.getStringParameter("inpNotInitialBalance", "N");
      vars.setSessionValue("AdvReportTrialBalance|notInitialBalance", strNotInitialBalance);

      printPageOpen(response, vars, strDateFrom, strDateTo, strOrg, strLevel, strcBpartnerId,
          strmProductId, strcProjectId, strcAcctSchemaId, strGroupBy, strAccountId,
          strNotInitialBalance);

    } else {
      pageError(response);
    }
  }

  private void printPageOpen(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strOrg, String strLevel, String strcBpartnerId,
      String strmProductId, String strcProjectId, String strcAcctSchemaId, String strGroupBy,
      String strAccountId, String strNotInitialBalance) throws IOException, ServletException {

    AdvReportTrialBalanceData[] data = null;
    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);

    log4j.debug("Output: Expand subaccount details " + strAccountId);

    data = AdvReportTrialBalanceData.selectAccountLines(this, strGroupBy, vars.getLanguage(),
        strLevel, strOrgFamily,
        Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"),
        Utility.getContext(this, vars, "#AccessibleOrgTree", "AdvReportTrialBalance"), null, null,
        strDateFrom, strAccountId, strcBpartnerId, strmProductId, strcProjectId, strcAcctSchemaId,
        (strNotInitialBalance.equals("Y") ? "O" : "P"),
        DateTimeData.nDaysAfter(this, strDateTo, "1"));

    if (data == null) {
      data = AdvReportTrialBalanceData.set();
    }

    // response.setContentType("text/plain");
    response.setContentType("text/html; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter out = response.getWriter();

    // Create JSON object
    // { "rows" : [ {"td1":"Bellen Ent.","td3":"0,00","td2":"0,00","td5":"-48,59","td4":"48,59"},
    // {"td1":"Mafalda Corporation","td3":"34,56","td2":"0,00","td5":"-334,79","td4":"369,35"}],
    // "config" : {"classDefault":"DataGrid_Body_Cell","classAmount":"DataGrid_Body_Cell_Amount"}
    // }
    DecimalFormat df = Utility.getFormat(vars, "euroInform");
    JSONObject table = new JSONObject();
    JSONArray tr = new JSONArray();
    Map<String, String> tds = null;
    try {

      for (int i = 0; i < data.length; i++) {
        tds = new HashMap<String, String>();
        tds.put("td1", data[i].groupbyname);
        tds.put("td2", df.format(new BigDecimal(data[i].saldoInicial)));
        tds.put("td3", df.format(new BigDecimal(data[i].amtacctdr)));
        tds.put("td4", df.format(new BigDecimal(data[i].amtacctcr)));
        tds.put("td5", df.format(new BigDecimal(data[i].saldoFinal)));
        tr.put(data.length - (i + 1), tds);
        table.put("rows", tr);
      }
      Map<String, String> props = new HashMap<String, String>();
      props.put("classAmount", "DataGrid_Body_Cell_Amount");
      props.put("classDefault", "DataGrid_Body_Cell");
      table.put("config", props);

    } catch (JSONException e) {
      log4j.error("Error creating JSON object for representing subaccount lines", e);
      throw new ServletException(e);
    }

    log4j.debug("JSON string: " + table.toString());

    out.println("jsonTable = " + table.toString());
    out.close();
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strPageNo, String strOrg, String strLevel,
      String strcElementValueFrom, String strcElementValueTo, String strcElementValueFromDes,
      String strcElementValueToDes, String strcBpartnerId, String strmProductId,
      String strcProjectId, String strcAcctSchemaId, String strNotInitialBalance, String strGroupBy)
      throws IOException, ServletException {

    String strMessage = "";
    XmlDocument xmlDocument = null;
    AdvReportTrialBalanceData[] data = null;
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();

    String discard[] = { "sectionGridView", "discard", "discard", "discard" };

    if (strLevel.equals("C")) {
      discard[1] = "fieldId1";
    } else {
      discard[1] = "fieldDescAccount";
    }

    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);
    String strTreeAccount = AdvReportTrialBalanceData.treeAccount(this, vars.getClient());
    // Remember values
    String strcBpartnerIdAux = strcBpartnerId;
    String strmProductIdAux = strmProductId;
    String strcProjectIdAux = strcProjectId;

    String strAccountFromValue = AdvReportTrialBalanceData.selectAccountValue(this,
        strcElementValueFrom);
    String strAccountToValue = AdvReportTrialBalanceData.selectAccountValue(this, strcElementValueTo);

    log4j.debug("Output: DataSheet");
    log4j.debug("strTreeOrg: " + strTreeOrg + "strOrgFamily: " + strOrgFamily + "strTreeAccount: "
        + strTreeAccount);
    log4j.debug("strcBpartnerId: " + strcBpartnerId + "strmProductId: " + strmProductId
        + "strcProjectId: " + strcProjectId);

    if (strDateFrom.equals("") && strDateTo.equals("")) {
      xmlDocument = xmlEngine.readXmlTemplate(
          "com/openbravofreelancer/accountingreports/ad_reports/AdvReportTrialBalance", discard).createXmlDocument();
      data = AdvReportTrialBalanceData.set();
      if (vars.commandIn("FIND")) {
        strMessage = Utility.messageBD(this, "BothDatesCannotBeBlank", vars.getLanguage());
        log4j.warn("Both dates are blank");
      }
    } else {
      if (strLevel.equals("S")) { // SubAccount selected
        data = AdvReportTrialBalanceData.selectAccountLines(this, "", vars.getLanguage(), strLevel,
            strOrgFamily, Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"),
            Utility.getContext(this, vars, "#AccessibleOrgTree", "AdvReportTrialBalance"),
            strAccountFromValue, strAccountToValue, strDateFrom, null, strcBpartnerId,
            strmProductId, strcProjectId, strcAcctSchemaId, (strNotInitialBalance.equals("Y") ? "O"
                : "P"), DateTimeData.nDaysAfter(this, strDateTo, "1"));
        if (strGroupBy.equals(""))
          discard[2] = "showExpand";

      } else {
        discard[2] = "showExpand";
        data = getDataWhenNotSubAccount(vars, strDateFrom, strDateTo, strOrg, strOrgFamily,
            strcAcctSchemaId, strLevel, strTreeAccount, strNotInitialBalance);
      }

      if (data != null && data.length > 0)
        discard[0] = "discard";

    }

    xmlDocument = xmlEngine.readXmlTemplate(
        "com/openbravofreelancer/accountingreports/ad_reports/AdvReportTrialBalance", discard).createXmlDocument();
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "",
          "C_ElementValue level", "", Utility.getContext(this, vars, "#AccessibleOrgTree",
              "AdvReportTrialBalance"), Utility.getContext(this, vars, "#User_Client",
              "AdvReportTrialBalance"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "AdvReportTrialBalance", "");
      xmlDocument.setData("reportLevel", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "AdvReportTrialBalance", false, "", "",
        "if (validate()) { imprimir(); } return false;", false, "ad_reports", strReplaceWith,
        false, true);
    toolbar.setEmail(false);
    toolbar.prepareSimpleToolBarTemplate();
    toolbar
        .prepareRelationBarTemplate(
            false,
            false,
            "if (validate()) { submitCommandForm('XLS', false, frmMain, 'ReportTrialBalanceExcel.xls', 'EXCEL'); } return false;");
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.openbravofreelancer.accountingreports.ad_reports.AdvReportTrialBalance");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "AdvReportTrialBalance.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "AdvReportTrialBalance.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    OBError myMessage = vars.getMessage("AdvReportTrialBalance");
    vars.removeMessage("AdvReportTrialBalance");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_ORG_ID", "",
          "", Utility.getContext(this, vars, "#AccessibleOrgTree", "AdvReportTrialBalance"),
          Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"), '*');
      comboTableData.fillParameters(null, "AdvReportTrialBalance", "");
      xmlDocument.setData("reportAD_ORGID", "liststructure", comboTableData.select(false));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    xmlDocument
        .setData("reportC_ACCTSCHEMA_ID", "liststructure", AccountingSchemaMiscData
            .selectC_ACCTSCHEMA_ID(this,
                Utility.getContext(this, vars, "#AccessibleOrgTree", "AdvReportTrialBalance"),
                Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"),
                strcAcctSchemaId));
    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("PageNo", strPageNo);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("adOrgId", strOrg);
    xmlDocument.setParameter("Level", "".equals(strLevel) ? "S" : strLevel);
    xmlDocument.setParameter("cAcctschemaId", strcAcctSchemaId);
    xmlDocument.setParameter("paramElementvalueIdFrom", strcElementValueFrom);
    xmlDocument.setParameter("paramElementvalueIdTo", strcElementValueTo);
    xmlDocument.setParameter("inpElementValueIdFrom_DES", strcElementValueFromDes);
    xmlDocument.setParameter("inpElementValueIdTo_DES", strcElementValueToDes);
    xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage
        + "');"));
    xmlDocument.setParameter("groupbyselected", strGroupBy);
    xmlDocument.setParameter("notInitialBalance", strNotInitialBalance);

    xmlDocument.setData(
        "reportCBPartnerId_IN",
        "liststructure",
        SelectorUtilityData.selectBpartner(this,
            Utility.getContext(this, vars, "#AccessibleOrgTree", ""),
            Utility.getContext(this, vars, "#User_Client", ""), strcBpartnerIdAux));

    xmlDocument.setData(
        "reportMProductId_IN",
        "liststructure",
        SelectorUtilityData.selectMproduct(this,
            Utility.getContext(this, vars, "#AccessibleOrgTree", ""),
            Utility.getContext(this, vars, "#User_Client", ""), strmProductIdAux));

    xmlDocument.setData(
        "reportCProjectId_IN",
        "liststructure",
        SelectorUtilityData.selectProject(this,
            Utility.getContext(this, vars, "#AccessibleOrgTree", ""),
            Utility.getContext(this, vars, "#User_Client", ""), strcProjectIdAux));

    if (data != null && data.length > 0) {
      xmlDocument.setData("structure1", data);
    } else {
      if (vars.commandIn("FIND")) {
        // No data has been found. Show warning message.
        xmlDocument.setParameter("messageType", "WARNING");
        xmlDocument.setParameter("messageTitle",
            Utility.messageBD(this, "ProcessStatus-W", vars.getLanguage()));
        xmlDocument.setParameter("messageMessage",
            Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
      }
    }

    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageDataXLS(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strDateFrom, String strDateTo, String strOrg,
      String strLevel, String strcElementValueFrom, String strcElementValueTo,
      String strcBpartnerId, String strmProductId, String strcProjectId, String strcAcctSchemaId,
      String strNotInitialBalance, String strGroupBy) throws IOException, ServletException {

    response.setContentType("text/html; charset=UTF-8");
    AdvReportTrialBalanceData[] data = null;
    boolean showbpartner = false;
    boolean showproduct = false;
    boolean showProject = false;
    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);
    String strTreeAccount = AdvReportTrialBalanceData.treeAccount(this, vars.getClient());

    String strAccountFromValue = AdvReportTrialBalanceData.selectAccountValue(this,
        strcElementValueFrom);
    String strAccountToValue = AdvReportTrialBalanceData.selectAccountValue(this, strcElementValueTo);

    log4j.debug("Output: XLS report");
    log4j.debug("strTreeOrg: " + strTreeOrg + "strOrgFamily: " + strOrgFamily + "strTreeAccount: "
        + strTreeAccount);
    log4j.debug("strcBpartnerId: " + strcBpartnerId + "strmProductId: " + strmProductId
        + "strcProjectId: " + strcProjectId);

    if (!strDateFrom.equals("") && !strDateTo.equals("") && !strOrg.equals("")
        && !strcAcctSchemaId.equals("")) {

      if (strLevel.equals("S")) {
        data = AdvReportTrialBalanceData.selectXLS(this, vars.getLanguage(), strLevel, strOrgFamily,
            Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"),
            Utility.getContext(this, vars, "#AccessibleOrgTree", "AdvReportTrialBalance"),
            strAccountFromValue, strAccountToValue, strDateFrom, strcBpartnerId, strmProductId,
            strcProjectId, strcAcctSchemaId, (strNotInitialBalance.equals("Y") ? "O" : "P"),
            DateTimeData.nDaysAfter(this, strDateTo, "1"));
        if (strGroupBy.equals("BPartner")) {
          showbpartner = true;
          showproduct = false;
          showProject = false;
        } else if (strGroupBy.equals("Product")) {
          showbpartner = false;
          showproduct = true;
          showProject = false;
        } else if (strGroupBy.equals("Project")) {
          showbpartner = false;
          showproduct = false;
          showProject = true;
        } else {
          showbpartner = true;
          showproduct = true;
          showProject = true;
        }
      } else {
        data = getDataWhenNotSubAccount(vars, strDateFrom, strDateTo, strOrg, strOrgFamily,
            strcAcctSchemaId, strLevel, strTreeAccount, strNotInitialBalance);
      }

      if (data == null || data.length == 0) {
        advisePopUp(request, response, "WARNING",
            Utility.messageBD(this, "ProcessStatus-W", vars.getLanguage()),
            Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
      } else if (data.length > 65532) {
        advisePopUp(request, response, "ERROR",
            Utility.messageBD(this, "ProcessStatus-E", vars.getLanguage()),
            Utility.messageBD(this, "numberOfRowsExceeded", vars.getLanguage()));
      } else {

        AcctSchema acctSchema = OBDal.getInstance().get(AcctSchema.class, strcAcctSchemaId);

        String strReportName = "@basedesign@/com.openbravofreelancer.accountingreports.ad_reports/ReportTrialBalanceExcel.jrxml";

        HashMap<String, Object> parameters = new HashMap<String, Object>();

        String strLanguage = vars.getLanguage();

        StringBuilder strSubTitle = new StringBuilder();

        strSubTitle.append(Utility.messageBD(this, "LegalEntity", vars.getLanguage()) + ": ");
        strSubTitle.append(AdvReportTrialBalanceData.selectCompany(this, vars.getClient()) + " (");
        strSubTitle.append(Utility.messageBD(this, "ACCS_AD_ORG_ID_D", vars.getLanguage()) + ": ");
        strSubTitle.append(AdvReportTrialBalanceData.selectOrgName(this, strOrg) + ") \n");
        strSubTitle.append(Utility.messageBD(this, "asof", vars.getLanguage()) + ": " + strDateTo
            + " \n");
        strSubTitle.append(Utility.messageBD(this, "generalLedger", vars.getLanguage()) + ": "
            + acctSchema.getName());

        parameters.put("REPORT_SUBTITLE", strSubTitle.toString());
        parameters.put("SHOWTOTALS", false);
        parameters.put("SHOWBPARTNER", showbpartner);
        parameters.put("SHOWPRODUCT", showproduct);
        parameters.put("SHOWPROJECT", showProject);
        parameters.put("DATE_FROM", strDateFrom);
        parameters.put("DATE_TO", strDateTo);

        renderJR(vars, response, strReportName, "xls", parameters, data, null);
      }
    } else {
      advisePopUp(request, response, "WARNING",
          Utility.messageBD(this, "ProcessStatus-W", vars.getLanguage()),
          Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
    }

  }

  private void printPageDataPDF(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strDateFrom, String strDateTo, String strOrg,
      String strLevel, String strcElementValueFrom, String strcElementValueFromDes,
      String strcElementValueTo, String strcElementValueToDes, String strcBpartnerId,
      String strmProductId, String strcProjectId, String strcAcctSchemaId,
      String strNotInitialBalance, String strGroupBy, String strPageNo) throws IOException,
      ServletException {

    response.setContentType("text/html; charset=UTF-8");
    AdvReportTrialBalanceData[] data = null;
    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);
    String strTreeAccount = AdvReportTrialBalanceData.treeAccount(this, vars.getClient());
    boolean strIsSubAccount = false;

    String strAccountFromValue = AdvReportTrialBalanceData.selectAccountValue(this,
        strcElementValueFrom);
    String strAccountToValue = AdvReportTrialBalanceData.selectAccountValue(this, strcElementValueTo);

    log4j.debug("Output: PDF report");
    log4j.debug("strTreeOrg: " + strTreeOrg + "strOrgFamily: " + strOrgFamily + "strTreeAccount: "
        + strTreeAccount);
    log4j.debug("strcBpartnerId: " + strcBpartnerId + "strmProductId: " + strmProductId
        + "strcProjectId: " + strcProjectId);

    if (!strDateFrom.equals("") && !strDateTo.equals("") && !strOrg.equals("")
        && !strcAcctSchemaId.equals("")) {

      if (strLevel.equals("S")) {
        data = AdvReportTrialBalanceData.selectAccountLines(this, strGroupBy, vars.getLanguage(),
            strLevel, strOrgFamily, Utility.getContext(this, vars, "#User_Client",
                "AdvReportTrialBalance"), Utility.getContext(this, vars, "#AccessibleOrgTree",
                "AdvReportTrialBalance"), strAccountFromValue, strAccountToValue, strDateFrom, null,
            strcBpartnerId, strmProductId, strcProjectId, strcAcctSchemaId, (strNotInitialBalance
                .equals("Y") ? "O" : "P"), DateTimeData.nDaysAfter(this, strDateTo, "1"));
        if (!strGroupBy.equals(""))
          strIsSubAccount = true;

      } else {
        data = getDataWhenNotSubAccount(vars, strDateFrom, strDateTo, strOrg, strOrgFamily,
            strcAcctSchemaId, strLevel, strTreeAccount, strNotInitialBalance);
      }

      if (data == null || data.length == 0) {
        advisePopUp(request, response, "WARNING",
            Utility.messageBD(this, "ProcessStatus-W", vars.getLanguage()),
            Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
      } else {

        AcctSchema acctSchema = OBDal.getInstance().get(AcctSchema.class, strcAcctSchemaId);

        String strLanguage = vars.getLanguage();
        String strReportName = "@basedesign@/com/openbravofreelancer/accountingreports/ad_reports/ReportTrialBalancePDF.jrxml";
        HashMap<String, Object> parameters = new HashMap<String, Object>();

        parameters.put("TOTAL", Utility.messageBD(this, "Total", strLanguage));
        StringBuilder strSubTitle = new StringBuilder();

        strSubTitle.append(Utility.messageBD(this, "LegalEntity", vars.getLanguage()) + ": ");
        strSubTitle.append(AdvReportTrialBalanceData.selectCompany(this, vars.getClient()) + " \n");
        strSubTitle.append(Utility.messageBD(this, "asof", vars.getLanguage()) + ": " + strDateTo
            + " \n");

        if (!("0".equals(strOrg)))
          strSubTitle.append(Utility.messageBD(this, "ACCS_AD_ORG_ID_D", vars.getLanguage()) + ": "
              + AdvReportTrialBalanceData.selectOrgName(this, strOrg) + " \n");

        strSubTitle.append(Utility.messageBD(this, "generalLedger", vars.getLanguage()) + ": "
            + acctSchema.getName());

        parameters.put("REPORT_SUBTITLE", strSubTitle.toString());

        parameters.put("DEFAULTVIEW", !strIsSubAccount);
        parameters.put("SUBACCOUNTVIEW", strIsSubAccount);
        parameters.put("DUMMY", true);
        parameters.put("PageNo", strPageNo);
        parameters.put("DATE_FROM", strDateFrom);
        parameters.put("DATE_TO", strDateTo);
        parameters.put("PAGEOF", Utility.messageBD(this, "PageOfNumber", vars.getLanguage()));

        renderJR(vars, response, strReportName, "pdf", parameters, data, null);
      }

    } else {
      advisePopUp(request, response, "WARNING",
          Utility.messageBD(this, "ProcessStatus-W", vars.getLanguage()),
          Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
    }

  }

  private AdvReportTrialBalanceData[] getDataWhenNotSubAccount(VariablesSecureApp vars,
      String strDateFrom, String strDateTo, String strOrg, String strOrgFamily,
      String strcAcctSchemaId, String strLevel, String strTreeAccount, String strNotInitialBalance)
      throws IOException, ServletException {
    AdvReportTrialBalanceData[] data = null;
    AdvReportTrialBalanceData[] dataAux = null;
    dataAux = AdvReportTrialBalanceData.select(this, strDateFrom, strDateTo, strOrg, strTreeAccount,
        strcAcctSchemaId, strNotInitialBalance.equals("Y") ? "O" : "P", strOrgFamily,
        Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"),
        Utility.getContext(this, vars, "#AccessibleOrgTree", "AdvReportTrialBalance"), strDateFrom,
        DateTimeData.nDaysAfter(this, strDateTo, "1"), "", "");
    AdvReportTrialBalanceData[] dataInitialBalance = AdvReportTrialBalanceData.selectInitialBalance(this,
        strDateFrom, strcAcctSchemaId, "", "", "", strOrgFamily,
        Utility.getContext(this, vars, "#User_Client", "AdvReportTrialBalance"),
        strNotInitialBalance.equals("Y") ? "initial" : "notinitial",
        strNotInitialBalance.equals("Y") ? "initial" : "notinitial");

    log4j.debug("Calculating tree...");
    dataAux = calculateTree(dataAux, null, new Vector<Object>(), dataInitialBalance,
        strNotInitialBalance);
    dataAux = levelFilter(dataAux, null, false, strLevel);
    dataAux = dataFilter(dataAux);

    log4j.debug("Tree calculated");

    if (dataAux != null && dataAux.length > 0) {
      data = filterTree(dataAux, strLevel);
      Arrays.sort(data, new AdvReportTrialBalanceDataComparator());
      for (int i = 0; i < data.length; i++) {
        data[i].rownum = "" + i;
      }
    } else {
      data = dataAux;
    }
    return data;

  }

  private AdvReportTrialBalanceData[] filterTree(AdvReportTrialBalanceData[] data, String strLevel) {
    ArrayList<Object> arrayList = new ArrayList<Object>();
    for (int i = 0; data != null && i < data.length; i++) {
      if (data[i].elementlevel.equals(strLevel))
        arrayList.add(data[i]);
    }
    AdvReportTrialBalanceData[] new_data = new AdvReportTrialBalanceData[arrayList.size()];
    arrayList.toArray(new_data);
    return new_data;
  }

  private AdvReportTrialBalanceData[] calculateTree(AdvReportTrialBalanceData[] data, String indice,
      Vector<Object> vecTotal, AdvReportTrialBalanceData[] dataIB, String strNotInitialBalance) {
    if (data == null || data.length == 0)
      return data;
    if (indice == null)
      indice = "0";
    AdvReportTrialBalanceData[] result = null;
    Vector<Object> vec = new Vector<Object>();
    // if (log4j.isDebugEnabled())
    // log4j.debug("AdvReportTrialBalanceData.calculateTree() - data: " +
    // data.length);
    if (vecTotal == null)
      vecTotal = new Vector<Object>();
    if (vecTotal.size() == 0) {
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
    }
    BigDecimal totalDR = new BigDecimal((String) vecTotal.elementAt(0));
    BigDecimal totalCR = new BigDecimal((String) vecTotal.elementAt(1));
    BigDecimal totalInicial = new BigDecimal((String) vecTotal.elementAt(2));
    BigDecimal totalFinal = new BigDecimal((String) vecTotal.elementAt(3));
    boolean encontrado = false;
    for (int i = 0; i < data.length; i++) {
      if (data[i].parentId.equals(indice)) {
        encontrado = true;
        Vector<Object> vecParcial = new Vector<Object>();
        vecParcial.addElement("0");
        vecParcial.addElement("0");
        vecParcial.addElement("0");
        vecParcial.addElement("0");
        AdvReportTrialBalanceData[] dataChilds = calculateTree(data, data[i].id, vecParcial, dataIB,
            strNotInitialBalance);
        BigDecimal parcialDR = new BigDecimal((String) vecParcial.elementAt(0));
        BigDecimal parcialCR = new BigDecimal((String) vecParcial.elementAt(1));
        BigDecimal parcialInicial = new BigDecimal((String) vecParcial.elementAt(2));
        BigDecimal parcialFinal = new BigDecimal((String) vecParcial.elementAt(3));
        data[i].amtacctdr = (new BigDecimal(data[i].amtacctdr).add(parcialDR)).toPlainString();
        data[i].amtacctcr = (new BigDecimal(data[i].amtacctcr).add(parcialCR)).toPlainString();
        data[i].saldoInicial = (new BigDecimal(data[i].saldoInicial).add(parcialInicial))
            .toPlainString();
        // Edit how the final balance is calculated
        data[i].saldoFinal = (new BigDecimal(data[i].saldoInicial).add(parcialDR)
            .subtract(parcialCR)).toPlainString();

        // Set calculated Initial Balances
        for (int k = 0; k < dataIB.length; k++) {
          if (dataIB[k].accountId.equals(data[i].id)) {
            if (strNotInitialBalance.equals("Y")) {
              data[i].saldoInicial = (new BigDecimal(dataIB[k].saldoInicial).add(parcialInicial))
                  .toPlainString();
            } else {
              data[i].amtacctdr = (new BigDecimal(dataIB[k].amtacctdr).add(parcialDR))
                  .toPlainString();
              data[i].amtacctcr = (new BigDecimal(dataIB[k].amtacctcr).add(parcialCR))
                  .toPlainString();
            }
            data[i].saldoFinal = (new BigDecimal(dataIB[k].saldoInicial).add(parcialDR)
                .subtract(parcialCR)).toPlainString();
          }
        }

        totalDR = totalDR.add(new BigDecimal(data[i].amtacctdr));
        totalCR = totalCR.add(new BigDecimal(data[i].amtacctcr));
        totalInicial = totalInicial.add(new BigDecimal(data[i].saldoInicial));
        totalFinal = totalFinal.add(new BigDecimal(data[i].saldoFinal));

        vec.addElement(data[i]);
        if (dataChilds != null && dataChilds.length > 0) {
          for (int j = 0; j < dataChilds.length; j++)
            vec.addElement(dataChilds[j]);
        }
      } else if (encontrado)
        break;
    }
    vecTotal.set(0, totalDR.toPlainString());
    vecTotal.set(1, totalCR.toPlainString());
    vecTotal.set(2, totalInicial.toPlainString());
    vecTotal.set(3, totalFinal.toPlainString());
    result = new AdvReportTrialBalanceData[vec.size()];
    vec.copyInto(result);
    return result;
  }

  /**
   * Filters positions with amount credit, amount debit, initial balance and final balance distinct
   * to zero.
   * 
   * @param data
   * @return AdvReportTrialBalanceData array filtered.
   */
  private AdvReportTrialBalanceData[] dataFilter(AdvReportTrialBalanceData[] data) {
    if (data == null || data.length == 0)
      return data;
    Vector<Object> dataFiltered = new Vector<Object>();
    for (int i = 0; i < data.length; i++) {
      if (new BigDecimal(data[i].amtacctdr).compareTo(BigDecimal.ZERO) != 0
          || new BigDecimal(data[i].amtacctcr).compareTo(BigDecimal.ZERO) != 0
          || new BigDecimal(data[i].saldoInicial).compareTo(BigDecimal.ZERO) != 0
          || new BigDecimal(data[i].saldoFinal).compareTo(BigDecimal.ZERO) != 0) {
        dataFiltered.addElement(data[i]);
      }
    }
    AdvReportTrialBalanceData[] result = new AdvReportTrialBalanceData[dataFiltered.size()];
    dataFiltered.copyInto(result);
    return result;
  }

  private AdvReportTrialBalanceData[] levelFilter(AdvReportTrialBalanceData[] data, String indice,
      boolean found, String strLevel) {
    if (data == null || data.length == 0 || strLevel == null || strLevel.equals(""))
      return data;
    AdvReportTrialBalanceData[] result = null;
    Vector<Object> vec = new Vector<Object>();
    // if (log4j.isDebugEnabled())
    // log4j.debug("AdvReportTrialBalanceData.levelFilter() - data: " +
    // data.length);

    if (indice == null)
      indice = "0";
    for (int i = 0; i < data.length; i++) {
      if (data[i].parentId.equals(indice)
          && (!found || data[i].elementlevel.equalsIgnoreCase(strLevel))) {
        AdvReportTrialBalanceData[] dataChilds = levelFilter(data, data[i].id,
            (found || data[i].elementlevel.equals(strLevel)), strLevel);
        vec.addElement(data[i]);
        if (dataChilds != null && dataChilds.length > 0)
          for (int j = 0; j < dataChilds.length; j++)
            vec.addElement(dataChilds[j]);
      }
    }
    result = new AdvReportTrialBalanceData[vec.size()];
    vec.copyInto(result);
    vec.clear();
    return result;
  }

  private String getFamily(String strTree, String strChild) throws IOException, ServletException {
    return Tree.getMembers(this, strTree, strChild);
  }

  public String getServletInfo() {
    return "Servlet AdvReportTrialBalance. This Servlet was made by Eduardo Argal and mirurita";
  }
}
