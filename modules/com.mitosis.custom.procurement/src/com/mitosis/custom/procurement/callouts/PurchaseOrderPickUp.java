	package com.mitosis.custom.procurement.callouts;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

import org.openbravo.xmlEngine.XmlDocument;

public class PurchaseOrderPickUp extends HttpSecureAppServlet {

	  /**
	 * 
	 */
	private static final long serialVersionUID = -3971357561056595738L;

	public void init (ServletConfig config) {
	    super.init(config);
	    boolHist = false;
	  }

	  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
	    VariablesSecureApp vars = new VariablesSecureApp(request);
	    if (vars.commandIn("DEFAULT")) {
	      String strChanged = vars.getStringParameter("inpLastFieldChanged");
	      if (log4j.isDebugEnabled()) log4j.debug("WE GO INTO THE DEFAULT");
	      if (log4j.isDebugEnabled()) log4j.debug("CHANGED: " + strChanged);
	      String strisRecoger = vars.getStringParameter("inpemPmIsrecoger");
	      String strTabId = vars.getStringParameter("inpTabId");
	      
	      try {
	        printPage(response, vars, strChanged, strisRecoger, strTabId);
	      } catch (ServletException ex) {
	        pageErrorCallOut(response);
	      }
	    } else pageError(response);
	  }

	  void printPage(HttpServletResponse response, VariablesSecureApp vars, String strChanged, String strisRecoger, String strTabId) throws IOException, ServletException {
	    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");
	    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_callouts/CallOut").createXmlDocument();
	    StringBuffer resultado = new StringBuffer();
	    resultado.append("var calloutName='PurchaseOrderPickUp';\n\n");
	    resultado.append("var respuesta = new Array(");
	    if (strisRecoger.equals("Y")){
	       resultado.append("new Array(\"inpdeliverynotes\", \"COMATEL RETIRA EL MATERIAL, POR FAVOR TENER LISTO Y FACTURADO; ENVIAR FACTURA POR FAX --REQ--\")");
	    } else{
	    	resultado.append("new Array(\"inpdeliverynotes\", \"POR FAVOR ENVIAR LO ANTES POSIBLE --REQ--\")");
	    }
	    	
	    resultado.append(");");
	    
	    xmlDocument.setParameter("array", resultado.toString());
	    xmlDocument.setParameter("frameName", "frameAplicacion");
	    response.setContentType("text/html; charset=UTF-8");
	    PrintWriter out = response.getWriter();
	    out.println(xmlDocument.print());
	    out.close();

	  }

}