package com.mitosis.custom.procurement.utility;

import org.openbravo.dal.core.OBContext;

public class ProcurementUtility {
  public static String getCurrentUser() {
    String name = null;
    name = OBContext.getOBContext().getUser().getName();
    return name;
  }
}
