package com.mitosis.custom.procurement.process;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.procurement.Requisition;
import org.openbravo.model.procurement.RequisitionLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.CallProcess;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

import com.mitosis.custom.SalesManagement.restrictions.RestrictionType;
import com.mitosis.custom.SalesManagement.restrictions.RestrictionUtils;

/**
 * Requisition completion with validation(User based restrictions).
 * 
 * @author Anbukkani G
 * 
 */
public class CompleteRequisition extends DalBaseProcess {
  Logger log = Logger.getLogger(CompleteRequisition.class);

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {

    String requisistionId = (String) bundle.getParams().get("M_Requisition_ID");
    Requisition requisition = OBDal.getInstance().get(Requisition.class, requisistionId);

    // MSLV based restrictions
    Long maxPurchasePrice = requisition.getOrganization().getPmMaxPurchasePrice();
    Long smlv = requisition.getOrganization().getPmSmlv();
    List<RequisitionLine> requisitionlines = requisition.getProcurementRequisitionLineList();
    BigDecimal totalPrice = new BigDecimal(0);
    for (RequisitionLine requisitionline : requisitionlines) {
      // TODO: Need to check(ask to andres) if price is null. what we will do?
      totalPrice = totalPrice.add((requisitionline.getUnitPrice() != null ? requisitionline
          .getUnitPrice() : new BigDecimal(0)).multiply(requisitionline.getQuantity()));
    }
    BigDecimal allowedPrice = new BigDecimal(maxPurchasePrice != null ? maxPurchasePrice : 0)
        .multiply(new BigDecimal(smlv != null ? smlv : 0));
    if (allowedPrice.compareTo(totalPrice) < 0
        && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
            RestrictionType.MSLV_BASED_RESTRICTION)) {
      throw new OBException("Requisition total amount existed for organization's SMLV");
    }
    // Minimum sales margin restriction
    List<RequisitionLine> requisitionLineList = requisition.getProcurementRequisitionLineList();
    for (RequisitionLine requisitionLine : requisitionLineList) {
      OrderLine orderline = requisitionLine.getPmOrderline();
      if (orderline != null) {
        BigDecimal priceList = requisitionLine.getUnitPrice() != null ? requisitionLine
            .getUnitPrice() : new BigDecimal(0);
        BigDecimal priceActual = orderline.getUnitPrice();
        BigDecimal salesProfit = new BigDecimal(0);
        if (priceActual != new BigDecimal(0)) {
          salesProfit = ((new BigDecimal(1).subtract((priceList).divide(priceActual, 4, 5)))
              .multiply(new BigDecimal(100)));
        }
        String minSalesProfit = ""
            + (requisitionLine.getOrganization().getOrganizationInformationList().get(0)
                .getSmMinSalesProfit() != null ? requisitionLine.getOrganization()
                .getOrganizationInformationList().get(0).getSmMinSalesProfit() : 0);
        if (new BigDecimal(minSalesProfit).compareTo(salesProfit) > 0
            && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
                RestrictionType.PO_MINI_SALES_PRICE_RESTRICTION)) {
          throw new OBException("Requisition lines " + requisitionLine.getLineNo()
              + " should have minimum organization sales profit.");

        }
      }
    }

    // Need to add restriction in requisition completion, if have any pending shipments(without
    // deliver) for this particular product.

    List<RequisitionLine> listOfRequisitionLines = requisition.getProcurementRequisitionLineList();
    boolean hasPendingShipment = false;

    String productName = "";
    for (RequisitionLine requisitionline : listOfRequisitionLines) {
      Product product = requisitionline.getProduct();
      OBCriteria<OrderLine> orderCriteria = OBDal.getInstance().createCriteria(OrderLine.class);
      orderCriteria.add(Restrictions.eq(OrderLine.PROPERTY_PRODUCT, product));
      List<OrderLine> orderLineList = orderCriteria.list();
      for (OrderLine orderLine : orderLineList) {
        if (!orderLine.getSalesOrder().getDocumentStatus().equalsIgnoreCase("CO"))
          continue;
        List<ShipmentInOutLine> shipmentLines = orderLine.getMaterialMgmtShipmentInOutLineList();
        for (ShipmentInOutLine shipmentLine : shipmentLines) {
          // ShipmentInOutLine shipmentLine =
          // orderLine.getMaterialMgmtShipmentInOutLineList().get(0);
          if (shipmentLine != null) {

            if (!shipmentLine.getShipmentReceipt().getDocumentStatus().equalsIgnoreCase("CO")
                && !shipmentLine.getShipmentReceipt().getDocumentStatus().equalsIgnoreCase("VO")) {
              hasPendingShipment = true;
              productName = product.getName();
              break;
            }
          } else {
            hasPendingShipment = true;
            productName = product.getName();
            break;
          }
        }
        if (hasPendingShipment)
          break;
      }
    }
    if (hasPendingShipment
        && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
            RestrictionType.PENDING_SHIPMENT_PO_RESTRICTION)) {
      throw new OBException("The requisition is not completed. Because the line product "
          + productName + " have pending shipment");
    }

    // Need to add restriction in requisition based on sales order restriction
    String soRestriction = requisition.getPmOrder() != null ? requisition.getPmOrder()
        .getSmRestrictions() : "";

    if (soRestriction != null
        && soRestriction.length() > 0
        && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
            RestrictionType.PO_SALESORDER_RESTRICTION)) {
      throw new OBException("The restriction in requisition based on sales order");
    }
    OBContext.getOBContext().setAdminMode();
    // Posting
    CallProcess callProcess = new CallProcess();
    ProcessInstance instance = callProcess.call("M_Requisition_Post", requisistionId, null);
    if (instance.getResult() == 1) {
      String successMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
          "SM_SuccessfullyBook", OBContext.getOBContext().getLanguage().getLanguage()));
      OBError successMessage = new OBError();
      successMessage.setTitle("Success");
      successMessage.setType("Success");
      successMessage.setMessage(successMsg);
      bundle.setResult(successMessage);
    } else {
      log.error("Error while calling the function c_order_post.");
      String processCallErrorMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
          instance.getErrorMsg().split("@")[2], OBContext.getOBContext().getLanguage()
              .getLanguage()));
      throw new OBException(processCallErrorMsg);
    }
    OBContext.getOBContext().restorePreviousMode();
    log.info("Sales Order Successfully Booked");

  }

}
