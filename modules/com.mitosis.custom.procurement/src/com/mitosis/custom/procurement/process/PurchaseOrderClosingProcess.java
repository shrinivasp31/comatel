package com.mitosis.custom.procurement.process;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.order.RejectReason;
import org.openbravo.model.procurement.Requisition;
import org.openbravo.model.procurement.RequisitionLine;
import org.openbravo.model.procurement.RequisitionPOMatch;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

import com.mitosis.custom.SalesManagement.process.QuotationCompletion;

public class PurchaseOrderClosingProcess extends DalBaseProcess {
  Logger log = Logger.getLogger(PurchaseOrderClosingProcess.class);

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
    ProcessLogger processLogger = bundle.getLogger();
    String errorMsg = "";
    // TODO Auto-generated method stub
    OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
    orderCriteria.add(Restrictions.and(Restrictions.eq(Order.PROPERTY_DOCUMENTSTATUS, "CO"),
        Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, false)));

    List<Order> orderList = orderCriteria.list();
    for (Order order : orderList) {
      try {
        if (order.getMaterialMgmtShipmentInOutList().size() == 0) {
          Long closingDays = order.getOrganization().getPmOrderClosingDays() != null ? order
              .getOrganization().getPmOrderClosingDays() : 0;
          SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
          Date orderDate = order.getOrderDate();
          Calendar c = Calendar.getInstance();
          c.setTime(orderDate);
          c.add(Calendar.DATE, closingDays.intValue());
          orderDate = c.getTime();
          String strOrderDate1 = format.format(orderDate);
          Date date = new Date();
          String strToday = format.format(date);
          if (format.parse(strOrderDate1).compareTo(format.parse(strToday)) < 0) {
            RejectReason rejectReason = checkRejectReason();
            order.setDocumentAction("CL");
            order.setPMRejectReason(rejectReason);
            OBDal.getInstance().save(order);
            OBDal.getInstance().flush();
            List<OrderLine> orderLines = order.getOrderLineList();
            for (OrderLine orderLine : orderLines) {
              List<RequisitionPOMatch> requisitionPOMatchs = orderLine
                  .getProcurementRequisitionPOMatchList();
              for (RequisitionPOMatch requisitionPOMatch : requisitionPOMatchs) {
                RequisitionLine requisitionLine = requisitionPOMatch.getRequisitionLine();
                Requisition requisition = requisitionLine.getRequisition();
                requisition.setDocumentStatus("CO");
                requisition.setDocumentAction("--");
                OBDal.getInstance().save(requisition);
                OBDal.getInstance().flush();
                requisitionLine.setRequisitionLineStatus("O");
                OBDal.getInstance().save(requisitionLine);
                OBDal.getInstance().flush();
                OBDal.getInstance().remove(requisitionPOMatch);
                OBDal.getInstance().flush();
              }
            }

            // QuotationCompletion quotationCompletion = new QuotationCompletion();
            ProcessInstance instance = QuotationCompletion.procedureCall(order);
            if (instance.getResult() != 1) {
              SessionHandler.getInstance().commitAndStart();
              log.error("Error while calling the function c_order_post.");
              String processCallErrorMsg = String.format(Utility.messageBD(
                  new DalConnectionProvider(), instance.getErrorMsg(), OBContext.getOBContext()
                      .getLanguage().getLanguage()));
              throw new OBException("Exception Occured while closing order : "
                  + order.getDocumentNo() + " ERROR: " + processCallErrorMsg.substring(7));
            }
            log.info("Purchase Order Successfully Closed");
          }
        }
      } catch (Exception ex) {
        if (errorMsg.length() > 0)
          errorMsg = errorMsg + "," + ex;
        else
          errorMsg = ex + "";
      }
    }
    if (errorMsg.length() > 0) {
      processLogger.log(errorMsg);
      throw new OBException(errorMsg);
    }
  }

  public static RejectReason checkRejectReason() {
    RejectReason rejectReason = null;
    // Organization orderOrganization = order.getOrganization();
    OBCriteria<RejectReason> rejectReasonCriteria = OBDal.getInstance().createCriteria(
        RejectReason.class);
    // rejectReasonCriteria.add(Restrictions.and(Restrictions.eq(RejectReason.PROPERTY_ORGANIZATION,
    // orderOrganization),Restrictions.eq(RejectReason.PROPERTY_SEARCHKEY,
    // "PM_ExpireDateBasedOnOrgParam")));
    rejectReasonCriteria.add(Restrictions.eq(RejectReason.PROPERTY_SEARCHKEY,
        "PM_ExpireDateBasedOnOrgParam"));
    rejectReason = rejectReasonCriteria.list().size() > 0 ? rejectReasonCriteria.list().get(0)
        : null;
    if (rejectReason == null) {
      rejectReason = OBProvider.getInstance().get(RejectReason.class);
      rejectReason.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
      rejectReason.setSearchKey("PM_ExpireDateBasedOnOrgParam");
      rejectReason.setName("Order Date Expired");
      rejectReason.setDescription("Order date expried.");
      OBDal.getInstance().save(rejectReason);
      OBDal.getInstance().flush();
    }

    return rejectReason;
  }
}
