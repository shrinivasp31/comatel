package com.mitosis.custom.procurement.process;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.procurement.RequisitionPOMatch;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.CallProcess;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

import com.mitosis.custom.SalesManagement.restrictions.RestrictionType;
import com.mitosis.custom.SalesManagement.restrictions.RestrictionUtils;

public class PurchaseOrderBooking extends DalBaseProcess {
  Logger log = Logger.getLogger(CompleteRequisition.class);

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {

    String purchaseOrderId = (String) bundle.getParams().get("C_Order_ID");
    Order purchaseOrder = OBDal.getInstance().get(Order.class, purchaseOrderId);

    // MSLV based restrictions
    Long maxPurchasePrice = purchaseOrder.getOrganization().getPmMaxPurchasePrice();
    Long smlv = purchaseOrder.getOrganization().getPmSmlv();
    List<OrderLine> purchaseOrderLines = purchaseOrder.getOrderLineList();
    BigDecimal totalPrice = new BigDecimal(0);
    for (OrderLine purchaseOrderLine : purchaseOrderLines) {
      // TODO: Need to check(ask to andres) if price is null. what we will do?
      totalPrice = totalPrice.add((purchaseOrderLine.getUnitPrice() != null ? purchaseOrderLine
          .getUnitPrice() : new BigDecimal(0)).multiply(purchaseOrderLine.getOrderedQuantity()));
    }
    BigDecimal allowedPrice = new BigDecimal(maxPurchasePrice != null ? maxPurchasePrice : 0)
        .multiply(new BigDecimal(smlv != null ? smlv : 0));
    if (allowedPrice.compareTo(totalPrice) < 0
        && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
            RestrictionType.MSLV_BASED_RESTRICTION)) {
      throw new OBException("Purchase Order total amount existed for organization's SMLV");
    }

    // Minimum sales margin restriction
    for (OrderLine purchaseOrderLine : purchaseOrderLines) {
      OBCriteria<RequisitionPOMatch> requisitionPOMatchCriteria = OBDal.getInstance()
          .createCriteria(RequisitionPOMatch.class);
      requisitionPOMatchCriteria.add(Restrictions.eq(RequisitionPOMatch.PROPERTY_SALESORDERLINE,
          purchaseOrderLine));
      List<RequisitionPOMatch> requisitionPOMatchList = requisitionPOMatchCriteria.list();

      if (requisitionPOMatchList.size() > 0)

      {
        RequisitionPOMatch requisitionPOMatchs = requisitionPOMatchList.get(0);
        // OrderLine orderline = requisitionLine.getPmOrderline();
        OrderLine orderline = requisitionPOMatchs.getRequisitionLine().getPmOrderline();

        if (orderline != null) {
          BigDecimal priceList = requisitionPOMatchs.getRequisitionLine().getUnitPrice() != null ? requisitionPOMatchs
              .getRequisitionLine().getUnitPrice() : new BigDecimal(0);
          BigDecimal priceActual = orderline.getUnitPrice();
          BigDecimal salesProfit = new BigDecimal(0);
          if (priceActual != new BigDecimal(0)) {
            salesProfit = ((new BigDecimal(1).subtract((priceList).divide(priceActual, 4, 5)))
                .multiply(new BigDecimal(100)));
          }
          String minSalesProfit = ""
              + (requisitionPOMatchs.getRequisitionLine().getOrganization()
                  .getOrganizationInformationList().get(0).getSmMinSalesProfit() != null ? requisitionPOMatchs
                  .getRequisitionLine().getOrganization().getOrganizationInformationList().get(0)
                  .getSmMinSalesProfit()
                  : 0);
          if (new BigDecimal(minSalesProfit).compareTo(salesProfit) > 0
              && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
                  RestrictionType.PO_MINI_SALES_PRICE_RESTRICTION)) {
            throw new OBException("Purchase Order lines "
                + requisitionPOMatchs.getRequisitionLine().getLineNo()
                + " should have minimum organization sales profit.");

          }
        }

      }
    }
    // Need to add restriction in requisition completion, if have any pending shipments(without
    // deliver) for this particular product.

    // List<RequisitionLine> listOfRequisitionLines =
    // requisition.getProcurementRequisitionLineList();
    boolean hasPendingShipment = false;

    String productName = "";
    for (OrderLine purchaseOrderLine : purchaseOrderLines) {
      Product product = purchaseOrderLine.getProduct();
      OBCriteria<OrderLine> orderCriteria = OBDal.getInstance().createCriteria(OrderLine.class);
      orderCriteria.add(Restrictions.eq(OrderLine.PROPERTY_PRODUCT, product));
      List<OrderLine> orderLineList = orderCriteria.list();
      for (OrderLine orderLine : orderLineList) {
        if (!orderLine.getSalesOrder().getDocumentStatus().equalsIgnoreCase("CO"))
          continue;
        List<ShipmentInOutLine> shipmentLines = orderLine.getMaterialMgmtShipmentInOutLineList();
        for (ShipmentInOutLine shipmentLine : shipmentLines) {
          // ShipmentInOutLine shipmentLine =
          // orderLine.getMaterialMgmtShipmentInOutLineList().get(0);
          if (shipmentLine != null) {

            if (!shipmentLine.getShipmentReceipt().getDocumentStatus().equalsIgnoreCase("CO")
                && !shipmentLine.getShipmentReceipt().getDocumentStatus().equalsIgnoreCase("VO")) {
              hasPendingShipment = true;
              productName = product.getName();
              break;
            }
          } else {
            hasPendingShipment = true;
            productName = product.getName();
            break;
          }
        }
        if (hasPendingShipment)
          break;
      }
    }

    if (hasPendingShipment
        && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
            RestrictionType.PENDING_SHIPMENT_PO_RESTRICTION)) {
      throw new OBException("The Purchase Order is not completed. Because the line product "
          + productName + " have pending shipment");
    }

    // Need to add restriction in requisition based on sales order restriction
    for (OrderLine purchaseOrderLine : purchaseOrderLines) {
      OBCriteria<RequisitionPOMatch> requisitionPOMatchCriteria = OBDal.getInstance()
          .createCriteria(RequisitionPOMatch.class);
      requisitionPOMatchCriteria.add(Restrictions.eq(RequisitionPOMatch.PROPERTY_SALESORDERLINE,
          purchaseOrderLine));
      List<RequisitionPOMatch> requisitionPOMatchList = requisitionPOMatchCriteria.list();

      if (requisitionPOMatchList.size() > 0) {
        RequisitionPOMatch requisitionPOMatchs = requisitionPOMatchList.get(0);
        String soRestriction = requisitionPOMatchs.getRequisitionLine().getRequisition()
            .getPmOrder() != null ? requisitionPOMatchs.getRequisitionLine().getRequisition()
            .getPmOrder().getSmRestrictions() : "";

        if (soRestriction != null
            && soRestriction.length() > 0
            && RestrictionUtils.checkRestrictionForUser(OBContext.getOBContext().getUser(),
                RestrictionType.PO_SALESORDER_RESTRICTION)) {
          throw new OBException("The restriction in purchase order based on sales order");
        }
      }
    }

    OBContext.getOBContext().setAdminMode();

    // updating order date.
    purchaseOrder.setOrderDate(new Date());
    OBDal.getInstance().save(purchaseOrder);
    OBDal.getInstance().flush();

    // Posting
    CallProcess callProcess = new CallProcess();
    ProcessInstance instance = callProcess.call("C_Order_Post", purchaseOrderId, null);
    if (instance.getResult() == 1) {
      // try {
      // purchaseOrder.setOrderDate(new Date());
      // OBDal.getInstance().save(purchaseOrder);
      // OBDal.getInstance().flush();
      // } catch (Exception e) {
      // System.err.println(e.getMessage());
      // throw e;
      // // throw new OBException(e.getMessage());
      // }
      String successMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
          "SM_SuccessfullyBook", OBContext.getOBContext().getLanguage().getLanguage()));
      OBError successMessage = new OBError();
      successMessage.setTitle("Success");
      successMessage.setType("Success");
      successMessage.setMessage(successMsg);
      bundle.setResult(successMessage);
    } else {
      log.error("Error while calling the function c_order_post.");
      String processCallErrorMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
          instance.getErrorMsg().split("@")[2], OBContext.getOBContext().getLanguage()
              .getLanguage()));
      throw new OBException(processCallErrorMsg);
    }
    OBContext.getOBContext().restorePreviousMode();
    log.info("Purchase Order Successfully Booked");

  }

}
