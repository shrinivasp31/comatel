package com.tesacol.comatel.customisation.ad_process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * @author shrinivas
 *
 */
public class PurchaseOrderVoid extends DalBaseProcess {

	@Override
	public void doExecute(ProcessBundle bundle) {
		OBError message = new OBError();
		try {
			String orderId = (String) bundle.getParams().get("C_Order_ID");
			Order order = OBDal.getInstance().get(Order.class, orderId);
			if (order.isCmcIsvoided() != null && order.isCmcIsvoided()) {
				message.setTitle("Void Goods Receipt");
				message.setType("Warning");
				message.setMessage("Cannot void this Purchase Order , as it is already Voided.");
				return;
			}
			if (!order.getDocumentStatus().equals("DR")) {
				message.setTitle("Void Purchase Order");
				message.setType("Warning");
				message.setMessage("Cannot void Purchase Order with this process.");
				return;
			}
			Order po = processRequest(order);
			order.setCmcIsvoided(true);
			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
			message.setTitle("Void Purchase Order");
			message.setType("Success");
			message.setMessage("Process Completed Successfully. Created Void Purchase Order with Document No ["
					+ po.getDocumentNo() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			message.setTitle("Void Purchase Order");
			message.setType("error");
			message.setMessage("Error while processing request " + e.getMessage());
		} finally {
			bundle.setResult(message);
		}
	}

	/**
	 * @param order
	 * @return
	 */
	private Order processRequest(Order order) {
		Order purchaseOrder = createPurchaseOrderHeader(order);
		for (OrderLine orderLine : order.getOrderLineList()) {
			createPurchaseOrderLine(orderLine, purchaseOrder);
		}
		return purchaseOrder;
	}

	/**
	 * @param order
	 * @return
	 */
	private Order createPurchaseOrderHeader(Order order) {
		Order purchaseOrder = OBProvider.getInstance().get(Order.class);
		purchaseOrder.setOrganization(order.getOrganization());
		purchaseOrder.setClient(order.getClient());
		purchaseOrder.setSalesTransaction(false);
		purchaseOrder.setBusinessPartner(order.getBusinessPartner());
		purchaseOrder.setPartnerAddress(order.getPartnerAddress());
		purchaseOrder.setDropShipLocation(order.getDropShipLocation());
		purchaseOrder.setSmStdBpAddress(order.getSmStdBpAddress());
		purchaseOrder.setInvoiceAddress(order.getInvoiceAddress());
		purchaseOrder.setTransactionDocument(order.getTransactionDocument());
		purchaseOrder.setDocumentType(order.getDocumentType());
		purchaseOrder.setDocumentNo("Void-PO-" + order.getDocumentNo());
		purchaseOrder.setOrderDate(new Date());
		purchaseOrder.setAccountingDate(new Date());
		purchaseOrder.setPaymentMethod(order.getPaymentMethod());
		purchaseOrder.setPaymentTerms(order.getPaymentTerms());
		purchaseOrder.setScheduledDeliveryDate(order.getScheduledDeliveryDate());
		purchaseOrder.setPriceList(order.getPriceList());
		purchaseOrder.setDeliveryNotes(order.getDeliveryNotes());
		purchaseOrder.setCmcIsvoided(true);
		purchaseOrder.setCurrency(order.getCurrency());
		purchaseOrder.setWarehouse(order.getWarehouse());
		OBDal.getInstance().save(purchaseOrder);
		return purchaseOrder;
	}

	/**
	 * @param orderLine
	 * @param purchaseOrder
	 */
	private void createPurchaseOrderLine(OrderLine orderLine, Order purchaseOrder) {
		OrderLine poLine = OBProvider.getInstance().get(OrderLine.class);
		poLine.setLineNo(orderLine.getLineNo());
		poLine.setOrganization(orderLine.getOrganization());
		poLine.setClient(orderLine.getClient());
		poLine.setProduct(orderLine.getProduct());
		poLine.setOrderedQuantity(orderLine.getOrderedQuantity());
		poLine.setUnitPrice(orderLine.getUnitPrice().multiply(new BigDecimal(-1)));
		poLine.setUOM(orderLine.getUOM());
		poLine.setLineGrossAmount(orderLine.getLineGrossAmount().multiply(new BigDecimal(-1)));
		poLine.setLineNetAmount(orderLine.getLineNetAmount().multiply(new BigDecimal(-1)));
		poLine.setGrossUnitPrice(orderLine.getGrossUnitPrice().multiply(new BigDecimal(-1)));
		poLine.setPriceLimit(orderLine.getPriceLimit().multiply(new BigDecimal(-1)));
		poLine.setTax(orderLine.getTax());
		poLine.setListPrice(orderLine.getListPrice());
		poLine.setDiscount(orderLine.getDiscount());
		poLine.setDescription(orderLine.getDescription());
		poLine.setSMMUTotal(orderLine.getSMMUTotal());
		poLine.setPmMuPvVsOs(orderLine.getPmMuPvVsOs());
		poLine.setPmPricesalesorder(orderLine.getPmPricesalesorder());
		poLine.setSmInventory(orderLine.getSmInventory());
		poLine.setSmLastPurchaseInvAmt(orderLine.getSmLastPurchaseInvAmt());
		poLine.setSmLastPurchaseInvDate(orderLine.getSmLastPurchaseInvDate());
		poLine.setWarehouse(orderLine.getWarehouse());
		poLine.setOrderDate(new Date());
		poLine.setCurrency(orderLine.getCurrency());
		poLine.setSalesOrder(purchaseOrder);
		List<OrderLine> orderLineList = null;
		if (purchaseOrder.getOrderLineList() == null) {
			orderLineList = new ArrayList<OrderLine>();
		} else {
			orderLineList = purchaseOrder.getOrderLineList();
		}
		orderLineList.add(poLine);
		purchaseOrder.setOrderLineList(orderLineList);
		OBDal.getInstance().save(poLine);
	}
}
