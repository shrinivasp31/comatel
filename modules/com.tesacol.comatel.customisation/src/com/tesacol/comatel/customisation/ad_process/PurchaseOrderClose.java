package com.tesacol.comatel.customisation.ad_process;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.order.RejectReason;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.CallProcess;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DalConnectionProvider;

/**
 * @author shrinivas <br>
 *         Background that closes purchase order those are completely received
 */
public class PurchaseOrderClose extends DalBaseProcess {
	Logger logger = Logger.getLogger(PurchaseOrderClose.class);

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {
		ProcessLogger processLogger = bundle.getLogger();

		try {
			OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
			orderCriteria.add(Restrictions.eq(Order.PROPERTY_DOCUMENTSTATUS, "CO"));
			orderCriteria.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, false));
			List<Order> orderCriteriaList = orderCriteria.list();
			if (orderCriteriaList.isEmpty()) {
				logger.info("Skipping the process as there are no records to c.");
				return;
			}
			closeCompletedPurchaseOrders(orderCriteriaList);
		} catch (Exception e) {
			processLogger.log("Error while closing Purchase Order:" + e.getMessage());
			throw new OBException(e.getMessage());
		}
	}

	/**
	 * @param inoutCriteriaList
	 */
	private void closeCompletedPurchaseOrders(List<Order> orderCriteriaList) {
		logger.info("closing Purchase Order");
		RejectReason rejectReason = getRejectReason("Auto Close Purchase Order");
		for (Order order : orderCriteriaList) {
			if (order.getRejectReason() == null) {
				order.setRejectReason(rejectReason);
				order.setPMRejectReason(rejectReason);
			}
			order.setDocumentAction("CL");
			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
			if (isEligibleforClosing(order)) {
				logger.info(
						"Purchase Order with document no [ " + order.getDocumentNo() + " ] is eligible for closing.");
				ProcessInstance instance = callProcedure(order);
				if (instance.getResult() != 1) {
					SessionHandler.getInstance().commitAndStart();
					logger.error("Error while calling the function c_order_post.");
					String processCallErrorMsg = String.format(Utility.messageBD(new DalConnectionProvider(),
							instance.getErrorMsg(), OBContext.getOBContext().getLanguage().getLanguage()));
					throw new OBException("Exception Occured while closing order : " + order.getDocumentNo()
							+ " ERROR: " + processCallErrorMsg.substring(7));
				}
				logger.info("Purchase Order [ " + order.getDocumentNo() + " ] Successfully Closed");
			}
		}
	}

	private RejectReason getRejectReason(String rejectReasonName) {
		RejectReason rejectReason = null;
		OBCriteria<RejectReason> rejectReasonCriteria = OBDal.getInstance().createCriteria(RejectReason.class);
		rejectReasonCriteria.add(Restrictions.eq(RejectReason.PROPERTY_NAME, rejectReasonName));
		List<RejectReason> rejectReasonCriteriaList = rejectReasonCriteria.list();
		if (rejectReasonCriteriaList.isEmpty()) {
			rejectReason = OBProvider.getInstance().get(RejectReason.class);
			rejectReason.setSearchKey(rejectReasonName);
			rejectReason.setName(rejectReasonName);
			rejectReason.setDescription(
					"This is default reject reason we are setting for the purchase order while closing automatically");
			OBDal.getInstance().save(rejectReason);
			OBDal.getInstance().flush();
		} else {
			rejectReason = rejectReasonCriteriaList.get(0);
		}
		return rejectReason;
	}

	/**
	 * @param inout
	 * @return <br>
	 *         This method calls stored procedure to close purchase order.
	 */
	public ProcessInstance callProcedure(Order order) {
		logger.info("Closing order [ " + order.getDocumentNo() + " ]");
		String orderID = order.getId();
		CallProcess callProcess = new CallProcess();
		ProcessInstance instance = callProcess.call("C_Order_Post", orderID, null);
		return instance;
	}

	/**
	 * @param order
	 * @return This method checks whether given purchase order is eligible for
	 *         closing or not based on the goods receipt associated by this. If
	 *         the goods receipt line qty associated by this exceeds given
	 *         purchase order qty.
	 */
	private boolean isEligibleforClosing(Order order) {
		boolean isEligible = false;
		BigDecimal orderedQty = new BigDecimal(0);
		for (OrderLine ol : order.getOrderLineList()) {
			orderedQty = orderedQty.add(ol.getOrderedQuantity());
		}
		BigDecimal movementQty = new BigDecimal(0);
		OBCriteria<ShipmentInOut> goodsReceiptCriteria = OBDal.getInstance().createCriteria(ShipmentInOut.class);
		goodsReceiptCriteria.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESORDER, order));
		goodsReceiptCriteria.add(Restrictions.eq(ShipmentInOut.PROPERTY_DOCUMENTSTATUS, "CO"));
		List<ShipmentInOut> goodsReceiptCriteriaList = goodsReceiptCriteria.list();
		if (!goodsReceiptCriteriaList.isEmpty()) {
			for (ShipmentInOut receipt : goodsReceiptCriteriaList)
				for (ShipmentInOutLine receiptLine : receipt.getMaterialMgmtShipmentInOutLineList()) {
					movementQty = movementQty.add(receiptLine.getMovementQuantity());
				}
		}
		if (movementQty.compareTo(orderedQty) == 1 || movementQty.compareTo(orderedQty) == 0) {
			isEligible = true;
		}
		return isEligible;
	}

}
