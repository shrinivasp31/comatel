package com.tesacol.comatel.customisation.ad_process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

public class GoodsReceiptVoid extends DalBaseProcess {

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {

		OBError message = new OBError();
		try{
		String inoutId = (String) bundle.getParams().get("M_InOut_ID");
		ShipmentInOut inout = OBDal.getInstance().get(ShipmentInOut.class, inoutId);
			if (inout.isCmcIsVoid() != null && inout.isCmcIsVoid()) {
				message.setTitle("Void Goods Receipt");
				message.setType("Warning");
				message.setMessage("Cannot void this Goods Receipt , as it is already Voided.");
				return;
			}
			if (!inout.getDocumentStatus().equals("DR")) {
				message.setTitle("Void Goods Receipt");
				message.setType("Warning");
				message.setMessage("Cannot void goods receipt with this process.");
				return;
			}
			ShipmentInOut receipt = processRequest(inout);
			inout.setCmcIsVoid(true);
			OBDal.getInstance().save(inout);
			OBDal.getInstance().flush();
			message.setTitle("Void Goods Receipt");
			message.setType("Success");
			message.setMessage("Process Completed Successfully. Created Void Goods Receipt with Document No ["
					+ receipt.getDocumentNo() + "]");
		}catch(Exception e){
			e.printStackTrace();
			message.setTitle("Void Goods Receipt");
			message.setType("error");
			message.setMessage("Error while processing request " + e.getMessage());
		}finally{
			bundle.setResult(message);
		}
	}

	/**
	 * @param inout
	 */
	private ShipmentInOut processRequest(ShipmentInOut inout) {
		ShipmentInOut receipt = createInoutHeader(inout);
		for (ShipmentInOutLine inoutLine : inout.getMaterialMgmtShipmentInOutLineList()) {
			createInoutLines(receipt, inoutLine);
		}
		return receipt;
	}

	/**
	 * @param receipt
	 * @param inoutLine
	 * @return
	 */
	private ShipmentInOutLine createInoutLines(ShipmentInOut goodsReceipt, ShipmentInOutLine inoutLine) {
		ShipmentInOutLine receiptLine = OBProvider.getInstance().get(ShipmentInOutLine.class);
		receiptLine.setProduct(inoutLine.getProduct());
		receiptLine.setOrganization(inoutLine.getOrganization());
		receiptLine.setClient(inoutLine.getClient());
		receiptLine.setLineNo(inoutLine.getLineNo());
		receiptLine.setMovementQuantity(inoutLine.getMovementQuantity().multiply(new BigDecimal(-1)));
		receiptLine.setSmProductValue(inoutLine.getSmProductValue());
		receiptLine.setShipmentReceipt(goodsReceipt);
		receiptLine.setStorageBin(inoutLine.getStorageBin());
		receiptLine.setAttributeSetValue(inoutLine.getAttributeSetValue());
		receiptLine.setUOM(inoutLine.getUOM());
		receiptLine.setDescription(inoutLine.getDescription());
		receiptLine.setSmPriceactual(inoutLine.getSmPriceactual());
		receiptLine.setSMMUTotal(inoutLine.getSMMUTotal());
		receiptLine.setSmLastPurchaseInvAmt(inoutLine.getSmLastPurchaseInvAmt());
		receiptLine.setSmLastPurchaseInvDate(inoutLine.getSmLastPurchaseInvDate());
		receiptLine.setSmLastSalesInvAmt(inoutLine.getSmLastSalesInvAmt());
		receiptLine.setSmLastSalesInvDate(inoutLine.getSmLastSalesInvDate());
		receiptLine.setSmQtyOnHand(inoutLine.getSmQtyOnHand());
		List<ShipmentInOutLine> receiptLineList = null;
		if (goodsReceipt.getMaterialMgmtShipmentInOutLineList() == null) {
			receiptLineList = new ArrayList<ShipmentInOutLine>();
		} else {
			receiptLineList = goodsReceipt.getMaterialMgmtShipmentInOutLineList();
		}
		OBDal.getInstance().save(receiptLine);
		receiptLineList.add(receiptLine);
		goodsReceipt.setMaterialMgmtShipmentInOutLineList(receiptLineList);
		OBDal.getInstance().save(receiptLine);
		return receiptLine;
	}

	/**
	 * @param inout
	 * @return
	 */
	private ShipmentInOut createInoutHeader(ShipmentInOut inout) {
		ShipmentInOut receipt = OBProvider.getInstance().get(ShipmentInOut.class);
		receipt.setBusinessPartner(inout.getBusinessPartner());
		receipt.setPartnerAddress(inout.getPartnerAddress());
		receipt.setWarehouse(inout.getWarehouse());
		receipt.setOrganization(inout.getOrganization());
		receipt.setClient(inout.getClient());
		receipt.setDocumentStatus("DR");
		receipt.setMovementType(inout.getMovementType());
		receipt.setDocumentType(inout.getDocumentType());
		receipt.setSalesTransaction(false);
		receipt.setDocumentNo("Void-GR-" + inout.getDocumentNo());
		receipt.setDocumentAction(inout.getDocumentAction());
		receipt.setDescription("Voided Goods Receipt for Goods Receipt [ " + inout.getDocumentNo() + " ]");
		receipt.setMovementDate(new Date());
		receipt.setAccountingDate(new Date());
		receipt.setCmcIsVoid(true);
		OBDal.getInstance().save(receipt);
		return receipt;
	}
}
