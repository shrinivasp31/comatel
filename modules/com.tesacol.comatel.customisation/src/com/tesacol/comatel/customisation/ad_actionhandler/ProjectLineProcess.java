package com.tesacol.comatel.customisation.ad_actionhandler;

import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;

import com.tesacol.comatel.customisation.CMCProject;

public class ProjectLineProcess extends BaseProcessActionHandler {

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters, String content) {
		JSONObject jsonObject = null;
		JSONObject error = null;
		try {
			error = new JSONObject();
			jsonObject = new JSONObject(content);
			processRequest(jsonObject);
			error.put("text", "Process Completed Successfully.");
			error.put("severity", "success");
			jsonObject.put("message", error);
		} catch (JSONException e) {
			error = new JSONObject();
			try {
				error.put("text", "Error while Processing Request:" + e.getMessage());
				error.put("severity", "error");
				jsonObject.put("message", error);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		OBDal.getInstance().flush();
		return jsonObject;
	}

	private void processRequest(JSONObject jsonObject) throws JSONException {
		String invoiceId = jsonObject.getString("inpcInvoiceId");
		Invoice invoice = OBDal.getInstance().get(Invoice.class, invoiceId);
		JSONArray selectedLines = jsonObject.getJSONObject("_params").getJSONObject("EM_Cmc_Create_Project_Line")
				.getJSONArray("_selection");
		if (selectedLines.length() < 0) {
			return;
		}

		CMCProject project = OBDal.getInstance().get(CMCProject.class,
				selectedLines.getJSONObject(0).getString("cMCProject"));
		invoice.setCmcProject(project);
		for (int i = 0; i < selectedLines.length(); i++) {
			ShipmentInOut inout = OBDal.getInstance().get(ShipmentInOut.class,
					selectedLines.getJSONObject(i).getString("inout"));
			if (!invoice.getBusinessPartner().getId().equals(inout.getBusinessPartner().getId())) {
				throw new OBException("El proyecto debe tener el mismo Tercero");
			}
			inout.setInvoice(invoice);
			inout.setCmcIncInProject(true);
			OBDal.getInstance().save(inout);
		}
		OBDal.getInstance().save(invoice);
	}
}
